<?php

class Database extends mysqli
{
	public function __construct($host = false, $database = false, $user = false, $password = false){

		$dbhost = !$host ? DB_HOST : $host;
        $dbname = !$database ? DB_NAME : $database;
        $dbuser = !$user ? DB_USER : $user;
        $dbpass = !$password ? DB_PASS : $password;

        parent::__construct($dbhost, $dbuser, $dbpass, $dbname);

        if (mysqli_connect_error()) {
            die("Error de conexión. Asegúrese de que el nombre del laboratorio sea el correcto.");
            /*die('Error de Conexión (' . mysqli_connect_errno() . ') '
                    . mysqli_connect_error());*/
        }

        $this->set_charset('utf8');
	}

	public function fetchArray($preparedQuery){
		$meta = $preparedQuery->result_metadata();

        while ($field = $meta->fetch_field()) {
            $params[] = &$row[$field->name];
        }

        call_user_func_array(array($preparedQuery, 'bind_result'), $params);
        while ($preparedQuery->fetch()) {
            foreach($row as $key => $val) {
                $c[$key] = $val;
            }
            $assoccArray[] = $c;
        }
        return $assoccArray;
	}

	public function log($query){
        $row = date('Y-m-d h:i:s') . ' | ' . Session::get('iduser') . ' | ' . Session::get('nombre_apellido') . ' | ' . $query . "\n";
        if(!DEV){
            $saveFolder = $_GET['lab'];
        } else {
            $saveFolder = 'test';
        }
        file_put_contents(ROOT . DS . 'laboratorios' . DS . 'assets' . DS . $saveFolder . DS . 'database.log', $row, FILE_APPEND);
    }
}

?>
