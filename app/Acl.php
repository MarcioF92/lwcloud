<?php

class Acl
{
	private $_registry;
	private $_iduser;
	private $_user;
	private $_idrole;
	private $_permissions;

	public function __construct($id = false){

		$this->_registry = Registry::getInstance();

		if($id){
			$this->_iduser = $_iduser;
		} else {
			
			if(Session::get('iduser')){
				$this->_iduser = Session::get('iduser');
			} else {
				$this->_iduser = 0;
			}

		}

		if ($this->_iduser) {
			$this->_idrole = Session::get('role');
			$this->_permissions = $this->getPermissionsRole();
		}

		
	}

	public function getPermissionsRole(){
		$permissions = $this->getPermissions();
		$permissions_role = $this->getArrayData('permission_roles');
		$data = array();

		foreach ($permissions_role as $permission_role) {
			$permission_role_array = (array)$permission_role;
			if ($permission_role_array['role'] == $this->_idrole) {
				if (array_key_exists($permission_role_array['permission'], $permissions)) {
					$data[$permission_role_array['permission']] = $permission_role_array['permission'];
				}
			}

		}

		return $data;
	}

	public function getPermissions(){
		return $this->getArrayData('permissions');
	}

	public function permission($key){
		if (array_key_exists($key, $this->_permissions)) {
				return true;	
		}

		return false;
	}

	public function access($key){
		if ($this->permission($key)) {
			Session::time();
			return;
		}

		header("location: " . BASE_URL . "error/access/error/5050");
		exit;
	}

	public function getArrayData($table){
		if ($table == "roles" || $table == "permissions" || $table == "permission_roles") {

			if (file_exists(APP_PATH . "permission_roles" . DS . $table . ".json")) {
				$json = file_get_contents(APP_PATH . "permission_roles" . DS . $table . ".json");
				$jsonObj = json_decode($json);
				$jsonArray = (array)$jsonObj->data;
				
				return $jsonArray;
			}

		}
		
	}

}

?>