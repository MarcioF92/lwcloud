<?php

class App{
    public static function init(){

        if(!DEV) {
            /*$arrayHost = explode(".",$_SERVER['HTTP_HOST']);
            define('SUBDOMAIN', array_shift($arrayHost));*/

            define('SUBDOMAIN', 'http://www.biodatasa.com.ar/lwcloud/'.$_GET['lab']);

            define('IS_LAB', false);

            ini_set('display_errors',0); // Determina que se muestren los errores

            header('Access-Control-Allow-Origin: http://'.SUBDOMAIN.'.biodatasa.com.ar');

        } else {

            define('SUBDOMAIN', 'test');

            define('IS_LAB', false);

            ini_set('display_errors',1); // Determina que se muestren los errores
        }
    }
}