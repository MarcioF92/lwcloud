<?php

/*
* Index Controller of Home Module
* /public_html/laboratorios/mobile/modules/login/home/indexController.php
*/

class indexController extends Controller
{
	private $_postsModel;

    public function __construct()
    {
        parent::__construct();
        $this->_postsModel = $this->loadModel('post');
    }
 
    public function index(){
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }

        if(Session::get('role') != 1 && Session::get('role') != 6 ){
            $this->redirect('rol');
        }

    	$this->_view->assign('titulo', 'Inicio');
        $this->_view->assign('body-classes', array('home-background'));
        $this->_view->assign('content-classes', array('vertical-align'));
        $this->_view->assign('idRole', Session::get('role'));

        $page = 'index';

        if(Session::get('role') == 6 ){
            $page = 'admin';
        }

        $this->_view->render($page, 'Inicio');
    }

}

?>