<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-ms-6 col-ms-offset-3 col-xs-10 col-xs-offset-1 text-center">
	<div class="col-lg-6 col-lg-offset-3">
		<div class="col-lg-12 form-control-container">
			<strong>
					<a class="custom-form-control col-lg-12 light-blue-color" href="<?php echo $this->_layoutParams['base_url']; ?>rol">
					RESULTADOS ONLINE (ROL)
				</a>
			</strong>
		</div>

        <div class="col-lg-12 form-control-container">
            <strong>
                <a class="custom-form-control col-lg-12 light-blue-color" href="<?php echo $this->_layoutParams['base_url']; ?>rol/index/resultados_por_fechas">
                    RESULTADOS POR FECHAS
                </a>
            </strong>
        </div>

		<div class="col-lg-12 form-control-container">
			<strong>
				<a class="custom-form-control col-lg-12 light-blue-color" href="<?php echo $this->_layoutParams['base_url']; ?>mol">
					MUESTRAS ONLINE (MOL)
				</a>
			</strong>
		</div>

        <?php if (Session::get('role') == 1 && Session::get('user') == 'RESIDE') : ?>

            <div class="col-lg-12 form-control-container">
                <strong>
                    <a class="custom-form-control col-lg-12 light-blue-color" href="<?php echo $this->_layoutParams['base_url']; ?>conta">
                        LIQUIDACIONES
                    </a>
                </strong>
            </div>

        <?php endif; ?>

		<div class="col-lg-12 form-control-container">
			<strong>
				<a class="custom-form-control col-lg-12 light-blue-color" href="<?php echo BASE_URL . 'login/index/cerrar'; ?>">
					SALIR
				</a>
			</strong>
		</div>

		<div class="col-lg-12 form-control-container text-center">
			<img src="<?php echo PRIMARY_URL . '/public/img/labwin-logo-mini.png'; ?>" />
		</div>
	</div>
</div>
