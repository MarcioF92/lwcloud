<?php

/*
* Index Controller of Home Module
* /public_html/laboratorios/mobile/modules/login/home/indexController.php
*/

class indexController extends Controller
{
	private $_postsModel;
	private $tiposLiquidaciones = [
        "OP" => "órden de pago",
        "RE" => "Recibos",
        "FA" => "Factura A",
        "FB" => "Factura B",
        "FC" => "Factura C",
        "CA" => "Nota de crédito A",
        "CB" => "Nota de crédito B",
        "CC" => "Nota de crédito C",
        "DA" => "Nota de débito A",
        "DB" => "Nota de  débito B",
        "DC" => "Nota de  débito C",
        "FMA" => "Factura MiPyme A",
        "FMB" => "Factura MiPyme B",
        "FMC" => "Factura MiPyme C",
        "CMA" => "Nota de crédito MiPyme A",
        "CMB" => "Nota de crédito MiPyme B",
        "CMC" => "Nota de crédito MiPyme C",
        "DMA" => "Nota de débito MiPyme A",
        "DMB" => "Nota de  débito MiPyme B",
        "DMC" => "Nota de  débito MiPyme C",
        "LI" => "Liquidación",
        "PL" => "Pre liquidación",
        "RL" => "Re liquidación"
    ];

    public function __construct()
    {
        parent::__construct();
        $this->_postsModel = $this->loadModel('post');
    }
 
    public function index()
    {
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }

        $ficheros = scandir(__DIR__ . "/../../../laboratorios/assets/".$_GET['lab']."/liquidaciones");

        $periodos = [];

        foreach ($ficheros as $fichero) {
            if($fichero != '.' && $fichero != '..')
                $periodos[] = $fichero;
        }

    	$this->_view->assign('titulo', 'Datos contables');
        $this->_view->assign('body-classes', array('home-background'));
        $this->_view->assign('periodos', $periodos);

        $this->_view->render('index', 'datos_contables');
    }

    public function periodo($periodo)
    {
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }

        $ficheros = scandir(__DIR__ . "/../../../laboratorios/assets/".$_GET['lab']."/liquidaciones/".$periodo);

        $liquidaciones = [];

        foreach ($ficheros as $fichero) {

            if($fichero == '.' || $fichero == '..')
                continue;

            $liquidacion = $this->processLiquidacion($fichero);
            if ($liquidacion['id'] == Session::get('iduser'))
                $liquidaciones[] = $liquidacion;

        }

        $this->_view->assign('titulo', 'Datos contables');
        $this->_view->assign('body-classes', array('home-background'));
        $this->_view->assign('liquidaciones', $liquidaciones);
        $this->_view->assign('periodo', $periodo);

        $this->_view->render('liquidaciones', 'liquidaciones');
    }

    private function processLiquidacion($fichero)
    {
        $data = explode('-', $fichero);

        return [
            'tipo-liquidacion' => $this->tiposLiquidaciones[$data[0]],
            'id' => $data[array_search('DerBioq', $data) + 1],
            'archivo' => $fichero
        ];
    }

}

?>