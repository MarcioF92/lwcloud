<div class="col-md-12">

    <div class="col-lg-10 col-lg-offset-1 col-xs-12">
        <div class="col-lg-12 col-xs-12">
            <div class="col-lg-4 col-xs-12 text-left">
                <img class="col-lg-12 col-xs-12" src="<?php echo PRIMARY_URL; ?>public/img/labwin-logo.png" />
            </div>
            <div class="col-lg-8 col-xs-12 text-right">
                <h2 class="title light-blue-color"><?php echo $this->_customVars['titulo']; ?></h2>
            </div>
        </div>
    </div>

    <div id="resultado-blue-strip" class="col-lg-12 col-xs-12">
        <div class="col-lg-10 col-lg-offset-1 col-xs-12">
        </div>
    </div>


    <div class="col-lg-10 col-lg-offset-1 col-xs-12">
        <div class="col-lg-12 col-xs-12 row">
            <?php	if($this->_customVars['periodos'] && count($this->_customVars['periodos']) > 0){ ?>

                <div class="col-lg-12 col-xs-12 table-header word-break hidden-xs">
                    <div class="col-xs-12">Período</div>
                </div>
                <?php foreach ($this->_customVars['periodos'] as $periodo) { ?>

                    <div class="col-lg-12 col-xs-12 no-padding word-break">
                        <div class="col-xs-12 custom-form-control">
                            <a href="<?php echo $this->_layoutParams['base_url'].'conta/index/periodo/'.$periodo;?>"><?php echo $periodo; ?></a>
                        </div>
                    </div>
                <?php } ?>

            <?php	} else {
                echo "No hay datos";
            }
            ?>
        </div>
    </div>
</div>
