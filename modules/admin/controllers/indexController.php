<?php

/*
* Index Controller of Admin Module
* /public_html/laboratorios/mobile/modules/admin/indexController.php
*/

class indexController extends Controller
{

    private $_adminModel;

    public function __construct()
    {
        parent::__construct();
        $this->_view->setTemplate('admin_template');
        $this->_adminModel = $this->loadModel('admin');
        $this->_acl->access('admin');
    }

    public function index(){
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }

        $this->_view->assign('titulo', 'Control de envíos');
        $this->_view->assign('body-classes', array('home-background'));
        $this->_view->assign('tipos_usuarios', $this->_adminModel->getTiposDeUsuario());

        $this->_view->assign('colegas', $this->_adminModel->getColegas());
        $this->_view->setJs(array('oneImput', 'admin'));

        if ($this->getPostParam('buscar_1') == 1) {
            $this->_view->assign('datos', $_POST);
            $this->_view->assign('tipoUsuario', $this->getPostParam('tipo_usuario'));

            if ($this->getPostParam('fecha_desde') == '') {
              $fecha_desde = date('d/m/Y', mktime(0, 0, 0, 1, 1, 1980));
            } else {
              $fecha_desde = $this->getPostParam('fecha_desde');
            }

            if ($this->getPostParam('fecha_hasta') == '') {
              $fecha_hasta = date('d/m/Y');
            } else {
              $fecha_hasta = $this->getPostParam('fecha_hasta');
            }

            if ($fecha_desde > $fecha_hasta) {
                $this->_view->assign('_error', 'La fecha final debe ser mayor a la de inicio');
                $this->_view->render('index', 'Admin');
                exit;
            }
            $resultados = $this->_adminModel->getDerivacionesFiltrosUno($fecha_desde, $fecha_hasta, $this->getPostParam('tipo_usuario'), $this->getPostParam('usuarios'));
            if($resultados) {
                foreach ($resultados as $resultado) {
                    $resultadoSession[$resultado['Numero_Fld']] = [
                        'Numero_Fld' => $resultado['Numero_Fld'],
                        'Sucursal_Fld' => $resultado['Sucursal_Fld'],
                    ];
                }
                Session::set('resultados', $resultadoSession);
                $this->_view->assign('resultados', $resultados);
            }
        }

        if ($this->getPostParam('buscar_2') == 1) {
            $this->_view->assign('datos', $_POST);

            $this->_view->assign('numeroDeEnvio', false);

            if ($this->getPostParam('numero_envio') != '') {
                $this->_view->assign('numeroDeEnvio', true);
            }
            $resultados = $this->_adminModel->getDerivacionesFiltrosDos($this->getPostParam('numero_envio'), $this->getPostParam('numero_protocolo'), $this->getPostParam('dni'));
            foreach ($resultados as $resultado){
                $resultadoSession[$resultado['Numero_Fld']] = [
                    'Numero_Fld' => $resultado['Numero_Fld'],
                    'Sucursal_Fld' => $resultado['Sucursal_Fld'],
                ];
            }

            if(count($resultados) == 1 && $resultados !== false)
                $this->redirect('/admin/index/ver_resultado/'.$resultados[0]['Sucursal_Fld'].'/'.$resultado['Numero_Fld']);

            Session::set('resultados', $resultadoSession);
            $this->_view->assign('resultados', $resultados);
        }

        $this->_view->render('index', 'Admin');
    }

    public function getByTipo(){
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }
        $data = json_decode(file_get_contents("php://input"));
        echo json_encode($this->_adminModel->getUsuariosByTipo($data->slug));
    }

    public function ver_resultado($sucursal = false, $idderivacion = false){
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }
        $this->_acl->access('admin');

        $this->_view->assign('titulo','Ver resultado');
        $this->_view->assign('datos_derivacion',array('sucursal' => $sucursal, 'idderivacion' => $idderivacion));
        $formatos = $this->_adminModel->getFormatos();
        $formatosFinal = [];
        foreach ($formatos as $formato){
            $formatosFinal[$formato['Abrev_Fld']] = $formato;
        }
        $resultadoSession = Session::get('resultados');
        $resultadoSessionAux = $resultadoSession;
        $currentResultadoSession = $resultadoSession[$idderivacion];
        while (current($resultadoSession) !== $currentResultadoSession) {
            next($resultadoSession);
            next($resultadoSessionAux);
        }

        $prev = prev($resultadoSession);
        $resultadoSession = $resultadoSessionAux;
        $next = next($resultadoSession);

        $this->_view->assign('prev', $prev);
        $this->_view->assign('next', $next);

        $this->_view->assign('formatos_determinaciones', $formatosFinal);
        $fuente = $this->_adminModel->getFuente($formatosFinal['DEFAULT']['Fuente']);
        $this->_view->assign('fuente', $fuente['font-family']);

        $configs = $this->_adminModel->getConfigs();

        $this->_view->assign('configs', $configs);

        $this->_view->assign('resultado', $this->_adminModel->getResultado($sucursal, $idderivacion));
        $this->_view->render('ver_resultado', 'admin');
    }

    public function ver_resultado_pdf($sucursal = false, $idderivacion = false)
    {
        $this->redirect('rol/index/ver_resultado_pdf/'.$sucursal.'/'.$idderivacion.'/'.Session::get('role').'/'.Session::get('iduser'));
    }

    public function ver_remito($idremito){
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }

        $this->_view->assign('titulo', 'Control de envíos');
        $this->_view->assign('body-classes', array('home-background'));

        $this->_view->assign('resultado', $this->_adminModel->getSinIngresar($idremito));

        $this->_view->render('ver_remito', 'Admin');

    }

    public function confirmar_eliminacion($sucursal, $idresultado){
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }
        $this->_acl->access('admin');
        if ($idresultado == false) {
            $this->redirect('admin');
        }
        $this->_view->assign('titulo','Confirmar eliminación');

        if($this->getInt('eliminar') == 1){
            if($this->_adminModel->eliminarResultado($idresultado)){
                $this->redirect('admin');
            } else {
                echo "Devolvió false";
            }
        }

        $this->_view->assign('datos_resultado', array('sucursal' => $sucursal, 'idresultado' => $idresultado));
        $this->_view->assign('resultado', $this->_adminModel->getResultado($sucursal, $idresultado));
        $this->_view->render('confirmar_eliminacion', 'Admin');
    }

    public function modificar_nombre(){
        $idderivacion = $this->getPostParam('idderivacion');
        $sucursal = $this->getPostParam('sucursal');
        $nombre = $this->getPostParam('nombre');

        $this->_adminModel->modificarNombre($idderivacion, $sucursal, $nombre);

        $this->redirect('admin/index/ver_resultado/' . $sucursal . '/' . $idderivacion);
    }

    /* Eliminar determinaciones del nomeclador */
    public function eliminar_practica($sucursal, $idderivacion, $abrev){
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }

        $this->_acl->access('config');

        $this->_view->assign('titulo', '¿Eliminar párcica?');
        $this->_view->assign('sucursal', $sucursal);
        $this->_view->assign('idderivacion', $idderivacion);
        $this->_view->assign('determinacion', $this->_adminModel->getDeterminacion(str_replace('_ESP_', ' ',$abrev)));

        if($this->getPostParam('aceptar')){
            if($this->_adminModel->deleteDeterminacion($sucursal, $idderivacion, str_replace('_ESP_', ' ',$abrev)))
                $this->redirect('admin/index/ver_resultado/1/370494/');
        }

        $this->_view->render('eliminar_practica', 'admin');
    }

}

?>
