<div class="col-lg-10 col-lg-offset-1">
  <div class="col-lg-12">
    <div class="col-lg-4 text-left hidden-xs hidden-sm">
      <img class="col-lg-12 col-xs-12" src="/public/img/labwin-logo.png" />
    </div>
    <div style="padding: 10px;" class="col-lg-8 text-right">
      <h2 class="title light-blue-color col-xs-12">
      	CONTROL DE ENVÍOS
      </h2>
    </div>
  </div>
</div>

<div id="resultado-blue-strip" class="col-lg-12 col-xs-12 hidden-xs hidden-sm">
	<div class="col-lg-10 col-lg-offset-1 col-xs-12">
		<div class="col-lg-6 col-xs-6">
			<p class="big-font no-margin no-padding"><i class="fa fa-eye white-color" aria-hidden="true"></i></p>
		</div>
		<div class="col-lg-6 text-right col-xs-6">
			<a class="custom-form-control" href="<?php echo $this->_layoutParams['base_url']?>admin">Volver <i class="fa fa-reply" aria-hidden="true"></i></a>
		</div>
	</div>
</div>

<div class="col-lg-10 col-lg-offset-1">
	Envío <?php echo $this->_customVars['resultado']['datos']['tipo_envio'] . ' - ' . $this->_customVars['resultado']['datos']['idu'] . ' - ' . $this->_customVars['resultado']['datos']['id']; ?><br>
	<br>
	<strong>Colega: <?php echo $this->_customVars['resultado']['datos']['derivante']['Nombre_Fld']; ?></strong><br>
	<strong>Fecha: <?php echo $this->_customVars['resultado']['datos']['fecha']; ?></strong><br>

	<?php foreach ($this->_customVars['resultado']['derivaciones'] as $derivacion) { ?>
		<div class="col-lg-12 col-xs-12 custom-form-control">
			<div class="col-lg-4 col-xs-4">
				<p>Nro. Tubo</p>
			</div>
			<div class="col-lg-4 col-xs-4">
				<p>Apellido y Nombre</p>
			</div>
			<div style="word-break:break-all" class="col-lg-4 col-xs-4">
				<p>Determinaciones</p>
			</div>
			<div class="col-lg-4 col-xs-4">
				<p><?php echo $derivacion['Tubos_Fld']; ?></p>
			</div>
			<div class="col-lg-4 col-xs-4">
				<p><?php echo $derivacion['Nombre_Fld']; ?></p>
			</div>
			<div class="col-lg-4 col-xs-4">
				<p><?php echo $derivacion['determinaciones']; ?></p>
			</div>
			<div class="col-lg-12">
				<strong>Otras determinaciones químicas: </strong>
				<?php echo $derivacion['Observ_Fld']; ?>
			</div>
		</div>
	<?php } ?>
</div>
