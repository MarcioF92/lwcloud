<div class="col-lg-10 col-lg-offset-1">
    <div class="col-lg-12">
        <div class="col-lg-4 text-left hidden-xs hidden-sm">
            <img class="col-lg-12 col-xs-12" src="/public/img/labwin-logo.png" />
        </div>
        <div style="padding: 10px;" class="col-lg-8 text-right">
            <h2 class="title light-blue-color col-xs-12">
                ELIMINAR RESULTADO
            </h2>
        </div>
    </div>
</div>

<div id="resultado-blue-strip" class="col-lg-12 col-xs-12 hidden-xs hidden-sm">

</div>

<div class="col-lg-10 col-lg-offset-1">
    <h3>Está por eliminar un resultado con los siguientes datos, ¿Desea continuar?</h3>
    <form action="" method="post">

        <input type="hidden" name="eliminar" value="1" />
        <input type="hidden" name="sucursal" value="<?php echo $this->_customVars['datos_resultado']['sucursal']; ?>" />
        <input type="hidden" name="idresultado" value="<?php echo $this->_customVars['datos_resultado']['idresultado']; ?>" />

        <div class="col-md-12 col-xs-12">
            <div class="col-md-4 col-xs-6">
                <p>Paciente: <?php echo $this->_customVars['resultado']['datos'][0]['Paciente']; ?></p>
                <p>Protocolo: <?php echo $this->_customVars['resultado']['datos'][0]['Protocolo']; ?></p>
            </div>
            <div class="col-md-4 col-xs-6">
                <p>Fecha: <?php echo substr($this->_customVars['resultado']['datos'][0]['Fecha'], 6) . "/" . substr($this->_customVars['resultado']['datos'][0]['Fecha'], 4, 2) . "/" . substr($this->_customVars['resultado']['datos'][0]['Fecha'], 0, 4); ?></p>
                <p>Solicita: <?php echo $this->_customVars['resultado']['datos'][0]['Solicita']; ?></p>
            </div>
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="col-md-4 col-xs-6">
                <button type="submit" class="btn btn-primary">Aceptar</button>
            </div>
            <div class="col-md-4 col-xs-6">
                <a href="<?php echo $this->_layoutParams['root'];?>admin">Volver</a>
            </div>
        </div>
    </form>
</div>
