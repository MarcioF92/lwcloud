<div>
    <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-ms-12 col-xs-12">
      <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12">
        <div class="col-lg-4 col-md-4 col-ms-4 col-xs-12 text-left">
          <img class="col-lg-12 col-md-12 col-ms-12 col-xs-12" src="<?php echo PRIMARY_URL . '/public/img/labwin-logo.png'; ?>" />
        </div>
        <div style="padding: 10px;" class="col-lg-8 text-center-xs text-right">
          <h2 class="title light-blue-color"><?php echo $this->_customVars['titulo']; ?></h2>
        </div>
      </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function(){
        $( ".datepicker" ).datepicker({
            inline: true
        });
        var colega = "<?php if(isset($this->_customVars['datos']['colegas'])){echo $this->_customVars['datos']['colegas'];} ?>";
        if (colega == '') {
            colega = -1;
        }
        $("#colegas").val(colega);
    });
    </script>

    <div ng-controller="ADMIN" class="tab-content col-lg-12 col-xs-12 no-padding">
        <div class="col-lg-10 col-lg-offset-1 white-color">
            <div class="col-lg-12 no-padding">
                <form action="" method="POST">
                    <input type="hidden" name="buscar_1" value="1">
                    <div class="col-lg-6">
                        <div class="col-lg-2">
                            FECHA
                        </div>
                        <div class="col-lg-5">
                            <?php
                                if(isset($this->_customVars['datos']['fecha_desde'])){
                                    $fechaDesde = $this->_customVars['datos']['fecha_desde'];
                                } else {
                                    $fechaDesde = date('Y-m-d', time() - (7 * 24 * 60 * 60));
                                }
                                if(isset($this->_customVars['datos']['fecha_hasta'])){
                                    $fechaHasta = $this->_customVars['datos']['fecha_hasta'];
                                } else {
                                    $fechaHasta = date('Y-m-d');
                                }
                            ?>
                            <input type="date" class="form-control" name="fecha_desde" value="<?php echo $fechaDesde; ?>" required="">
                        </div>
                        <div class="col-lg-5">
                            <input type="date" class="form-control" name="fecha_hasta" value="<?php echo $fechaHasta; ?>" required="">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="col-lg-5">
                            <select id="tipo_usuario" name="tipo_usuario" class="form-control" ng-model="tipoUsuario" ng-change="ajaxUpdate()">
                                <option value="-1">Tipo de usuario</option>
                                <?php foreach ($this->_customVars['tipos_usuarios'] as $index => $tipo) { ?>
                                    <?php if($index != 'paciente') { ?>
                                        <option value="<?php echo $index; ?>"><?php echo $tipo; ?></option>
                                    <?php  } ?>
                                <?php  } ?>
                            </select>
                        </div>
                        <div class="col-lg-5">
                            <select name="usuarios" id="usuarios" class="form-control">
                                <option ng-repeat="usuario in usuarios" value="{{ usuario.identificador }}">{{ usuario.nombre }}</option>
                            </select>
                        </div>
                        <div class="col-lg-2">
                            <input type="submit" value="Buscar" class="btn btn-info">
                        </div>
                    </div>
                </form>
            </div>
            <hr class="white-color">
            <div class="col-lg-12">
                <form action="" method="POST">
                    <input type="hidden" name="buscar_2" value="1">
                    <input type="hidden" name="campoFoco" id="campoFoco" value="">

                    <div class="col-lg-3">
                        N° DE ENVÍO <input type="text" name="numero_envio" id="numero_envio" class="form-control clean-on-focus" value="<?php if(isset($this->_customVars['datos']['numero_envio'])){echo $this->_customVars['datos']['numero_envio'];} ?>">
                    </div>
                    <div class="col-lg-3">
                        N° DE PROTOCOLO <input type="text" name="numero_protocolo" id="numero_protocolo" class="form-control clean-on-focus" value="<?php if(isset($this->_customVars['datos']['numero_protocolo'])){echo $this->_customVars['datos']['numero_protocolo'];} ?>">
                    </div>
                    <div class="col-lg-3">
                    DNI <input type="text" name="dni" id="dni" class="form-control clean-on-focus" value="<?php if(isset($this->_customVars['datos']['dni'])){echo $this->_customVars['datos']['dni'];} ?>">
                    </div>
                    <div class="col-lg-3">
                    <input type="submit" value="Buscar" class="btn btn-info">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="resultados">
    <?php if(isset($this->_customVars['resultados'])){ ?>
        <?php if($this->_customVars['resultados'] != false){ ?>
            <?php if(!isset($this->_customVars['numeroDeEnvio'])){ ?>
                <div class="col-lg-10 col-lg-offset-1 col-xs-12 no-padding">
                    <div class="col-lg-12 table-header hidden-xs">
                      <div class="col-lg-2"><p>Colega</p></div>
                      <div class="col-lg-1"><p>Fecha</p></div>
                      <div class="col-lg-1"><p>Protocolo</p></div>
                      <div class="col-lg-2"><p>Apellido y nombre</p></div>
                      <div class="col-lg-1"><p>Documento</p></div>
                      <div class="col-lg-2"><p>Fecha de entrega</p></div>
                      <div class="col-lg-1"><p>Estado</p></div>
                      <div class="col-lg-1"><p>Importado</p></div>
                      <div class="col-lg-1"><p>Ver</p></div>
                    </div>

                    <?php foreach ($this->_customVars['resultados'] as $resultado) {
                        if ($resultado['estado'] != 'NOMOSTRAR') { ?>
                            <div class="col-lg-12 col-xs-12 custom-form-control no-padding">
                                <div class="col-lg-2 col-xs-4 word-break no-padding text-center"><?php echo $resultado['nombreUsuario']; ?></div>
                                <div class="col-lg-1 col-xs-4 word-break no-padding">
                                    <?php
                                    echo substr($resultado['Fecha_Fld'], 6, 2) . '/' . substr($resultado['Fecha_Fld'], 4, 2) . '/' . substr($resultado['Fecha_Fld'], 0, 4);
                                    ?>
                                </div>
                                <div class="col-lg-1 col-xs-4 word-break no-padding"><?php echo $resultado['ImportNumOrig_Fld']; ?></div>
                                <div class="col-lg-2 col-xs-4 word-break no-padding"><?php echo $resultado['Nombre_Fld']; ?></div>
                                <div class="col-lg-1 col-xs-4 word-break no-padding"><?php echo $resultado['HClin_Fld']; ?></div>
                                <div class="col-lg-2 col-xs-4 word-break no-padding">
                                    <?php
                                    if ($resultado['Entrega_Fld'] != '') {
                                        echo substr($resultado['Entrega_Fld'], 6, 2) . '/' . substr($resultado['Entrega_Fld'], 4, 2) . '/' . substr($resultado['Entrega_Fld'], 0, 4);
                                    } else {
                                        echo "No disponible";
                                    }
                                    ?>
                                </div>
                                <div class="col-lg-1 col-xs-4 word-break no-padding"><?php echo $resultado['estado']; ?></div>
                                <div class="col-lg-1 col-xs-4 word-break no-padding"><?php echo $resultado['Importado_Fld']; ?></div>
                                <div class="col-lg-1 col-xs-4 word-break no-padding">
                                    <div class="col-lg-6 col-xs-6">
                                        <a ng-click="confirm('¿Desea borrar el resultado?')" href="<?php echo BASE_URL . 'admin/index/confirmar_eliminacion/' . $resultado['Sucursal_Fld'] . '/' . $resultado['Numero_Fld']; ?>">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <div class="col-lg-6 col-xs-6 text-right">
                                        <a href="<?php echo BASE_URL . 'admin/index/ver_resultado/' . $resultado['Sucursal_Fld'] . '/' . $resultado['Numero_Fld']; ?>">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php }
                    }
                    ?>

                </div>
            <?php } else { ?>
                <div class="col-lg-10 col-lg-offset-1 col-xs-12 no-padding">
                    <div class="col-lg-12 table-header hidden-xs">
                        <div class="col-lg-1"><p># Envío</p></div>
                        <div class="col-lg-1"><p>Protocolo</p></div>
                        <div class="col-lg-2"><p>Fecha</p></div>
                        <div class="col-lg-2"><p>Apellido y nombre</p></div>
                        <div class="col-lg-1"><p>Documento</p></div>
                        <div class="col-lg-1"><p>Ver</p></div>
                    </div>
                <?php foreach ($this->_customVars['resultados'] as $resultado) { ?>

                        <?php //echo "<pre>"; print_r($resultado); echo "</pre>"; ?>
                        <div class="col-lg-12 col-xs-12 custom-form-control no-padding">
                            <div class="col-lg-1 col-xs-4 word-break no-padding text-center">
                                <?php echo $resultado['NumEnvio_Fld']; ?>
                            </div>
                            <div class="col-lg-1 col-xs-4 word-break no-padding text-center">
                                <?php echo $resultado['ImportNumOrig_Fld']; ?>
                            </div>
                            <div class="col-lg-2 col-xs-4 word-break">
                                <?php
                                echo substr($resultado['Fecha_Fld'], 6, 2) . '/' . substr($resultado['Fecha_Fld'], 4, 2) . '/' . substr($resultado['Fecha_Fld'], 0, 4);
                                ?>
                            </div>
                            <div class="col-lg-2 col-xs-4 word-break">
                                <?php echo $resultado['Nombre_Fld']; ?>
                            </div>
                            <div class="col-lg-1 col-xs-4 word-break no-padding text-center">
                                <?php echo $resultado['HClin_Fld']; ?>
                            </div>
                            <div class="col-lg-1 col-xs-4 word-break">
                                <div class="col-lg-6 col-xs-6">
                                    <a ng-click="confirm('¿Desea borrar el resultado?')" href="<?php echo BASE_URL . 'admin/index/confirmar_eliminacion/' . $resultado['Sucursal_Fld'] . '/' . $resultado['Numero_Fld']; ?>">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <div class="col-lg-6 col-xs-6 text-right">
                                    <a href="<?php echo BASE_URL . 'admin/index/ver_resultado/' . $resultado['Sucursal_Fld'] . '/' . $resultado['Numero_Fld']; ?>">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        <?php } else { ?>
                <div class="no-result text-center col-md-12">
                    <h2>CONTROLE EN ESTA SECCIÓN EL REMITO DEL COLEGA</h2>
                </div>
        <?php } ?>
    <?php } else { ?>
        <div class="no-result text-center col-md-12">
            <h2>CONTROLE EN ESTA SECCIÓN EL REMITO DEL COLEGA</h2>
        </div>
    <?php } ?>
    </div>
</div>