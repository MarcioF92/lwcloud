<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-ms-6 col-ms-offset-3 col-xs-10 col-xs-offset-1 text-center">
    <div class="col-lg-8 col-lg-offset-2">
        <form action="" method="post">
            <input type="hidden" name="aceptar" value="1" />
            <input type="hidden" name="abrev" value="<?php echo $this->_customVars['determinacion']['Abrev_Fld']; ?>" />
            <input type="hidden" name="sucursal" value="<?php echo $this->_customVars['sucursal']; ?>" />
            <input type="hidden" name="idderivacion" value="<?php echo $this->_customVars['idderivacion']; ?>" />

            <h3>¿Está seguro que desea eliminar la práctica <?php echo $this->_customVars['determinacion']['Nombre_Fld']; ?> de la derivación?</h3>
            <button type="submit" class="btn btn-primary">Aceptar</button>
            <a href="<?php echo $this->_layoutParams['root'].$_GET['lab'].'/admin/index/ver_resultado/'.$this->_customVars['sucursal'].'/'.$this->_customVars['idderivacion']; ?>">Cancelar</a>
        </form>
    </div>
</div>