<?php
$anterioresHTML = '';


if(is_array($resultado['ANTERIORES']) && count($resultado['ANTERIORES']) > 0){
    $id = uniqid();

    $botonAnteriores = '<button class="btn btn-info" data-toggle="collapse" data-target="#collapse-' . $id . '">+</button>';
    $anterioresHTML .= '<div id="collapse-' . $id . '" class="collapse col-md-12 col-xs-12">';
    foreach ($resultado['ANTERIORES'] as $anterior) {

            $anterioresHTML .= '<div class="col-md-12 col-xs-12 no-padding">';
            $anterioresHTML .= '<div class="col-md-6 col-xs-6 no-padding">';
            $anterioresHTML .= substr($anterior['fecha'], 6) . "/" . substr($anterior['fecha'], 4, 2) . "/" . substr($anterior['fecha'], 0, 4);
            $anterioresHTML .= '</div>';
            $anterioresHTML .= '<div class="col-md-4 col-xs-4 no-padding">';
            $anterioresHTML .= "<" . $tags['Anteriores'] . ">" . $anterior['valor1'] . "</" . $tags['Anteriores'] . ">";
            $anterioresHTML .= '</div>';
            $anterioresHTML .= '<div class="col-md-2 col-xs-2 no-padding">';

            if (isset($anterior['valor2']) && $anterior['valor2'] != '') {
                $anterioresHTML .= "<" . $tags['Anteriores'] . ">" . $anterior['valor2'] . "</" . $tags['Anteriores'] . ">";
            }
            $anterioresHTML .= "<" . $tags['Unidades'] . ">" . $anterior['unidades'] . "</" . $tags['Unidades'] . ">";
            $anterioresHTML .= '</div>';
            $anterioresHTML .= '</div>';

    }

    $anterioresHTML .= '</div><!-- Anteriores -->';
}
?>

<div class="col-md-6 col-xs-6 no-padding">
    <div class="col-md-12 col-xs-12 no-padding" style="padding-left:<?php echo $tags['TabTexto'] . 'px;' ?>">
        <?php if(isset($botonAnteriores)){
            echo $botonAnteriores;
        } ?>
        <?php echo "<".$tags['Predeterminada'].">" ?>
        <?php
            $valor_resultado = "<" . $tags['Resultados'] . ">" . $resultado['RESULTADO'] . "</" . $tags['Resultados'] . ">" . "<" . $tags['Unidades'] . "> " . $resultado['UNIDADES'] . "</" . $tags['Unidades'] . ">";

            if(strpos($resultado['TEXTO_RESULTADO'], '&R') !== false || strpos($resultado['TEXTO_RESULTADO'], '&r') !== false) {
                $texto_resultado = str_replace('&R', $valor_resultado, $resultado['TEXTO_RESULTADO']);
                $texto_resultado = str_replace('&r', $valor_resultado, $resultado['TEXTO_RESULTADO']);
            } else {
                $texto_resultado = $resultado['TEXTO_RESULTADO'] . " " . $valor_resultado;
            }

            echo str_replace("Resultado :", "<span>Resultado :</span> ", $texto_resultado);
        ?>
        <?php echo "</".$tags['Predeterminada'].">" ?>
    </div>

    <div class="col-md-12 col-xs-12 no-padding">
        <?php
        if ($resultado['METODO'] != '' ||
            $resultado['VALOR_REFERENCIA'] != '' ||
            $resultado['MATERIAL'] != '') {
            ?>
            <div class="col-md-12 col-xs-12 no-padding">
                <div class="col-md-12 col-xs-12 no-padding" style="padding-left:<?php echo $tags['TabTexto'] . 'px'; ?>">
                    <ul>
                        <?php
                        $br = "";
                        if ($resultado['METODO'] != '') {
                            echo "<".$tags['Metodos'].">"."Método: " . $resultado['METODO']."</".$tags['Metodos'].">";
                            $br = "<br>";
                        }
                        if ($resultado['VALOR_REFERENCIA'] != '') {
                            echo $br . "<".$tags['Referencias'].">"."Valor de referencia: " . $resultado['VALOR_REFERENCIA'] . "</".$tags['Referencias'].">";
                            $br = "<br>";
                        }
                        if ($resultado['MATERIAL'] != '') {
                            echo $br . "<".$tags['Materiales'].">" . "Material: " . $resultado['MATERIAL'] . "</".$tags['Materiales'].">";
                        }
                        ?>
                    </ul>
                </div>
            </div>
        <?php } ?>
    </div>

    <?php if(isset($botonAnteriores)){ ?>
        <div class="col-md-12 col-xs-12 no-padding">
            <?php
                echo $anterioresHTML;
            ?>
        </div>
        <?php
    } ?>

</div>

<?php
    unset($botonAnteriores);
    unset($anterioresHTML);
?>
