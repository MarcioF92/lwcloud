<?php

if(isset($resultado['RESULTADOS'])) {
    $texto_resultado_anterior = '';
    $encabezado = '';
    if (isset($resultado['ENCABEZADO']))
        $encabezado = $resultado['ENCABEZADO'];
    ?>
    <div class="col-md-12 col-xs-12 no-padding resultado" style="background: <?php echo $tags['FondoResultado']; ?>">
        <div class="col-md-12 col-xs-12">
            <?php
            echo "<h4>" . $encabezado . "</h4>";
            ?>
        </div>
        <?php
        $nroResultadoIncluido = 1;
        foreach ($resultado['RESULTADOS'] as $key => $resu) { ?>

            <!-- <?php echo "Número Resultado Incluído: $nroResultadoIncluido - Nueva línea: " . $resu['NUEVA_LINEA'] . " - " . $key; ?> -->

            <?php
            if (!isset($resu['NUEVA_LINEA'])) $resu['NUEVA_LINEA'] = 1;
            if ($resu['NUEVA_LINEA'] == 1) $nroResultadoIncluido = 0;
            ?>


            <?php if (($nroResultadoIncluido % 2 != 0 && $resu['NUEVA_LINEA'] == 0) || $resu['NUEVA_LINEA'] == 1) { ?>
                <div class="col-md-12 col-xs-12 no-padding">
            <?php } ?>

            <div class="col-md-6 col-xs-6 no-padding" style="padding-left:<?php echo $tags['TabTexto'] . 'px'; ?>">

                <?php

                $valor_resultado = "<" . $tags['Resultados'] . ">" . $resu['RESULTADO'] . "</" . $tags['Resultados'] . ">" . "<" . $tags['Unidades'] . ">" . $resu['UNIDADES'] . "</" . $tags['Unidades'] . ">"; ?>


                <?php
                if(strpos($resu['TEXTO_RESULTADO'], '&r') !== false) {
                    $texto_resultado = str_replace('&r', $valor_resultado, $resu['TEXTO_RESULTADO']);
                }

                if(strpos($resu['TEXTO_RESULTADO'], '&R') !== false) {
                    $texto_resultado = str_replace('&R', $valor_resultado, $resu['TEXTO_RESULTADO']);
                }



                if (is_array($resu['ANTERIORES']) && count($resu['ANTERIORES']) > 0) {
                    $id = uniqid();
                    ?>

                    <button class="btn btn-info" data-toggle="collapse"
                            data-target="#collapse-<?php echo $id; ?>">+
                    </button>

                <?php } ?>

                <!-- Resultado: -->
                <?php
                echo "<" . $tags['Predeterminada'] . ">";
                echo $texto_resultado;
                echo "</" . $tags['Predeterminada'] . ">";
                ?>
            </div>

            <?php
            if (is_array($resu['ANTERIORES']) && count($resu['ANTERIORES']) > 0) {
                ?>

                <div class="col-md-12 col-xs-12">

                    <div id="collapse-<?php echo $id; ?>" class="collapse col-md-12 col-xs-12">
                        <?php foreach ($resu['ANTERIORES'] as $anterior) { ?>
                            <div class="col-md-12 col-xs-12 no-padding">
                                <div class="col-md-4 col-xs-4 no-padding">
                                    <?php
                                    echo substr($anterior['fecha'], 6) . "/" . substr($anterior['fecha'], 4, 2) . "/" . substr($anterior['fecha'], 0, 4);
                                    ?>
                                </div>
                                <div class="col-md-4 col-xs-4 no-padding">
                                    <?php
                                    echo "<" . $tags['Anteriores'] . ">" . $anterior['valor1'] . "</" . $tags['Anteriores'] . ">";
                                    ?>
                                </div>
                                <div class="col-md-4 col-xs-4 no-padding">
                                    <?php
                                    if (isset($anterior['valor2']) && $anterior['valor2'] != '') {
                                        echo "<" . $tags['Anteriores'] . ">" . $anterior['valor2'] . "</" . $tags['Anteriores'] . ">";
                                    }
                                    echo "<" . $tags['Unidades'] . ">" . $anterior['unidades'] . "</" . $tags['Unidades'] . ">" . "<br>";
                                    ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>

            <?php } ?>

            <?php
            if ($resu['METODO'] != '' ||
                $resu['VALOR_REFERENCIA'] != '' ||
                $resu['MATERIAL'] != ''
            ) {
                ?>
                <div class="col-md-12 col-xs-12 no-padding">
                    <div class="col-md-12 col-xs-12 no-padding"
                         style="padding-left:<?php echo $tags['TabTexto'] . 'px'; ?>">
                        <ul>
                            <?php
                            $br = "";
                            if ($resu['METODO'] != '') {
                                echo "<" . $tags['Metodos'] . ">" . "Método: " . $resu['METODO'] . "</" . $tags['Metodos'] . ">";
                                $br = "<br>";
                            }
                            if ($resu['VALOR_REFERENCIA'] != '') {
                                echo $br . "<" . $tags['Referencias'] . ">" . "Valor de referencia: " . $resu['VALOR_REFERENCIA'] . "</" . $tags['Referencias'] . ">";
                                $br = "<br>";
                            }
                            if ($resu['MATERIAL'] != '') {
                                echo $br . "<" . $tags['Materiales'] . ">" . "Material: " . $resu['MATERIAL'] . "</" . $tags['Materiales'] . ">";
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            <?php } ?>

            <?php if ($resu['NUEVA_LINEA'] == 1) { ?>
                </div>  <!-- Determinación Incluída 0 -->
            <?php } ?>

            <?php $nroResultadoIncluido++;
        } ?>

    </div> <!-- Fin resultado -->
    <?php
}
?>