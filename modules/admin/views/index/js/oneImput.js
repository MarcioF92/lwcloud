$(document).ready(function(){
	$(".clean-on-focus").on('focus', function(){
		$("#campoFoco").val($(this).attr('id'));

		$(this).parents('form').find('input[name="campoFoco"]').val($(this).attr('name'));
        $(this).siblings('.clean-on-focus').val('');
	});
});