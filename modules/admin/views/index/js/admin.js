var app=angular.module('ui.bootstrap', ['ngAnimate', 'ui.bootstrap']);

app.controller('ADMIN', ['$scope', '$filter', '$http', '$q', '$log', function ($scope, $filter, $http, $q, $log) {

    $scope.usuarios = [];

    $scope.ajaxUpdate=function(){
        $scope.usuarios = [{
            Numero_Fld: -1,
            Nombre_Fld: 'Cargando...'
        }];
        var deferred = $q.defer();
        if($scope.tipoUsuario != 1) {
            var conAjax = $http.post(__root__ + 'admin/index/getByTipo', {
                slug: $scope.tipoUsuario
            });
            conAjax.success(function (respuesta) {
                if (respuesta.length > 0) {
                    $scope.usuarios = [];
                    for (var i = respuesta.length - 1; i >= 0; i--) {
                        $scope.usuarios.push(respuesta[i]);
                    };
                } else {
                    return false;
                }
            });
            //return the promise
            return deferred.promise;
        }
    }

}]);

