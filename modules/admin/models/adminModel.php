<?php
class adminModel extends Model{

    public function __construct(){
        parent::__construct();
    }

    public function getTiposDeUsuario(){
        return [
            "bioquimico" => "Bioquímico",
            "medico" => "Médico",
            "paciente" => "Paciente",
            "obra_social" => "Obra Social",
            "lorigen" => "Lugar de Origen",
        ];
    }

    public function getUsuariosByTipo($tipo){
        $preparedQuery = $this->_db->stmt_init();

        switch ($tipo){
            case 'bioquimico':
                $id = 'Numero_Fld';
                $nombre = 'Nombre_Fld';
                $tabla = 'DerBioq';
                break;

            case 'medico':
                $id = 'Numero_Fld';
                $nombre = 'Nombre_Fld';
                $tabla = 'Medicos';
                break;

            case 'paciente':
                $id = 'Numero_Fld';
                $nombre = 'Nombre_Fld';
                $tabla = 'Pacientes';
                break;

            case 'obra_social':
                $id = 'Numero_Fld';
                $nombre = 'Nombre_Fld';
                $tabla = 'ObraSoc';
                break;

            case 'lorigen':
                $id = 'Abrev_Fld';
                $nombre = 'NomLab_Fld';
                $tabla = 'LOrigen';
                break;
        }

        $preparedQuery->prepare('SELECT '.$id.' as identificador,'.$nombre.' as nombre FROM '.$tabla.' ORDER BY '.$nombre.' DESC;');

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $usuarios = $this->_db->fetchArray($preparedQuery);
                return $usuarios;
            } else {
                return false;
            }
        }
    }

    public function getColegas(){
    	$preparedQuery = $this->_db->stmt_init();

    	$preparedQuery->prepare('SELECT * FROM DerBioq ORDER BY Nombre_Fld');

    	$preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $colegas = $this->_db->fetchArray($preparedQuery);
                return $colegas;
            } else {
                return false;
            }
        }
    }

    public function getEstado($resultado){
        $consulta = "SELECT SUM( IF (Modificado_Fld IS NULL, 0, 1) ) as modificados,
                       COUNT(*) AS total,
                       SUM( IF(Result_Fld IS NULL, 0, 1) ) AS escritos
                FROM Deters
                WHERE Numero_Fld = ? GROUP BY Numero_Fld";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param('i', $resultado['Numero_Fld']);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $aux = $this->_db->fetchArray($preparedQuery);
                $aux = $aux[0];
                if ($aux['total'] == 0) {
                    $estado = 'NOMOSTRAR';
                } else {
                    if($aux['escritos'] == 0) $estado =  'Ingresado';
                    elseif ($aux['total'] != $aux['escritos']  && ($aux['modificados'] > 0)) $estado = 'Actualizado';
                    elseif ($aux['total'] == $aux['escritos'] ) $estado = 'Completo';
                    elseif ($aux['total'] > $aux['escritos'] ) $estado = 'Parcial';
                    $VecesImp_Fld = ($resultado['VecesImp_Fld'] && !($aux['modificados'] > 0));
                }
                $return = array('estado' => $estado, 'VecesImp_Fld', $VecesImp_Fld);
                return $return;
            } else {
                return false;
            }
        }


    }

    public function getDerivacionesFiltrosUno($fechaInicio, $fechaFin, $tipo = false, $id = false)
    {

    	$l = "U";

    	switch ($tipo){
            case 'bioquimico':
                $campoId = 'Numero_Fld';
                $idEnPaciente = 'NumDeriv_Fld';
                $campoNombre = 'Nombre_Fld';
                $tabla = 'DerBioq';
                break;

            case 'medico':
                $campoId = 'Numero_Fld';
                $idEnPaciente = 'NumMedico_Fld';
                $campoNombre = 'Nombre_Fld';
                $tabla = 'Medicos';
                break;

            case 'paciente':
                $campoId = 'Numero_Fld';
                $campoNombre = 'Nombre_Fld';
                $tabla = 'Pacientes';
                $l = "P";
                break;

            case 'obra_social':
                $campoId = 'Numero_Fld';
                $idEnPaciente = 'Mutual_Fld';
                $campoNombre = 'Nombre_Fld';
                $tabla = 'ObraSoc';
                break;

            case 'lorigen':
                $campoId = 'Abrev_Fld';
                $idEnPaciente = 'Origen_Fld';
                $campoNombre = 'NomLab_Fld';
                $tabla = 'LOrigen';
                break;
        }

        $consulta = "SELECT P.*";

    	if($tipo !== false)
            $consulta .= ", $l.".$campoNombre." as nombreUsuario, $l.".$campoId." as userID";

        $consulta .= " FROM Pacientes P";

        if($tipo !== false) {
            $consulta .= " LEFT JOIN " . $tabla . " U
                ON P.".$idEnPaciente." = U.".$campoId;
        }

        $consulta .= " WHERE";

    	if(!empty($fechaInicio)){
            $aux = DateTime::createFromFormat('Y-m-d', $fechaInicio);
            $fechaInicio = $aux->format('Ymd');
            $consulta .= " P.Fecha_Fld >= '".$fechaInicio."'";
        }

        if(!empty($fechaFin)){
            $aux = DateTime::createFromFormat('Y-m-d', $fechaFin);
            $fechaFin = $aux->format('Ymd');
            $consulta .= " AND P.Fecha_Fld <= '".$fechaFin."'";
        }

        if($tipo != 'lorigen') {
            if($tipo !== false)
                $consulta .= " AND $l.".$campoId." = ".$id;
        } else {
            $consulta .= " AND $l.".$campoId." = '".$id."'";
        }

        $consulta .= " LIMIT 0, 100";

        //echo $consulta;exit;

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->execute();

        $preparedQuery->store_result();


        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {

                $resultados = $this->_db->fetchArray($preparedQuery);

                foreach ($resultados as &$resultado) {
                    $arrayResult = $this->getEstado($resultado);
                    $resultado['estado'] = $arrayResult['estado'];
                    if(isset($arrayResult['VecesImp_Fld']))
                        $resultado['VecesImp_Fld'] = $arrayResult['VecesImp_Fld'];
                }
                return $resultados;
            } else {
                return false;
            }
        }
    }

    public function getDerivacionesFiltrosDos($envio, $protocolo, $dni){

        $preparedQuery = $this->_db->stmt_init();

        if ($envio != '') {
            $consulta = 'SELECT E.*, COUNT(P.Numero_Fld) as cantder, (SELECT COUNT(*)
                FROM Pacientes WHERE Importado_Fld != 2 and NumEnvio_Fld = ?) as ingder
                FROM Envios E INNER JOIN Pacientes P ON E.id = P.NumEnvio_Fld
                WHERE P.NumDeriv_Fld <> 0 AND E.id = '.$envio;

            $preparedQuery->prepare($consulta);
            $preparedQuery->bind_param("s", $envio);
        } else{
            /*$consulta = 'SELECT P.*, D.Nombre_Fld as nombreColega, D.Numero_Fld as userID
                FROM Pacientes P LEFT JOIN DerBioq D
                ON P.NumDeriv_Fld = D.Numero_Fld';*/

            $consulta = 'SELECT DISTINCT *
                FROM Pacientes';

            $original = $consulta;
            if ($protocolo != '') {
                $consulta .= " WHERE ImportNumOrig_Fld LIKE '%" . $protocolo . "%'";
                if ($dni != '' && $dni != 0) {
                    $consulta .= " AND HClin_Fld LIKE '%" . $dni . "%'";
                }
            } else {
                if ($dni != '' && $dni != 0) {
                    $consulta .= " WHERE HClin_Fld LIKE '%" . $dni . "%'";
                }
            }

            if ($original == $consulta) {
                return false;
            }

            $consulta .= " ORDER BY Fecha_Fld DESC";

            $preparedQuery->prepare($consulta);

        }

        $preparedQuery->execute();

        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $resultados = $this->_db->fetchArray($preparedQuery);
                return $resultados;
            } else {
                return false;
            }
        }
    }

    public function getSinIngresar($idremito){
        $consulta = 'SELECT * FROM Envios WHERE id = ?';

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);
        $preparedQuery->bind_param("i", $idremito);

        $preparedQuery->execute();

        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $aux = $this->_db->fetchArray($preparedQuery);
                $resultado['datos'] = $aux[0];
                $resultado['datos']['derivante'] = $this->getDerivante($aux[0]['idu']);
                $resultado['derivaciones'] = $this->getDerivaciones($idremito);
                return $resultado;
            } else {
                return false;
            }
        }
    }

    public function getDerivante($idu){
        $consulta  = 'SELECT * FROM DerBioq WHERE IDU_Fld = ?';

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);
        $preparedQuery->bind_param("i", $idu);

        $preparedQuery->execute();

        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $derivante = $this->_db->fetchArray($preparedQuery);
                return $derivante[0];
            } else {
                return false;
            }
        }
    }

    public function getDerivaciones($idremito){
        $consulta = 'SELECT * FROM Pacientes WHERE Importado_Fld !=2 AND NumEnvio_Fld = ?';

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);
        $preparedQuery->bind_param("i", $idremito);

        $preparedQuery->execute();

        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $derivaciones = $this->_db->fetchArray($preparedQuery);
                foreach ($derivaciones as &$derivacion) {
                    $derivacion['determinaciones'] = $this->getDeterminaciones($derivacion['Numero_Fld']);
                }
                return $derivaciones;
            } else {
                return false;
            }
        }
    }

    public function getDeterminaciones($numero_fld){

        $consulta = "SELECT * FROM Deters WHERE  Numero_Fld = ?";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("i", $numero_fld);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $abrevs = $this->_db->fetchArray($preparedQuery);
                $abrevsFinal = '';
                $isFirst = true;
                foreach ($abrevs as $abr) {
                    if ($isFirst) {
                        $abrevsFinal = $abr['Abrev_Fld'];
                        $isFirst = false;
                    } else {
                        $abrevsFinal .= ', ' . $abr['Abrev_Fld'];
                    }

                }
                return $abrevsFinal;
            } else {
                return false;
            }
        }
    }

    /* Funciones para ver la determinación */

    public function getDerivacion($sucursal, $idderivacion){

        $consulta = "SELECT *
                FROM Pacientes WHERE Sucursal_Fld = ? AND Numero_Fld = ?";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("ii", $sucursal, $idderivacion);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {

            if ($preparedQuery->num_rows > 0) {
                $derivacion = $this->_db->fetchArray($preparedQuery);

                if(Session::get('role') != 6) {
                    $this->sumImpreso($sucursal, $idderivacion, $role, $iduser, $derivacion[0]['VecesImp_Fld']);
                }
                return $derivacion;
            } else {
                return false;
            }
        }

    }

    public function getResultado($sucursal, $idderivacion){
        $isIni = false;
        $derivacion = $this->getDerivacion($sucursal, $idderivacion);
        $determinaciones = $this->getDeterminacionesList($idderivacion, $sucursal);

        $finalArray = array();
        $excluidos = array();
        $todosLasDeters = array();
        foreach ($determinaciones as $determinacion) {
            if (substr($determinacion['Result_Fld'], 0, 5) == "[INI]") {
                $aux = array();
                $stringToParse = str_replace("[][", "\n\n[", $determinacion['Result_Fld']);

                $resultado = str_replace("[]", "\n", $stringToParse);
                $resultado = str_replace("]", "]\n", $resultado);
                $resultado = str_replace("[INI]", "", $resultado);

                $arrayAux = explode("[PIE]", $resultado);

                $pie = $arrayAux[1];

                $arrayResultados = explode("[RESULTADO]", $resultado);

                $cabecera = $arrayResultados[0];
                unset($arrayResultados[0]);

                $cabecera = $this->customSpecialCharsDecoder(parse_ini_string($this->customSpecialCharsEncoder($cabecera)));

                $aux['encabezado'] = $cabecera['ENCABEZADO'];
                $aux['id'] = $cabecera['ABREV'];
                $orden = 0;
                foreach ($arrayResultados as $res) {
                    $res = str_replace('RAWRESULT=', 'RAWRESULT= ', $res);
                    $r = parse_ini_string($this->customSpecialCharsEncoder(str_ireplace("&r", "",$res)));
                    switch ($r['TIPO']) {
                        case 'RV':
                            $aux['resultados'][$orden]['TIPO'] = $this->customSpecialCharsDecoder($r['TIPO']);
                            $aux['resultados'][$orden]['TEXTO_RESULTADO'] = $this->customSpecialCharsDecoder($r['TEXTO']);
                            $aux['resultados'][$orden]['RESULTADO'] = $this->customSpecialCharsDecoder($r['RESULTADO']);
                            $aux['resultados'][$orden]['RAWRESULT'] = $this->customSpecialCharsDecoder($r['RAWRESULT']);
                            $aux['resultados'][$orden]['UNIDADES'] = $this->customSpecialCharsDecoder($r['UNIDADES']);
                            $aux['resultados'][$orden]['METODO'] = $this->customSpecialCharsDecoder($r['METODO']);
                            $aux['resultados'][$orden]['MATERIAL'] = $this->customSpecialCharsDecoder($r['MATERIAL']);
                            isset($r['VALNOR']) ? $aux['resultados'][$orden]['VALOR_REFERENCIA'] = $this->customSpecialCharsDecoder(trim($r['VALNOR'])) : $aux['resultados'][$orden]['VALOR_REFERENCIA'] = '';
                            $aux['resultados'][$orden]['NUEVA_LINEA'] = $this->customSpecialCharsDecoder($r['NEWLINE']);
                            $aux['resultados'][$orden]['ANTERIORES'] = false;

                            $aux['resultados'][$orden]['PIE'] = $this->customSpecialCharsDecoder($pie);
                            break;
                    }
                    $orden += 1;
                }

                /*echo "<pre>";
                print_r($aux);
                echo "</pre>";
                exit;*/

                $todosLasDeters[$cabecera['ABREV']] = $aux;

                $isIni = true;

            } else {
                $aux = array();

                if (trim($determinacion['Result_Fld']) != '') {

                    $xmlObject = simplexml_load_string($determinacion['Result_Fld'], null, LIBXML_NOCDATA);

                    $aux['encabezado'] = (String) $xmlObject->ANALISIS->ENCABEZADO;
                    $aux['id'] = (String) $xmlObject->ANALISIS->ID;

                    $anteriores = $this->getAnteriores($sucursal, $idderivacion, $aux['id']);
                    foreach ($xmlObject->ANALISIS->RESULTADOS as $resultado) {
                        $resultado = (array) $resultado;
                        foreach ($resultado as $res) {
                            $res = (array) $res;
                            if (!isset($res[0]) || !$res[0] instanceOf SimpleXMLElement) {

                                //Acá Empieza la cagada

                                $aux['resultados'][0]['TIPO'] = (String) $res['TIPO'];
                                switch ($res['TIPO']) {
                                    case 'RV':
                                        $aux['resultados'][0]['TEXTO_RESULTADO'] = (String) $res['TEXTO_RESULTADO'];
                                        $aux['resultados'][0]['RESULTADO'] = (String) $res['RESULTADO'];
                                        $aux['resultados'][0]['UNIDADES'] = (String) $res['UNIDADES'];
                                        $aux['resultados'][0]['METODO'] = (String) $res['METODO'];
                                        $aux['resultados'][0]['MATERIAL'] = (String) $res['MATERIAL'];
                                        $aux['resultados'][0]['VALOR_REFERENCIA'] = (String) $res['VALOR_REFERENCIA'];
                                        $aux['resultados'][0]['NUEVA_LINEA'] = (String) $res['NUEVA_LINEA'];
                                        $aux['resultados'][0]['ANTERIORES'] = false;
                                        if (is_array($anteriores)) {
                                            $aux['resultados'][0]['ANTERIORES'] = $this->getAnterior($anteriores, $aux['resultados'][0]['TEXTO_RESULTADO'], $res['TIPO'], $aux['resultados'][0]['NUEVA_LINEA'], $sucursal, $idderivacion, $aux['id']);
                                        }

                                        if (isset($res['PIE'])) {
                                            $aux['resultados'][0]['PIE'] = (String)$res['PIE'];
                                        } else {
                                            $aux['resultados'][0]['PIE'] = '';
                                        }
                                        break;

                                    case 'I':
                                        $abrev = (String) $res['INCLUIDOS']->INCLUIDO;
                                        $incluido = $this->getIncluido($idderivacion, $sucursal, $abrev);
                                        if ($incluido != false) {
                                            $excluidos[$abrev] = $abrev;
                                            $inc = simplexml_load_string($incluido[0]['Result_Fld'], null, LIBXML_NOCDATA);
                                            foreach ($inc->ANALISIS->RESULTADOS as $incResultado) {
                                                $incResultado = (array) $incResultado;
                                                $incResultadoArray = (array) $incResultado['RESULTADO'];
                                                if (!isset($incResultadoArray[0]) || !$incResultadoArray[0] instanceOf SimpleXMLElement) {
                                                    $aux['resultados'][0]['RESULTADOS'][0]['TEXTO_RESULTADO'] = (String) $incResultadoArray['TEXTO_RESULTADO'];
                                                    $aux['resultados'][0]['RESULTADOS'][0]['RESULTADO'] = (String) $incResultadoArray['RESULTADO'];
                                                    $aux['resultados'][0]['RESULTADOS'][0]['UNIDADES'] = (String) $incResultadoArray['UNIDADES'];
                                                    $aux['resultados'][0]['RESULTADOS'][0]['METODO'] = (String) $incResultadoArray['METODO'];
                                                    $aux['resultados'][0]['RESULTADOS'][0]['MATERIAL'] = (String) $incResultadoArray['MATERIAL'];
                                                    $aux['resultados'][0]['RESULTADOS'][0]['VALOR_REFERENCIA'] = (String) $incResultadoArray['VALOR_REFERENCIA'];
                                                    $aux['resultados'][0]['RESULTADOS'][0]['NUEVA_LINEA'] = (String) $incResultadoArray['NUEVA_LINEA'];
                                                    $aux['resultados'][0]['RESULTADOS'][0]['ANTERIORES'] = false;
                                                    if (is_array($anteriores)) {
                                                        $aux['resultados'][0]['RESULTADOS'][0]['ANTERIORES'] = $this->getAnterior($anteriores, $aux['resultados'][0]['RESULTADOS'][0]['TEXTO_RESULTADO'], $res['TIPO'], $aux['resultados'][0]['RESULTADOS'][0]['NUEVA_LINEA'], $sucursal, $idderivacion, $aux['id']);
                                                    }
                                                } else {
                                                    $incIA = 0;
                                                    foreach ($incResultadoArray as $incResultadoArrayResult) {
                                                        $incResultadoArrayResult = (array) $incResultadoArrayResult;

                                                        $aux['resultados'][0]['RESULTADOS'][$incIA]['TEXTO_RESULTADO'] = (String) $incResultadoArrayResult['TEXTO_RESULTADO'];
                                                        $aux['resultados'][0]['RESULTADOS'][$incIA]['RESULTADO'] = (String) $incResultadoArrayResult['RESULTADO'];
                                                        $aux['resultados'][0]['RESULTADOS'][$incIA]['UNIDADES'] = (String) $incResultadoArrayResult['UNIDADES'];
                                                        $aux['resultados'][0]['RESULTADOS'][$incIA]['METODO'] = (String) $incResultadoArrayResult['METODO'];
                                                        $aux['resultados'][0]['RESULTADOS'][$incIA]['MATERIAL'] = (String) $incResultadoArrayResult['MATERIAL'];
                                                        $aux['resultados'][0]['RESULTADOS'][$incIA]['VALOR_REFERENCIA'] = (String) $incResultadoArrayResult['VALOR_REFERENCIA'];
                                                        $aux['resultados'][0]['RESULTADOS'][$incIA]['NUEVA_LINEA'] = (String) $incResultadoArrayResult['NUEVA_LINEA'];
                                                        $aux['resultados'][0]['RESULTADOS'][$incIA]['ANTERIORES'] = false;
                                                        if (is_array($anteriores)) {
                                                            $aux['resultados'][0]['RESULTADOS'][$incIA]['ANTERIORES'] = $this->getAnterior($anteriores, $aux['resultados'][0]['RESULTADOS'][$incIA]['TEXTO_RESULTADO'], $res['TIPO'], $aux['resultados'][0]['RESULTADOS'][$incIA]['NUEVA_LINEA'], $sucursal, $idderivacion, $aux['id']);
                                                        }
                                                        $incIA += 1;
                                                    }


                                                }
                                                $aux['resultados'][0]['PIE'] = (String) $inc->ANALISIS->PIE;
                                            }
                                        }
                                        break;

                                    case 'T':
                                        $aux['resultados'][0]['RESULTADOS'][0]['TEXTO_RESULTADO'] = (String) $r['TEXTO_RESULTADO'];
                                        break;

                                    case 'IM':
                                        $aux['resultados'][0]['IMAGENES'] = 'imagenes';
                                        /*foreach ($res['IMAGEN'] as $imagen) {
                                            $aux['resultados'][0]['IMAGENES'][] = (String)$imagen['IMAGEN'];
                                        }*/
                                        break;
                                }

                                //Acá termina la cagada

                            } else {
                                $i = 0;
                                foreach ($res as $r) {
                                    $r = (array) $r;
                                    $aux['resultados'][$i]['TIPO'] = (String) $r['TIPO'];
                                    switch ($r['TIPO']) {
                                        case 'RV':
                                            $aux['resultados'][$i]['TEXTO_RESULTADO'] = (String) $r['TEXTO_RESULTADO'];
                                            $aux['resultados'][$i]['RESULTADO'] = (String) $r['RESULTADO'];
                                            $aux['resultados'][$i]['UNIDADES'] = (String) $r['UNIDADES'];
                                            $aux['resultados'][$i]['METODO'] = (String) $r['METODO'];
                                            $aux['resultados'][$i]['MATERIAL'] = (String) $r['MATERIAL'];
                                            $aux['resultados'][$i]['VALOR_REFERENCIA'] = (String) $r['VALOR_REFERENCIA'];
                                            $aux['resultados'][$i]['NUEVA_LINEA'] = (String) $r['NUEVA_LINEA'];
                                            $aux['resultados'][$i]['ANTERIORES'] = false;
                                            if (is_array($anteriores)) {
                                                $aux['resultados'][$i]['ANTERIORES'] = $this->getAnterior($anteriores, $aux['resultados'][$i]['TEXTO_RESULTADO'], $r['TIPO'], $aux['resultados'][$i]['NUEVA_LINEA'], $sucursal, $idderivacion, $aux['id']);
                                            }
                                            $aux['resultados'][$i]['PIE'] = array_key_exists('PIE', $r) ? (String) $r['PIE'] : '';
                                            break;

                                        case 'I':
                                            $abrev = (String) $r['INCLUIDOS']->INCLUIDO;
                                            $incluido = $this->getIncluido($idderivacion, $sucursal, $abrev);
                                            if ($incluido != false) {
                                                $excluidos[$abrev] = $abrev;
                                                $inc = simplexml_load_string($incluido[0]['Result_Fld'], null, LIBXML_NOCDATA);

                                                foreach ($inc->ANALISIS->RESULTADOS as $incResultado) {
                                                    $incResultado = (array) $incResultado;
                                                    $incResultadoArray = (array) $incResultado['RESULTADO'];
                                                    if (!isset($incResultadoArray[0]) || !$incResultadoArray[0] instanceOf SimpleXMLElement) {
                                                        $aux['resultados'][$i]['RESULTADOS'][0]['TEXTO_RESULTADO'] = (String) $incResultadoArray['TEXTO_RESULTADO'];
                                                        $aux['resultados'][$i]['RESULTADOS'][0]['RESULTADO'] = (String) $incResultadoArray['RESULTADO'];
                                                        $aux['resultados'][$i]['RESULTADOS'][0]['UNIDADES'] = (String) $incResultadoArray['UNIDADES'];
                                                        $aux['resultados'][$i]['RESULTADOS'][0]['METODO'] = (String) $incResultadoArray['METODO'];
                                                        $aux['resultados'][$i]['RESULTADOS'][0]['MATERIAL'] = (String) $incResultadoArray['MATERIAL'];
                                                        $aux['resultados'][$i]['RESULTADOS'][0]['VALOR_REFERENCIA'] = (String) $incResultadoArray['VALOR_REFERENCIA'];
                                                        $aux['resultados'][$i]['RESULTADOS'][0]['NUEVA_LINEA'] = (String) $incResultadoArray['NUEVA_LINEA'];
                                                        $aux['resultados'][$i]['RESULTADOS'][0]['ANTERIORES'] = false;
                                                        if (is_array($anteriores)) {
                                                            $aux['resultados'][$i]['RESULTADOS'][0]['ANTERIORES'] = $this->getAnterior($anteriores, $aux['resultados'][$i]['RESULTADOS'][0]['TEXTO_RESULTADO'], $r['TIPO'], $aux['resultados'][$i]['RESULTADOS'][0]['NUEVA_LINEA'], $sucursal, $idderivacion, $abrev);

                                                        }
                                                    } else {
                                                        $incIA = 0;
                                                        foreach ($incResultadoArray as $incResultadoArrayResult) {
                                                            $incResultadoArrayResult = (array) $incResultadoArrayResult;

                                                            $aux['resultados'][$i]['RESULTADOS'][$incIA]['TEXTO_RESULTADO'] = (String) $incResultadoArrayResult['TEXTO_RESULTADO'];
                                                            $aux['resultados'][$i]['RESULTADOS'][$incIA]['RESULTADO'] = (String) $incResultadoArrayResult['RESULTADO'];
                                                            $aux['resultados'][$i]['RESULTADOS'][$incIA]['UNIDADES'] = (String) $incResultadoArrayResult['UNIDADES'];
                                                            $aux['resultados'][$i]['RESULTADOS'][$incIA]['METODO'] = (String) $incResultadoArrayResult['METODO'];
                                                            $aux['resultados'][$i]['RESULTADOS'][$incIA]['MATERIAL'] = (String) $incResultadoArrayResult['MATERIAL'];
                                                            $aux['resultados'][$i]['RESULTADOS'][$incIA]['VALOR_REFERENCIA'] = (String) $incResultadoArrayResult['VALOR_REFERENCIA'];
                                                            $aux['resultados'][$i]['RESULTADOS'][$incIA]['NUEVA_LINEA'] = isset($incResultadoArray['NUEVA_LINEA']) ? (String) $incResultadoArray['NUEVA_LINEA'] : '0';
                                                            $aux['resultados'][$i]['RESULTADOS'][$incIA]['ANTERIORES'] = false;
                                                            if (is_array($anteriores)) {
                                                                $aux['resultados'][$i]['RESULTADOS'][$incIA]['ANTERIORES'] = $this->getAnterior($anteriores, $aux['resultados'][$i]['RESULTADOS'][$incIA]['TEXTO_RESULTADO'], $r['TIPO'], $aux['resultados'][$i]['RESULTADOS'][$incIA]['NUEVA_LINEA'], $sucursal, $idderivacion, $abrev);
                                                            }
                                                            $incIA += 1;
                                                        }


                                                    }
                                                    $aux['resultados'][$i]['PIE'] = (String) $inc->ANALISIS->PIE;
                                                }
                                            }
                                            break;

                                        case 'T':
                                            $aux['resultados'][$i]['RESULTADOS'][0]['TEXTO_RESULTADO'] = (String) $r['TEXTO_RESULTADO'];
                                            break;

                                        case 'IM':

                                            $aux['resultados'][$i]['IMAGENES'][] = (String) $r['IMAGENES']->IMAGEN;
                                            break;
                                    }

                                    $i += 1;
                                }
                            }
                        }

                        $todosLasDeters[$aux['id']] = $aux;

                    }


                } else {

                    $aux['encabezado'] = $determinacion['Nombre_Fld'];
                    $aux['id'] = $determinacion['Abrev_Fld'];
                    $aux['resultados'][0]['TIPO'] = 'RV';
                    $aux['resultados'][0]['TEXTO_RESULTADO'] = $determinacion['Nombre_Fld'];
                    $aux['resultados'][0]['RESULTADO'] = 'En proceso...';
                    $aux['resultados'][0]['UNIDADES'] = '';
                    $aux['resultados'][0]['METODO'] = '';
                    $aux['resultados'][0]['MATERIAL'] = '';
                    $aux['resultados'][0]['VALOR_REFERENCIA'] = '';
                    $aux['resultados'][0]['NUEVA_LINEA'] = '';
                    $aux['resultados'][0]['ANTERIORES'] = false;

                    if(!empty($res['PIE'])) {
                        $aux['resultados'][0]['PIE'] = (String)$res['PIE'];
                    } else {
                        $aux['resultados'][0]['PIE'] = '';
                    }

                    $todosLasDeters[$aux['id']] = $aux;
                }
            }


        }

        $finalArray['datos'] = $this->getDatosDerivacion($idderivacion, $sucursal);
        $finalArray['textos'] = $this->getTextos();

        if(!$isIni){
            $finalArray['determinaciones'] = $this->limpiarArrayDeters($todosLasDeters, $excluidos);
        } else {
            $finalArray['determinaciones'] = $todosLasDeters;
        }

        //print_r($finalArray);

        return $finalArray;
    }

    public function getAnteriores($sucursal, $idderivacion, $abrev){

        //Revisa si HCLIN es != 0
        $consulta = "SELECT HClin_Fld FROM Pacientes WHERE Sucursal_Fld = ? AND Numero_Fld = ?";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("ii", $sucursal, $idderivacion);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $hclin = $this->_db->fetchArray($preparedQuery);
            }
        }

        if ($hclin[0]['HClin_Fld'] == 0 || $hclin[0]['HClin_Fld'] == "0") {
            return false;
        }

        $hclin = (int) $hclin[0]['HClin_Fld'];


        //Si no lo es busca las derivaciones históricas
        $consulta = "SELECT Sucursal_Fld, Numero_Fld, Fecha_Fld FROM Pacientes WHERE HClin_Fld = ?
                    AND Sucursal_Fld = ? AND Numero_Fld <> ?
                    ORDER BY Fecha_Fld DESC";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("iii", $hclin, $sucursal, $idderivacion);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $derivacionesHistoricas = $this->_db->fetchArray($preparedQuery);
            } else {
                return false;
            }
        }

        $finalArray = array();
        $x = 0;
        //Si existen las históricas, busca su respectivos valores
        foreach ($derivacionesHistoricas as $dH) {
            $consulta = "SELECT * FROM Deters
                        WHERE Sucursal_Fld = ?
                        AND Numero_Fld = ?
                        AND Abrev_Fld = ?";

            $preparedQuery = $this->_db->stmt_init();

            $preparedQuery->prepare($consulta);

            $preparedQuery->bind_param("iis", $dH['Sucursal_Fld'], $dH['Numero_Fld'], $abrev);

            $preparedQuery->execute();
            $preparedQuery->store_result();

            if ($preparedQuery->error != "") {
                return false;
            } else {
                if ($preparedQuery->num_rows > 0) {
                    $determinaciones = $this->_db->fetchArray($preparedQuery);
                } else {
                    $determinaciones = [];
                }
            }

            foreach ($determinaciones as $deter) {
                $finalArray[$deter['Abrev_Fld']][$x]['fecha'] = $dH['Fecha_Fld'];
                $xml = simplexml_load_string($deter['Result_Fld'], null, LIBXML_NOCDATA);
                $finalArray[$deter['Abrev_Fld']][$x]['resultados'] = $xml->ANALISIS->RESULTADOS;
                $x++;
            }
        }

        return $finalArray;

    }

    public function getAnterior($anteriores, $texto, $tipo, $nueva_linea, $sucursal, $idderivacion, $abrev){
        if ($tipo == "I") {

            $anteriores = $this->getAnteriores($sucursal, $idderivacion, $abrev);

        }

        if (str_replace('&r', '', $texto) != '') {
            $finalArray = array();

            if(count($anteriores)) {

                $x = 0;
                foreach ($anteriores[$abrev] as $key => $anterior) {
                    foreach ($anterior['resultados'] as  $resultado) {
                        $id = uniqid();
                        if (count($resultado) > 1) {
                            foreach ($resultado->RESULTADO as $r) {
                                $texto_resultado = (string)$r->TEXTO_RESULTADO;
                                if ($nueva_linea != "1") {
                                    if (str_replace('&r', '', $texto_resultado) != '') {

                                        if ($texto == $texto_resultado) {
                                            /*$finalArray[$anterior['fecha']] = [];
                                            array_push($finalArray[$anterior['fecha']], [
                                                'fecha' => $anterior['fecha'],
                                                'valor1' => (string)$r->RESULTADO,
                                                'unidades' => (string)$r->UNIDADES,
                                            ]);*/
                                            $finalArray[$key]['fecha'] = $anterior['fecha'];
                                            $finalArray[$key]['valor1'] = (string)$r->RESULTADO;
                                            $finalArray[$key]['unidades'] = (string)$r->UNIDADES;
                                        }
                                    } else {

                                        if (isset($finalArray[$key]['valor1'])) {
                                            /*array_push($finalArray[$anterior['fecha']], [
                                                'valor2' => (string)$r->RESULTADO,
                                            ]);*/

                                            $finalArray[$key]['valor2'] = (string)$r->RESULTADO;
                                            break;
                                        }

                                    }
                                } else {
                                    if ($texto == $texto_resultado) {
                                        /*$finalArray[$anterior['fecha']] = [];
                                        array_push($finalArray[$anterior['fecha']], [
                                            'fecha' => $anterior['fecha'],
                                            'valor1' => (string)$r->RESULTADO,
                                            'unidades' => (string)$r->UNIDADES,
                                        ]);*/

                                        $finalArray[$key]['fecha'] = $anterior['fecha'];
                                        $finalArray[$key]['valor1'] = (string)$r->RESULTADO;
                                        $finalArray[$key]['unidades'] = (string)$r->UNIDADES;
                                        break;
                                    }
                                }
                            }
                        } else {
                            $texto_resultado = (string)$resultado->RESULTADO->TEXTO_RESULTADO;
                            if ($nueva_linea != "1") {
                                $lastDate = $anterior['fecha'];
                                if (str_replace('&r', '', $texto_resultado) != '') {
                                    if ($texto == $texto_resultado) {
                                        /*$finalArray[$anterior['fecha']] = [];
                                        array_push($finalArray[$anterior['fecha']], [
                                            'fecha' => $lastDate,
                                            'valor1' => (string)$resultado->RESULTADO->RESULTADO,
                                            'unidades' => (string)$resultado->RESULTADO->UNIDADES,
                                        ]);*/
                                        $finalArray[$key]['fecha'] = $lastDate;
                                        $finalArray[$key]['valor1'] = (string)$resultado->RESULTADO->RESULTADO;
                                        $finalArray[$key]['unidades'] = (string)$resultado->RESULTADO->UNIDADES;
                                        break;
                                    }
                                } else {
                                    /*$finalArray[$lastDate] = [];
                                    array_push($finalArray[$lastDate], [
                                        'valor2' => (string)$resultado->RESULTADO->RESULTADO,
                                    ]);
                                    $finalArray[$lastDate]['valor2'] = (string)$resultado->RESULTADO->RESULTADO;*/
                                    $finalArray[$key]['valor2'] = (string)$resultado->RESULTADO->RESULTADO;
                                }
                            } else {
                                if ($texto == $texto_resultado) {
                                    /*$finalArray[$anterior['fecha']] = [];
                                    array_push($finalArray[$anterior['fecha']], [
                                        'fecha' => $anterior['fecha'],
                                        'valor1' => (string)$resultado->RESULTADO->RESULTADO,
                                        'unidades' => (string)$resultado->RESULTADO->UNIDADES,
                                    ]);*/
                                    $finalArray[$key]['fecha'] = $anterior['fecha'];
                                    $finalArray[$key]['valor1'] = (string)$resultado->RESULTADO->RESULTADO;
                                    $finalArray[$key]['unidades'] = (string)$resultado->RESULTADO->UNIDADES;
                                    break;
                                }
                            }
                        }

                    }

                }
                $x++;
            }


            return $finalArray;
        } else {
            return false;
        }
    }

    public function getDeterminacionesList($idderivacion, $sucursal){
        $consulta = "SELECT DISTINCT D.Abrev_Fld, N.Nombre_Fld, D.TrajoMuestra_Fld, D.Result_Fld,  D.Orden_Fld FROM Deters D INNER JOIN Nomen N ON D.Abrev_Fld COLLATE latin1_general_cs = N.Abrev_Fld COLLATE latin1_general_cs WHERE D.Sucursal_Fld = ? AND D.Numero_Fld = ? ORDER BY D.Orden_Fld";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("ii", $sucursal, $idderivacion);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $determinaciones = $this->_db->fetchArray($preparedQuery);
            } else {
                $determinaciones = [];
            }
        }

        return $determinaciones;
    }

    public function getIncluido($iddeterminacion, $sucursal, $abrev){
        $consulta = "SELECT D.Abrev_Fld, N.Nombre_Fld, D.Result_Fld FROM Deters D LEFT JOIN Nomen N ON D.Abrev_Fld = N.Abrev_Fld WHERE D.Numero_Fld like ? AND D.Sucursal_Fld = ? AND D.Abrev_Fld = ? LIMIT 1";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("iis", $iddeterminacion, $sucursal, $abrev);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {

            if ($preparedQuery->num_rows > 0) {
                $incluido = $this->_db->fetchArray($preparedQuery);
            } else {
                $incluido = false;
            }
        }

        return $incluido;
    }

    public function limpiarArrayDeters($todosLasDeters, $excluidos){
        $finalArray = array();
        foreach ($todosLasDeters as $deter) {
            if (!$this->esExcluido($deter['id'], $excluidos)) {
                $finalArray[$deter['id']] = $deter;
            }
        }
        return $finalArray;
    }

    public function esExcluido($abrev, $excluidos){
        return in_array($abrev, $excluidos);
    }

    public function getDatosDerivacion($idderivacion, $sucursal){
        $consulta = "SELECT p.Nombre_Fld as Paciente, p.Fecha_Fld as Fecha, p.ImportNumOrig_Fld as Protocolo";

        /*switch (Session::get('role')) {
            //Bioquímico
            case 1:
                $consulta .= ", s.Nombre_Fld as Solicita FROM Pacientes p INNER JOIN DerBioq s ON p.NumDeriv_Fld = s.Numero_Fld";
                break;

            //Médico
            case 2:
                $consulta .= ", s.Nombre_Fld as Solicita FROM Pacientes p INNER JOIN Medicos s ON p.NumMedico_Fld = s.Numero_Fld";
                break;

            //Paciente
            case 3:
                $consulta .= ", p.Nombre_Fld as Solicita FROM Pacientes p";
                break;

            //Obra Social
            case 4:
                $consulta .= ", s.Nombre_Fld as Solicita FROM Pacientes p INNER JOIN ObraSoc s ON p.Mutual_Fld = s.Numero_Fld";
                break;

            //LOrigen TODO CAMBIAR NOMBRE_FLD
            case 5:
                $consulta .= ", s.NomLab_Fld as Solicita FROM Pacientes p INNER JOIN LOrigen s ON p.Origen_Fld = s.Abrev_Fld";
                break;

            case 6:
                $consulta .= ", s.Nombre_Fld as Solicita FROM Pacientes p INNER JOIN DerBioq s ON p.NumDeriv_Fld = s.Numero_Fld";
                break;

            default:
                break;
        }*/

        $consulta .= " FROM Pacientes p WHERE p.Numero_Fld = ? AND p.Sucursal_Fld = ?";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("ii", $idderivacion, $sucursal);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $datos = $this->_db->fetchArray($preparedQuery);
            } else {
                $datos = false;
            }
        }

        return $datos;
    }

    public function getTextos(){
        $consulta = "SELECT * FROM textos";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $textos = $this->_db->fetchArray($preparedQuery);
            } else {
                $textos = false;
            }
        }

        return $textos;
    }

    public function customSpecialCharsEncoder($string)
    {
        $parsedString = $string;

        //Caracteres especiales
        //Palabras reservadas
        $parsedString = str_replace('no', '#n_o#', $parsedString);
        $parsedString = str_replace('No', '#N_o#', $parsedString);
        $parsedString = str_replace('NO', '#N_O#', $parsedString);

        $parsedString = str_replace('true', '#tr_ue#', $parsedString);
        $parsedString = str_replace('TRUE', '#TR_UE#', $parsedString);
        $parsedString = str_replace('True', '#Tr_ue#', $parsedString);

        $parsedString = str_replace('false', '#fal_se#', $parsedString);
        $parsedString = str_replace('FALSE', '#FAL_SE#', $parsedString);
        $parsedString = str_replace('False', '#fal_se#', $parsedString);

        $parsedString = str_replace('on', '#o_n#', $parsedString);
        $parsedString = str_replace('On', '#O_n#', $parsedString);
        $parsedString = str_replace('ON', '#O_N#', $parsedString);

        $parsedString = str_replace('off', '#o_ff#', $parsedString);
        $parsedString = str_replace('Off', '#O_ff#', $parsedString);
        $parsedString = str_replace('OFF', '#O_FF#', $parsedString);

        $parsedString = str_replace('none', '#n_one#', $parsedString);
        $parsedString = str_replace('None', '#N_one#', $parsedString);
        $parsedString = str_replace('NONE', '#N_ONE#', $parsedString);

        $parsedString = str_replace('?', '#signo_pregunta#', $parsedString);
        $parsedString = str_replace('{', '#llave_abre#', $parsedString);
        $parsedString = str_replace('}', '#llave_cierra#', $parsedString);
        $parsedString = str_replace('|', '#barra_vertical#', $parsedString);
        $parsedString = str_replace('&', '#ampersand#', $parsedString);
        $parsedString = str_replace('~', '#tilde#', $parsedString);
        $parsedString = str_replace('!', '#signo_exclamacion#', $parsedString);
        $parsedString = str_replace('[', '#corchete_abre#', $parsedString);
        $parsedString = str_replace('(', '#parentesis_abre#', $parsedString);
        $parsedString = str_replace(')', '#parentesis_cierra#', $parsedString);
        $parsedString = str_replace('^', '#acento_circunflejo#', $parsedString);
        $parsedString = str_replace('"', '#comilla#', $parsedString);
        $parsedString = str_replace('$', '#signo_peso#', $parsedString);
        $parsedString = str_replace('+', '#signo_mas#', $parsedString);
        $parsedString = str_replace(':', '#dos_puntos#', $parsedString);

        return $parsedString;

    }

    public function customSpecialCharsDecoder($string)
    {
        //Caracteres especiales
        $parsedString = $string;

        $parsedString = str_replace('#signo_pregunta#', '?', $parsedString);
        $parsedString = str_replace('#llave_abre#', '{', $parsedString);
        $parsedString = str_replace('#llave_cierra#', '}', $parsedString);
        $parsedString = str_replace('#barra_vertical#', '|', $parsedString);
        $parsedString = str_replace('#ampersand#', '&', $parsedString);
        $parsedString = str_replace('#tilde#', '~', $parsedString);
        $parsedString = str_replace('#signo_exclamacion#', '!', $parsedString);
        $parsedString = str_replace('#corchete_abre#', '[', $parsedString);
        $parsedString = str_replace('#parentesis_abre#', '(', $parsedString);
        $parsedString = str_replace('#parentesis_cierra#', ')', $parsedString);
        $parsedString = str_replace('#acento_circunflejo#', '^', $parsedString);
        $parsedString = str_replace('#comilla#', '"', $parsedString);
        $parsedString = str_replace('#signo_peso#', '$', $parsedString);
        $parsedString = str_replace('#signo_mas#', '+', $parsedString);
        $parsedString = str_replace('#dos_puntos#', ':', $parsedString);

        //Palabras reservadas
        $parsedString = str_replace('#n_o#', 'no', $parsedString);
        $parsedString = str_replace('#N_o#', 'No', $parsedString);
        $parsedString = str_replace('#N_O#', 'NO', $parsedString);

        $parsedString = str_replace('#tr_ue#', 'true', $parsedString);
        $parsedString = str_replace('#TR_UE#', 'TRUE', $parsedString);
        $parsedString = str_replace('#Tr_ue#', 'True', $parsedString);

        $parsedString = str_replace('#fal_se#', 'false', $parsedString);
        $parsedString = str_replace('#FAL_SE#', 'FALSE', $parsedString);
        $parsedString = str_replace('#fal_se#', 'False', $parsedString);

        $parsedString = str_replace('#o_n#', 'on', $parsedString);
        $parsedString = str_replace('#O_n#', 'On', $parsedString);
        $parsedString = str_replace('#O_N#', 'ON', $parsedString);

        $parsedString = str_replace('#o_ff#', 'off', $parsedString);
        $parsedString = str_replace('#O_ff#', 'Off', $parsedString);
        $parsedString = str_replace('#O_FF#', 'OFF', $parsedString);

        $parsedString = str_replace('#n_one#', 'none', $parsedString);
        $parsedString = str_replace('#N_one#', 'None', $parsedString);
        $parsedString = str_replace('#N_ONE#', 'NONE', $parsedString);

        return $parsedString;

    }

    public function getFormatos(){
        $consulta = "SELECT * FROM FormatoDeterminaciones";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $textos = $this->_db->fetchArray($preparedQuery);
            } else {
                $textos = false;
            }
        }

        return $textos;
    }

    public function getFuente($idFuente){
        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare('SELECT * FROM Fuentes WHERE id = ?');

        $preparedQuery->bind_param('i',$idFuente);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $fuente = $this->_db->fetchArray($preparedQuery)[0];
                return $fuente;
            } else {
                return false;
            }
        }
    }

    public function eliminarResultado($idresultado){
        $consulta = "DELETE FROM Pacientes WHERE Numero_Fld = ?";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("i", $idresultado);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            $consulta = "DELETE FROM Deters WHERE Numero_Fld = ?";

            $preparedQuery = $this->_db->stmt_init();

            $preparedQuery->prepare($consulta);

            $preparedQuery->bind_param("i", $idresultado);

            $preparedQuery->execute();
            $preparedQuery->store_result();

            if ($preparedQuery->error != "") {
                return false;
            } else {
                return true;
            }
        }
    }

    public function modificarNombre($idderivacion, $sucursal, $nombre){
        $consulta = "UPDATE Pacientes SET Nombre_Fld = ? WHERE Numero_Fld = ? AND Sucursal_Fld = ?";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("sii", $nombre, $idderivacion, $sucursal);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            return true;
        }
    }

    public function getDeterminacion($abrev){
        $preparedQuery = $this->_db->stmt_init();

        if ($abrev == 'default'){
            $determinacion['Abrev_Fld'] = 'DEFAULT';
            return $determinacion;
        } else {
            $consulta = "SELECT * FROM Nomen WHERE Abrev_Fld = '".$abrev."'";
        }

        $preparedQuery->prepare($consulta);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $determinacion = $this->_db->fetchArray($preparedQuery)[0];
                return $determinacion;
            } else {
                return false;
            }
        }
    }

    public function deleteDeterminacion($sucursal, $idderivacion, $abrev){
        $consulta = "DELETE FROM Deters WHERE Sucursal_Fld = ? AND Numero_Fld = ? AND Abrev_Fld = ?";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("iis", $sucursal, $idderivacion, $abrev);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            return true;
        }
    }

    public function getConfigs(){
        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare('SELECT * FROM Configs');

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $configs = $this->_db->fetchArray($preparedQuery);
                $configsAux = [];
                foreach ($configs as $config){
                    $configsAux[$config['config_key']] = $config['config_value'];
                }
                return $configsAux;
            } else {
                return false;
            }
        }
    }

}
