<?php

/*
* Index Controller of Config Module
* /public_html/laboratorios/mobile/modules/login/config/indexController.php
*/

class indexController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->_configModel = $this->loadModel('config');
        $this->_view->setTemplate('admin_template');
        $this->_view->assign('content-classes', array('rol-mol-background'));
    }

    public function index()
    {
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }

        $this->_view->setTemplate('admin_template');

        $this->_acl->access('config');

        $this->_view->assign('titulo', 'Configuración');
        $this->_view->assign('body-classes', array('home-background'));

        $this->_view->render('index', 'config');
    }

    public function resultados()
    {
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }

        $this->_acl->access('config');

        $this->_view->assign('titulo', 'Configuración');
        $this->_view->setJs(array('config'));
        $this->_view->assign('determinaciones', $this->_configModel->getDeterminaciones());
        $this->_view->assign('body-classes', array('home-background'));

        $this->_view->render('resultados', 'config');
    }

    public function formatear($id)
    {
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }

        $this->_acl->access('config');

        $determinacion = $this->_configModel->getDeterminacion(str_replace('_ESP_', ' ', $id));

        $this->_view->assign('titulo', 'Formatear resultado');
        $this->_view->setJs(array('config'));

        if($this->getPostParam('guardar')){
            if (!$this->_configModel->getFormatoDeterminacion($this->getPostParam('Abrev_Fld'))){
                if($this->_configModel->saveFormat(
                    $this->getPostParam('letra_predeterminada'),
                    $this->getPostParam('letra_unidades'),
                    $this->getPostParam('letra_metodos'),
                    $this->getPostParam('letra_resultados'),
                    $this->getPostParam('letra_materiales'),
                    $this->getPostParam('letra_valores_de_referencia'),
                    $this->getPostParam('letra_resultados_anteriores'),
                    0,
                    '#ffffff',
                    $this->getPostParam('Abrev_Fld'),
                    $this->getInt('ActualizarTodos')
                )){
                    $this->redirect('/config');
                }
            } else {
                if ($this->getPostParam('Abrev_Fld') != 'DEFAULT') {
                    if ($this->_configModel->updateFormat(
                        $this->getPostParam('letra_predeterminada'),
                        $this->getPostParam('letra_unidades'),
                        $this->getPostParam('letra_metodos'),
                        $this->getPostParam('letra_resultados'),
                        $this->getPostParam('letra_materiales'),
                        $this->getPostParam('letra_valores_de_referencia'),
                        $this->getPostParam('letra_resultados_anteriores'),
                        $this->getPostParam('Abrev_Fld'),
                        $this->getPostParam('ActualizarTodos')
                    )
                    ) {
                        $this->redirect('/config');
                    }
                } else {
                    if ($this->_configModel->updateFormat(
                        $this->getPostParam('letra_predeterminada'),
                        $this->getPostParam('letra_unidades'),
                        $this->getPostParam('letra_metodos'),
                        $this->getPostParam('letra_resultados'),
                        $this->getPostParam('letra_materiales'),
                        $this->getPostParam('letra_valores_de_referencia'),
                        $this->getPostParam('letra_resultados_anteriores'),
                        $this->getPostParam('Abrev_Fld'),
                        $this->getPostParam('ActualizarTodos'),
                        $this->getPostParam('fondo_resultado'),
                        $this->getInt('TabTexto'),
                        $this->getInt('TabResultados'),
                        $this->getInt('Interlineado'),
                        $this->getPostParam('Fuente')
                    )
                    ) {
                        $this->redirect('/config/index/resultados');
                    }
                }
            }
        }

        $this->_view->assign('determinacion', $determinacion);
        $this->_view->assign('tags', $this->_configModel->getFormatosTags());
        $this->_view->assign('formato_determinacion', $this->_configModel->getFormatoDeterminacion($determinacion['Abrev_Fld'], Session::get('iduser')));
        $this->_view->assign('fuentes', $this->_configModel->getFuentes());
        $this->_view->assign('body-classes', array('home-background'));

        $this->_view->render('formatear_resultado', 'config');
    }

    public function eliminar_formato($id)
    {
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }

        $this->_acl->access('config');

        $determinacion = $this->_configModel->getDeterminacion($id);

        if(!$this->_configModel->getFormatoDeterminacion($determinacion['Abrev_Fld'])){
            $this->redirect('config/index/resultados');
        }

        if($this->_configModel->deleteFormat($determinacion['Abrev_Fld'])){
            $this->redirect('config/index/resultados');
        }
    }

    public function textos()
    {
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }

        $this->_acl->access('config');

        $this->_view->assign('titulo', 'Configuración');
        $this->_view->assign('body-classes', array('home-background'));

        $this->_view->assign('textos', $this->_configModel->getTexts()[0]);

        if($this->getPostParam('guardar')){
            $ruta_imagen = $this->uploadFile('imagen', array('gif','jpg','jpe','jpeg','png'));
            $ruta_firma = $this->uploadFile('firma', array('gif','jpg','jpe','jpeg','png'));

            if (!$ruta_imagen){
                $ruta_imagen = '';
            }

            if (!$ruta_firma){
                $ruta_firma = '';
            }

            $this->_configModel->saveTexts(
                $this->getPostParam('titulo'),
                $this->getPostParam('texto'),
                $this->getPostParam('pie'),
                $ruta_imagen,
                $ruta_firma
            );

            $this->redirect('/config/index/textos');
        }

        $this->_view->render('textos', 'config');
    }

    public function cambiar_password()
    {
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }

        $this->_view->assign('titulo', 'Cambiar contraseña');
        $this->_view->setTemplate('rol_mol_template');

        $this->_view->assign('body-classes', array('home-background'));


        if($this->getPostParam('guardar')){
            if ($this->getPostParam('actual') == '') {
                $this->_view->assign('_error', 'Escriba la contraseña actual, por favor.');
                $this->_view->render('cambiar_password', 'config');
                exit;
            }

            if ($this->getPostParam('nueva') == '') {
                $this->_view->assign('_error', 'Escriba la contraseña nueva, por favor.');
                $this->_view->render('cambiar_password', 'config');
                exit;
            }

            if ($this->getPostParam('nueva') != $this->getPostParam('confirm-nueva')) {
                $this->_view->assign('_error', 'La nueva contraseña no coincide');
                $this->_view->render('cambiar_password', 'config');
                exit;
            }

            if(strlen($this->getPostParam('nueva')) < 6){
                $this->_view->assign('_error', 'La nueva contraseña debe contener al menos 6 caracteres');
                $this->_view->render('cambiar_password', 'config');
                exit;
            }

            if(!$this->_configModel->getUser(Session::get('user'), $this->getPostParam('actual'), Session::get('role'))){
                $this->_view->assign('_error', 'La contraseña actual no es correcta');
                $this->_view->render('cambiar_password', 'config');
                exit;
            }

            $response = $this->_configModel->changePassword(
                $this->getPostParam('nueva'),
                Session::get('iduser'),
                Session::get('role')
            );


            $this->_view->assign('_error', $response);
            $this->_view->render('cambiar_password', 'config');

        }

        $this->_view->render('cambiar_password', 'config');
    }

    /* Configuración de sistema */
    public function sistema()
    {
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }

        $this->_acl->access('config');

        $this->_view->assign('titulo', 'Configuración de sistema');
        $this->_view->assign('body-classes', array('home-background'));

        $this->_view->assign('configs', $this->_configModel->getConfigs());

        if($this->getPostParam('guardar')){

            if($this->_configModel->saveSistemConfigs(
                $this->getPostParam('configs')
            )) $this->redirect('config');
        }

        $this->_view->render('configuracion_sistema', 'config');
    }

    public function logo_login()
    {
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }

        $this->_acl->access('config');

        $this->_view->assign('titulo', 'Cambio de logo en login');
        $this->_view->assign('body-classes', array('home-background'));

        $this->_view->assign('configs', $this->_configModel->getConfigs());

        if($this->getPostParam('guardar')){
            $ruta_imagen = $this->uploadFile('logo_login', array('gif','jpg','jpe','jpeg','png'));

            if (!$ruta_imagen){
                $ruta_imagen = '';
            }

            if($this->_configModel->saveSistemConfigs(
                ['logo_login' => $ruta_imagen]
            )) $this->redirect('config/index/logo_login');
        }

        $this->_view->render('logo_login', 'config');
    }

    /* Eliminar determinaciones del nomeclador */
    public function eliminar_del_nomeclador($abrev)
    {
        if (Session::get('authenticated') != true) {
            $this->redirect('login');
        }

        $this->_acl->access('config');

        $this->_view->assign('titulo', '¿Eliminar determinación?');
        $this->_view->assign('determinacion', $this->_configModel->getDeterminacion(str_replace('_ESP_', ' ',$abrev)));

        if($this->getPostParam('aceptar')){
            if($this->_configModel->deleteDeterFromNomen(str_replace('_ESP_', ' ',$abrev)))
                $this->redirect('config/index/resultados');
        }

        $this->_view->render('eliminar_determinacion', 'config');
    }

}

?>