<?php

class configModel extends Model{
    public function __construct(){
        parent::__construct();
    }

    public function getDeterminaciones()
    {
        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare('SELECT Abrev_Fld, Nombre_Fld FROM Nomen ORDER BY Abrev_Fld');

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $determinaciones = $this->_db->fetchArray($preparedQuery);
                return $determinaciones;
            } else {
                return false;
            }
        }
    }

    public function getDeterminacion($abrev)
    {
        $preparedQuery = $this->_db->stmt_init();

        if ($abrev == 'default'){
            $determinacion['Abrev_Fld'] = 'DEFAULT';
            return $determinacion;
        } else {
            $consulta = "SELECT * FROM Nomen WHERE Abrev_Fld = '".$abrev."'";
        }

        $preparedQuery->prepare($consulta);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $determinacion = $this->_db->fetchArray($preparedQuery)[0];
                return $determinacion;
            } else {
                return false;
            }
        }
    }

    public function getFormatosTags(){
        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare('SELECT * FROM FormatosTags');

        //$preparedQuery->bind_param('i',$id);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $tags = $this->_db->fetchArray($preparedQuery);
                return $tags;
            } else {
                return false;
            }
        }
    }

    public function getFuentes(){
        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare('SELECT * FROM Fuentes');

        //$preparedQuery->bind_param('i',$id);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $tags = $this->_db->fetchArray($preparedQuery);
                return $tags;
            } else {
                return false;
            }
        }
    }

    public function getFormatoDeterminacion($Abrev_Fld){
        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare("SELECT * FROM FormatoDeterminaciones WHERE Abrev_Fld = '".$Abrev_Fld."'");

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $formatos = $this->_db->fetchArray($preparedQuery)[0];
                return $formatos;
            } else {
                return false;
            }
        }
    }

    public function saveFormat($predeterminada, $unidades, $metodos, $resultados, $materiales, $referencias, $anteriores, $fuente, $fondoResultado, $abrev, $all){
        $preparedQuery = $this->_db->stmt_init();

        $consulta = "INSERT INTO FormatoDeterminaciones VALUES (null,'".$abrev."','".$predeterminada."','".$unidades."','".$metodos."','".$resultados."','".$materiales."','".$referencias."','".$anteriores."',null,null,null,$fuente,'".$fondoResultado."')";

        $query = $preparedQuery->prepare($consulta);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            echo $preparedQuery->error;
            return false;
        } else {
            if($preparedQuery->affected_rows > 0) {
                if($all){
                    return $this->updateAllFormats($predeterminada, $unidades, $metodos, $resultados, $materiales, $referencias, $anteriores, $tabTexto, $tabResultado, $interlineado);
                }
                return true;
            } else {
                return false;
            }
        }
    }

    public function updateFormat($predeterminada, $unidades, $metodos, $resultados, $materiales, $referencias, $anteriores, $abrev, $all = false, $fondoResultado = false, $tabTexto = false, $tabResultado = false, $interlineado = false, $fuente = false){
        $preparedQuery = $this->_db->stmt_init();

        $consulta = "UPDATE FormatoDeterminaciones SET Predeterminada = '".$predeterminada."', Unidades = '".$unidades."', Metodos = '".$metodos."', Resultados = '".$resultados."', Materiales = '".$materiales."', Referencias = '".$referencias."', Anteriores = '".$anteriores."'";

        if($abrev == 'DEFAULT'){
            $consulta .=  ", FondoResultado = '".$fondoResultado."', TabTexto = ".$tabTexto.", TabResultado = ".$tabResultado.", Interlineado = ".$interlineado.", Fuente = ".$fuente;
        }

        $consulta .=  " WHERE Abrev_Fld = '".$abrev."'";

        $preparedQuery->prepare($consulta);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error == "") {
            if($all){
                return $this->updateAllFormats($predeterminada, $unidades, $metodos, $resultados, $materiales, $referencias, $anteriores, $fondoResultado);
            }
            return false;
        } else {
            return true;
        }
    }

    public function updateAllFormats($predeterminada, $unidades, $metodos, $resultados, $materiales, $referencias, $anteriores, $fondoResultado){
        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare("UPDATE FormatoDeterminaciones SET Predeterminada = '".$predeterminada."', Unidades = '".$unidades."', Metodos = '".$metodos."', Resultados = '".$resultados."', Materiales = '".$materiales."', Referencias = '".$referencias."', Anteriores = '".$anteriores."' FormatoResultados = '".$fondoResultado."'");

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            return true;
        }
    }

    public function deleteFormat($abrev)
    {

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare("DELETE FROM FormatoDeterminaciones WHERE Abrev_Fld = '".$abrev."'");

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->affected_rows > 0) {
                if ($all) {
                    return $this->updateAllFormats($predeterminada, $unidades, $metodos, $resultados, $materiales, $referencias, $anteriores, $tabTexto, $tabResultado, $interlineado);
                }
                return true;
            } else {
                return false;
            }
        }
    }

    public function deleteDeterFromNomen($abrev){
        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare("DELETE FROM Nomen WHERE Abrev_Fld = ?");

        $preparedQuery->bind_param("s", str_replace('_ESP_', ' ', $abrev));

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->affected_rows > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function getTexts(){
        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare('SELECT * FROM textos');

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $textos = $this->_db->fetchArray($preparedQuery);
                return $textos;
            } else {
                return false;
            }
        }
    }

    public function saveTexts($titulo, $texto, $pie, $imagen, $firma){
        $preparedQuery = $this->_db->stmt_init();

        $consulta = "UPDATE textos SET titulo = '".$titulo."', texto = '".$texto."', pie = '".$pie."', predet = 1";

        if(!empty($imagen))
            $consulta .= ", imagen = '".$imagen."'";

        if(!empty($firma))
            $consulta .= ", firma = '".$firma."'";

        $preparedQuery->prepare($consulta);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if($preparedQuery->affected_rows > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function getUser($username, $password, $idRole)
    {

        $roleData = $this->getRoleData($idRole);

        $table = 'Pacientes';
        $idColumn = 'HClin_Fld';
        $nameColumn = 'Nombre_Fld';
        $usernameColumn = 'HClin_Fld';
        $passColumn = 'WebPassWord_Fld';
        $redirect = "rol";

        $preparedQuery = $this->_db->stmt_init();

        $consulta = "SELECT {$roleData['idColumn']}, {$roleData['usernameColumn']}, {$roleData['nameColumn']} FROM {$roleData['table']}  WHERE {$roleData['usernameColumn']} = ? AND {$roleData['passColumn']} = ?;";

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("ss", $username, $password);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                return true;
            } else {
                return false;
            }
        }

    }

    public function changePassword($newPass, $iduser, $idRole){

        $roleData = $this->getRoleData($idRole);

        $consulta = "UPDATE {$roleData['table']} SET {$roleData['passColumn']} = ? WHERE {$roleData['idColumn']} = ?;";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);
        $preparedQuery->bind_param("ss", $newPass, $iduser);

        $preparedQuery->execute();

        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->affected_rows > 0) {
                return 'La contraseña ha sido cambiada satisfactoriamente';
            } else {
                return 'Error al cambiar la contraseña';
            }
        }
    }

    protected function getRoleData($idrole)
    {

        switch ($idrole) {
            case 1:
                $table = 'DerBioq';
                $role = 'bioquimico';
                $idColumn = 'Numero_Fld';
                $nameColumn = 'Nombre_Fld';
                $usernameColumn = 'IDUUserID_Fld';
                $passColumn = 'IDUPassWord_Fld';
                $redirect = "";
                break;

            case 2:
                $table = 'Medicos';
                $role = 'medico';
                $idColumn = 'Numero_Fld';
                $nameColumn = 'Nombre_Fld';
                $usernameColumn = 'IDUUserID_Fld';
                $passColumn = 'IDUPassWord_Fld';
                break;

            case 3:
                $table = 'Pacientes';
                $role = 'paciente';
                $idColumn = 'HClin_Fld';
                $nameColumn = 'Nombre_Fld';
                $usernameColumn = 'HClin_Fld';
                $passColumn = 'WebPassWord_Fld';
                break;

            case 4:
                $table = 'ObraSoc';
                $role = 'obra_social';
                $idColumn = 'Numero_Fld';
                $nameColumn = 'Nombre_Fld';
                $usernameColumn = 'IDUUserID_Fld';
                $passColumn = 'IDUPassWord_Fld';
                break;

            case 5:
                $table = 'LOrigen';
                $role = 'lorigen';
                $idColumn = 'Abrev_Fld';
                $nameColumn = 'NomLab_Fld';
                $usernameColumn = 'IDUUserID_Fld';
                $passColumn = 'IDUPassWord_Fld';
                break;

            case 6:
                $table = 'UsuariosAdmin';
                $role = 'administrador';
                $idColumn = 'Id_Fld';
                $nameColumn = 'ApellidoyNombre_Fld';
                $usernameColumn = 'Usuario_Fld';
                $passColumn = 'Clave_Fld';
                break;

            default:
                echo "No se conoce tipo de usuario";
                break;
        }

        return compact('table', 'role', 'idColumn', 'nameColumn', 'usernameColumn', 'passColumn');
    }


    /* Configs */
    public function getConfigs(){
        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare('SELECT * FROM Configs');

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $configs = $this->_db->fetchArray($preparedQuery);
                $configsAux = [];
                foreach ($configs as $config){
                    $configsAux[$config['config_key']] = $config['config_value'];
                }
                return $configsAux;
            } else {
                return false;
            }
        }
    }

    public function saveSistemConfigs($configs){

        foreach ($configs as $keyConfig => $value) {

            $consulta = 'SELECT * FROM Configs WHERE config_key = ?;';

            $preparedQuery = $this->_db->stmt_init();

            $preparedQuery->prepare($consulta);
            $preparedQuery->bind_param("s", $keyConfig);

            $preparedQuery->execute();

            $preparedQuery->store_result();

            if ($preparedQuery->error == "") {

                if(!$preparedQuery->num_rows){

                    $consulta = 'INSERT INTO Configs VALUES (null, ?, ?);';

                    $preparedQuery = $this->_db->stmt_init();

                    $preparedQuery->prepare($consulta);
                    $preparedQuery->bind_param("ss", $keyConfig, $value);

                    $preparedQuery->execute();

                    $preparedQuery->store_result();

                } else {

                    $consulta = 'UPDATE Configs SET config_value = ? WHERE config_key = ?;';

                    $preparedQuery = $this->_db->stmt_init();

                    $preparedQuery->prepare($consulta);
                    $preparedQuery->bind_param("ss", $value, $keyConfig);

                    $preparedQuery->execute();

                    $preparedQuery->store_result();

                }

            }

        }

        return true;

    }
}

?>