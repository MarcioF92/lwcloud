<div class="col-lg-8 col-lg-offset-2">
    <div class="col-lg-12 form-control-container">
        <h3>Configurar textos</h3>
        <form method="POST" action="" enctype="multipart/form-data">
            <input type="hidden" name="guardar" value="1" />

            <div class="col-lg-12">
                <div class="form-control-group">
                    <p>
                        <label for="pie">Mostrar resultados incompletos</label><br>
                        <select name="configs[mostrar_resultados_incompletos]" class="custom-form-control form-control">
                            <option value="1" <?php
                            if ($this->_customVars['configs']['mostrar_resultados_incompletos']) echo "selected" ?>>
                                Sí
                            </option>
                            <option value="0" <?php
                            if (!$this->_customVars['configs']['mostrar_resultados_incompletos']) echo "selected" ?>>
                                No
                            </option>
                        </select>
                    </p>
                </div>

                <div class="form-control-group">
                    <p>
                        <label for="pie">Imprimir PDFs incompletos</label><br>
                        <select name="configs[imprimir_pdf_incompleto]" class="custom-form-control form-control">
                            <option value="1" <?php
                            if ($this->_customVars['configs']['imprimir_pdf_incompleto']) echo "selected" ?>>
                                Sí
                            </option>
                            <option value="0" <?php
                            if (!$this->_customVars['configs']['imprimir_pdf_incompleto']) echo "selected" ?>>
                                No
                            </option>
                        </select>
                    </p>
                </div>

                <div class="form-control-group">
                    <p>
                        <label for="pie">Visualización de resultados</label><br>
                        <select name="configs[visualizacion_resultados]" class="custom-form-control form-control">
                            <option value="ambos" <?php
                            if ($this->_customVars['configs']['visualizacion_resultados'] == 'ambos') echo "selected"; ?>>
                                Web y PDF
                            </option>
                            <option value="solo_web" <?php
                            if ($this->_customVars['configs']['visualizacion_resultados'] == 'solo_web') echo "selected"; ?>>
                                Solo Web
                            </option>
                            <option value="solo_pdf" <?php
                            if ($this->_customVars['configs']['visualizacion_resultados'] == 'solo_pdf') echo "selected"; ?>>
                                Solo PDF
                            </option>
                        </select>
                    </p>
                </div>

                <div class="form-control-group">
                    <p>
                        <label for="pie">Eliminar guiones de código de barras</label><br>
                        <select name="configs[eliminar_guiones_codigo_barra]" class="custom-form-control form-control">
                            <option value="1" <?php
                            if ($this->_customVars['configs']['eliminar_guiones_codigo_barra']) echo "selected" ?>>
                                Sí
                            </option>
                            <option value="0" <?php
                            if (!$this->_customVars['configs']['eliminar_guiones_codigo_barra']) echo "selected" ?>>
                                No
                            </option>
                        </select>
                    </p>
                </div>

                <div class="form-control-group">
                    <p>
                        <label for="pie">Cantidad de dígitos para el número de tubo</label><br>
                        <input
                            type="number"
                            step="1"
                            min="1"
                            name="configs[digitos_nro_tubo]"
                            class="custom-form-control form-control"
                            value="<?php echo isset($this->_customVars['configs']['digitos_nro_tubo']) ? $this->_customVars['configs']['digitos_nro_tubo'] : 4; ?>"
                        />
                    </p>
                </div>

                <div class="col-lg-12 text-center">
                    <input type="submit" class="btn btn-info" value="Guardar"> - <a href="<?php echo BASE_URL; ?>config">Cancelar</a>
                </div>
            </div>
        </form>
    </div>
    <div class="col-lg-12 form-control-container text-center">
        <img src="<?php echo PRIMARY_URL . '/public/img/labwin-logo-mini.png'; ?>" />
    </div>
</div>

