<div class="col-lg-8 col-lg-offset-2">
    <div class="col-lg-12 form-control-container">
        <h3>Cambiar contraseña</h3>
        <form method="POST" action="">
            <input type="hidden" name="guardar" value="1" />

            <div class="col-lg-12">
                <div class="form-control-group">
                    <p><label for="titulo">Contraseña actual</label><br>
                        <input type="password" name="actual" class="custom-form-control form-control" /></p>
                </div>
                <div class="form-control-group">
                    <p><label for="titulo">Contraseña nueva</label><br>
                        <input type="password" name="nueva" class="custom-form-control form-control" /></p>
                </div>
                <div class="form-control-group">
                    <p><label for="titulo">Repetir contraseña nueva</label><br>
                        <input type="password" name="confirm-nueva" class="custom-form-control form-control" /></p>
                </div>

                <div class="col-lg-12 text-center">
                    <input type="submit" class="btn btn-info" value="Guardar"> - <a href="<?php echo BASE_URL; ?>">Cancelar</a>
                </div>
            </div>
        </form>
    </div>
    <div class="col-lg-12 form-control-container text-center">
        <img src="<?php echo PRIMARY_URL . '/public/img/labwin-logo-mini.png'; ?>" />
    </div>
</div>

