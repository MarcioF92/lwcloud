<div class="col-lg-8 col-lg-offset-2">
    <div class="col-lg-12 form-control-container">
        <?php if($this->_customVars['determinacion']['Abrev_Fld'] != 'DEFAULT'){ ?>
            <p><a href="<?php echo $this->_layoutParams['base_url']; ?>config/index/eliminar_formato/<?php echo $this->_customVars['determinacion']['Abrev_Fld']; ?>">Limpiar formato</a></p>
            <p><a href="<?php echo $this->_layoutParams['base_url']; ?>config/index/eliminar_del_nomeclador/<?php echo str_replace(' ', '_ESP_', $this->_customVars['determinacion']['Abrev_Fld']); ?>">Eliminar determinación del nomeclador</a></p>
        <?php } ?>
        <form method="POST" action="">
            <input type="hidden" name="guardar" value="1" />
            <input type="hidden" name="Abrev_Fld" value="<?php echo $this->_customVars['determinacion']['Abrev_Fld']; ?>" />

            <div class="col-lg-12">
                <div class="form-control-group">
                    <p> Determinación: <?php echo $this->_customVars['determinacion']['Abrev_Fld']; ?></p>
                    <p> <?php echo $this->_customVars['determinacion']['Nombre_Fld']; ?></p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-control-group">
                    <div class="form-control-group">
                        <?php
                        if ($this->_customVars['determinacion']['Abrev_Fld'] == 'DEFAULT'){
                            ?>
                            <p><label for="TabTexto">Fuente</label><br>
                                <select class="custom-form-control" name="Fuente">
                                    <option value="-1">Elija una fuente</option>
                                    <?php
                                    foreach ($this->_customVars['fuentes'] as $fuente){
                                        ?>
                                        <option value="<?php echo $fuente['id']; ?>" style="font-family: <?php echo $fuente['font-family']; ?>"
                                            <?php
                                            if($this->_customVars['formato_determinacion']['Fuente'] == $fuente['id'])
                                                echo "selected";
                                            ?>
                                        ><?php echo $fuente['nombre']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </p>
                            <?php
                        }
                        ?>
                    </div>
                    <label for="letra_predeterminada">Letra predeterminada: </label><br>
                    <select class="custom-form-control" name="letra_predeterminada">
                        <?php
                        foreach ($this->_customVars['tags'] as $tag){
                            ?>
                            <option value="<?php echo $tag['tag']; ?>"
                                <?php if($this->_customVars['formato_determinacion']){ ?>
                                    <?php if($this->_customVars['formato_determinacion']['Predeterminada'] == $tag['tag']){
                                        echo "selected";
                                    }
                                    ?>
                                <?php } ?>
                            ><?php echo $tag['nombre']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-control-group">
                    <label for="letra_unidades">Letra unidades: </label><br>
                    <select class="custom-form-control" name="letra_unidades">
                        <?php
                        foreach ($this->_customVars['tags'] as $tag){
                            ?>
                            <option value="<?php echo $tag['tag']; ?>"
                                <?php if($this->_customVars['formato_determinacion']){ ?>
                                    <?php if($this->_customVars['formato_determinacion']['Unidades'] == $tag['tag']){
                                        echo "selected";
                                    }
                                    ?>
                                <?php } ?>
                            ><?php echo $tag['nombre']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-control-group">
                    <label for="letra_metodos">Letra métodos: </label><br>
                    <select class="custom-form-control" name="letra_metodos">
                        <?php
                        foreach ($this->_customVars['tags'] as $tag){
                            ?>
                            <option value="<?php echo $tag['tag']; ?>"
                                <?php if($this->_customVars['formato_determinacion']){ ?>
                                    <?php if($this->_customVars['formato_determinacion']['Metodos'] == $tag['tag']){
                                        echo "selected";
                                    }
                                    ?>
                                <?php } ?>
                            ><?php echo $tag['nombre']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-control-group">
                    <label for="letra_resultados">Letra resultados: </label><br>
                    <select class="custom-form-control" name="letra_resultados">
                        <?php
                        foreach ($this->_customVars['tags'] as $tag){
                            ?>
                            <option value="<?php echo $tag['tag']; ?>"
                                <?php if($this->_customVars['formato_determinacion']){ ?>
                                    <?php if($this->_customVars['formato_determinacion']['Resultados'] == $tag['tag']){
                                        echo "selected";
                                    }
                                    ?>
                                <?php } ?>
                            ><?php echo $tag['nombre']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-control-group">
                    <label for="fondo_resultado">Fondo resultados: </label><div class="previsualizacion_color" style="background: {{ fondo_resultado }};height: 10px; width: 10px;"></div><br>
                    <div ng-init="fondo_resultado = '<?php echo $this->_customVars['formato_determinacion']['FondoResultado']; ?>'">
                        <input name="fondo_resultado" class="custom-form-control" colorpicker type="text" ng-model="fondo_resultado" value="<?php echo $this->_customVars['formato_determinacion']['FondoResultado']; ?>" />
                    </div>

                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-control-group">
                    <label for="letra_materiales">Letra materiales: </label><br>
                    <select class="custom-form-control" name="letra_materiales">
                        <?php
                        foreach ($this->_customVars['tags'] as $tag){
                            ?>
                            <option value="<?php echo $tag['tag']; ?>"
                                <?php if($this->_customVars['formato_determinacion']){ ?>
                                    <?php if($this->_customVars['formato_determinacion']['Materiales'] == $tag['tag']){
                                        echo "selected";
                                    }
                                    ?>
                                <?php } ?>
                            ><?php echo $tag['nombre']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-control-group">
                    <label for="letra_valores_de_referencia">Letra valores de referencia: </label><br>
                    <select class="custom-form-control" name="letra_valores_de_referencia">
                        <?php
                        foreach ($this->_customVars['tags'] as $tag){
                            ?>
                            <option value="<?php echo $tag['tag']; ?>"
                                <?php if($this->_customVars['formato_determinacion']){ ?>
                                    <?php if($this->_customVars['formato_determinacion']['Referencias'] == $tag['tag']){
                                        echo "selected";
                                    }
                                    ?>
                                <?php } ?>
                            ><?php echo $tag['nombre']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-control-group">
                    <label for="letra_resultados_anteriores">Letra resultados anteriores: </label><br>
                    <select class="custom-form-control" name="letra_resultados_anteriores">
                        <?php
                        foreach ($this->_customVars['tags'] as $tag){
                            ?>
                            <option value="<?php echo $tag['tag']; ?>"
                                <?php if($this->_customVars['formato_determinacion']){ ?>
                                    <?php if($this->_customVars['formato_determinacion']['Anteriores'] == $tag['tag']){
                                        echo "selected";
                                    }
                                    ?>
                                <?php } ?>
                            ><?php echo $tag['nombre']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-control-group">
                    <?php
                    if ($this->_customVars['determinacion']['Abrev_Fld'] == 'DEFAULT'){
                    ?>
                    <p><label for="TabTexto">Margen izquierdo de texto</label><br>
                        <input class="custom-form-control" type="number" name="TabTexto" placeholder="Espaciado texto"
                               value="<?php echo $this->_customVars['formato_determinacion']['TabTexto']; ?>"/> px</p>
                    <p><label for="TabResultados">Margen izquierdo de resultado</label><br>
                        <input class="custom-form-control" type="number" name="TabResultados" placeholder="Espaciado resultados"
                               value="<?php echo $this->_customVars['formato_determinacion']['TabResultado']; ?>" /> px</p>
                    <p><label for="Interlineado">Interlineado entre resultados</label><br>
                        <input class="custom-form-control" type="number" name="Interlineado" placeholder="Espaciado resultados"
                               value="<?php echo $this->_customVars['formato_determinacion']['Interlineado']; ?>" /> px
                        <br>
                        <?php
                        }
                        ?>
                        <br>
                        <input type="checkbox" name="ActualizarTodos" value="1"> Extender formato a todos los análisis<br>
                        <input class="btn btn-info" type="submit" value="Guardar" /> - <a href="<?php echo $this->_layoutParams['base_url']; ?>config/index/resultados">Cancelar</a>
                </div>
            </div>
        </form>
    </div>
    <div class="col-lg-12 form-control-container text-center">
        <img src="<?php echo PRIMARY_URL . '/public/img/labwin-logo-mini.png'; ?>" />
    </div>
</div>

