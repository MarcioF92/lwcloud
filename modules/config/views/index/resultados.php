<div ng-controller="CONFIG">
    <div class="col-lg-8 col-lg-offset-2">
        <p><a href="<?php echo $this->_layoutParams['base_url'].'config/index/formatear/'; ?>default">Configurar formato de todos los resultados</a></p>
    </div>
    <div class="col-lg-8 col-lg-offset-2">
        <div class="col-lg-3 col-md-2 col-xs-6 hidden-xs">
            Buscador:
            <input type="text" class="form-control" name="protocolo" ng-model="buscador">
        </div>
    </div>

    <div class="col-lg-8 col-lg-offset-2" ng-init="determinaciones = <?php echo htmlspecialchars(json_encode($this->_customVars['determinaciones'])); ?>">

        <div class="col-lg-12">
            <ul ng-repeat="determinacion in getDeterminaciones()">
                <li><a href="<?php echo $this->_layoutParams['base_url'].'config/index/formatear/'; ?>{{determinacion.Abrev_Fld.split(' ').join('_ESP_')}}">{{determinacion.Abrev_Fld}} - {{determinacion.Nombre_Fld}}</a></li>
            </ul>
        </div>

    </div>

    <div class="col-lg-12 form-control-container text-center">
        <img src="<?php echo PRIMARY_URL . '/public/img/labwin-logo-mini.png'; ?>" />
    </div>
</div>