<div class="col-lg-8 col-lg-offset-2">
    <div class="col-lg-12 form-control-container">
        <h3><?php echo $this->_customVars['titulo']; ?></h3>
        <form method="POST" action="" enctype="multipart/form-data">
            <input type="hidden" name="guardar" value="1" />
                <div class="form-control-group">
                    <div class="col-lg-12 text-center">
                        <p><label for="imagen">Imagen</label><br>

                            <input type="file" name="logo_login" /></p>

                        <?php if (!empty($this->_customVars['configs']['logo_login'])) { ?>
                            <p>Imagen actual: <br>
                                <img style="max-width: 250px;margin: 0 auto;" src="<?php echo PRIMARY_URL . 'laboratorios/assets/' . LAB . '/' . $this->_customVars['configs']['logo_login']; ?>" />
                            </p>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-lg-12 text-center">
                    <input type="submit" class="btn btn-info" value="Guardar"> - <a href="<?php echo BASE_URL; ?>config">Cancelar</a>
                </div>
            </div>
        </form>
    </div>
    <div class="col-lg-12 form-control-container text-center">
        <img src="<?php echo PRIMARY_URL . '/public/img/labwin-logo-mini.png'; ?>" />
    </div>
</div>