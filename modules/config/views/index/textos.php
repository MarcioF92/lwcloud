<div class="col-lg-8 col-lg-offset-2">
    <div class="col-lg-12 form-control-container">
        <h3>Configurar textos</h3>
        <form method="POST" action="" enctype="multipart/form-data">
            <input type="hidden" name="guardar" value="1" />

            <div class="col-lg-12">
                <div class="form-control-group">
                    <p><label for="titulo">Título</label><br>
                    <input type="text" name="titulo" class="custom-form-control form-control" value="<?php if (!empty($this->_customVars['textos']['titulo'])) echo htmlspecialchars($this->_customVars['textos']['titulo']); ?>" /></p>
                </div>
                <div class="form-control-group">
                    <p><label for="texto">Texto</label><br>
                        <textarea name="texto" class="custom-form-control form-control"><?php if (!empty($this->_customVars['textos']['texto'])) echo $this->_customVars['textos']['texto'];?></textarea></p>
                </div>
                <div class="form-control-group">
                    <p><label for="pie">Pie</label><br>
                        <input type="text" name="pie" class="custom-form-control form-control" value="<?php if (!empty($this->_customVars['textos']['pie'])) echo htmlspecialchars($this->_customVars['textos']['pie']);?>" /></p>
                </div>
                <div class="form-control-group">
                    <div class="col-lg-6 text-center">
                        <p><label for="imagen">Imagen</label><br>

                            <input type="file" name="imagen" /></p>

                        <?php if (!empty($this->_customVars['textos']['imagen'])) { ?>
                            <p>Imagen actual: <br>
                                <img style="max-width: 250px;margin: 0 auto;" src="<?php echo PRIMARY_URL . 'laboratorios/assets/' . $_GET['lab'] . '/labfiles/' . $this->_customVars['textos']['imagen']; ?>" />
                            </p>
                        <?php } ?>
                    </div>
                    <div class="col-lg-6 text-center">
                        <p><label for="firma">Firma</label><br>
                            <input type="file" name="firma" /></p>
                        <?php if (!empty($this->_customVars['textos']['firma'])) { ?>
                            <p>Imagen actual: <br>
                                <img style="max-width: 250px;margin: 0 auto;" class="col-lg-12" src="<?php echo PRIMARY_URL . 'laboratorios/assets/' . $_GET['lab'] . '/labfiles/' . $this->_customVars['textos']['firma']; ?>" />
                            </p>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-lg-12 text-center">
                    <input type="submit" class="btn btn-info" value="Guardar"> - <a href="<?php echo BASE_URL; ?>config">Cancelar</a>
                </div>
            </div>
        </form>
    </div>
    <div class="col-lg-12 form-control-container text-center">
        <img src="<?php echo PRIMARY_URL . '/public/img/labwin-logo-mini.png'; ?>" />
    </div>
</div>

