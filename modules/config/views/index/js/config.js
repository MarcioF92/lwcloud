var app=angular.module('ui.bootstrap', ['ngAnimate', 'ui.bootstrap', 'colorpicker.module']);

app.controller('CONFIG', ['$scope', '$filter', function ($scope, $filter) {
    $scope.currentPage = 0;
    $scope.pageSize = 15;
    $scope.finalArray = [];
    $scope.searchText = '';

    $scope.getDeterminaciones = function () {
        var finalArray = $filter('filter')($scope.determinaciones, $scope.buscador);
        return finalArray;
    }
}]);