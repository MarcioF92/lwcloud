<?php
class loginModel extends Model{

    public function __construct(){
        parent::__construct();
    }

    public function getUser($username, $password, $role){

        switch ($role) {
        	case 'bioquimico':
        		$table = 'DerBioq';
                $idColumn = 'Numero_Fld';
                $nameColumn = 'Nombre_Fld';
        		$usernameColumn = 'DerivOnLineID_Fld';
        		$passColumn = 'DerivOnLinePW_Fld';
                $redirect = "";
        		break;

        	case 'medico':
        		$table = 'Medicos';
                $idColumn = 'Numero_Fld';
                $nameColumn = 'Nombre_Fld';
        		$usernameColumn = 'IDUUserID_Fld';
        		$passColumn = 'IDUPassWord_Fld';
                $redirect = "";
        		break;

        	case 'paciente':
        		$table = 'Pacientes';
                $idColumn = 'HClin_Fld';
                $nameColumn = 'Nombre_Fld';
        		$usernameColumn = 'HClin_Fld';
        		$passColumn = 'WebPassWord_Fld';
                $redirect = "rol";
        		break;

            case 'obra_social':
                $table = 'ObraSoc';
                $idColumn = 'Numero_Fld';
                $nameColumn = 'Nombre_Fld';
                $usernameColumn = 'WebID_Fld';
                $passColumn = 'PassWord_Fld';
                $redirect = "rol";
                break;

            case 'lorigen':
                $table = 'LOrigen';
                $idColumn = 'Abrev_Fld';
                $nameColumn = 'NomLab_Fld';
                $usernameColumn = 'IDUUserID_Fld';
                $passColumn = 'IDUPassWord_Fld';
                $redirect = "rol";
                break;

        	default:
        		echo "No se conoce tipo de usuario";
        		break;
        }

        if(!$this->tableExist($table)){
            return false;
        }

        $preparedQuery = $this->_db->stmt_init();

    	$preparedQuery->prepare("SELECT " . $idColumn . ", " . $usernameColumn . ", " . $nameColumn . " FROM " . $table . " WHERE " . $usernameColumn . " = ? AND " . $passColumn . " = ?;");

    	$preparedQuery->bind_param("ss", $username, $password);

    	$preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $preparedQuery->bind_result($userid, $currentUsername, $nombre);
                $preparedQuery->fetch();

                $user = array(
                        'userid' => $userid,
                        'username' => $currentUsername,
                        'nombre_apellido' => $nombre,
                        'role' => $role,
                        'redirect' => $redirect
                    );

                return $user;
            } else {
                return false;
            }
        }

    }

    public function newGetUser($username, $password){
        $tables = array('DerBioq', 'Medicos', 'Pacientes', 'ObraSoc', 'LOrigen', 'UsuariosAdmin');
        foreach ($tables as $table) {

            if ($this->searchUserInTable($username, $table)) {
                $user = $this->getUserInTable($username, $password, $table);
                if (!$user){
                    return false;
                } else {
                    return $user;
                }
            }
        }

        return false;
    }

    public function searchUserInTable($username, $table){

        switch ($table) {
            case 'DerBioq':
                $usernameColumn = 'IDUUserID_Fld';
                break;

            case 'Medicos':
                $usernameColumn = 'IDUUserID_Fld';
                break;

            case 'Pacientes':
                $usernameColumn = 'HClin_Fld';
                break;

            case 'ObraSoc':
                $usernameColumn = 'IDUUserID_Fld';
                break;

            case 'LOrigen':
                $usernameColumn = 'IDUUserID_Fld';
                break;

            case 'UsuariosAdmin':
                $usernameColumn = 'Usuario_Fld';
                break;

            default:
                echo "No se conoce tipo de usuario";
                break;
        }

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare("SELECT * FROM " . $table . " WHERE CAST(" . $usernameColumn . " AS CHAR(112)) = ?;");

        $preparedQuery->bind_param("s", $username);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                return true;
            } else {
                return false;
            }
        }

    }

    public function getUserInTable($username, $password, $table){

        $redirect = "rol";

        switch ($table) {
            case 'DerBioq':
                $role = 'bioquimico';
                $idColumn = 'Numero_Fld';
                $nameColumn = 'Nombre_Fld';
                $usernameColumn = 'IDUUserID_Fld';
                $passColumn = 'IDUPassWord_Fld';
                $redirect = "";
                break;

            case 'Medicos':
                $role = 'medico';
                $idColumn = 'Numero_Fld';
                $nameColumn = 'Nombre_Fld';
                $usernameColumn = 'IDUUserID_Fld';
                $passColumn = 'IDUPassWord_Fld';
                break;

            case 'Pacientes':
                $role = 'paciente';
                $idColumn = 'HClin_Fld';
                $nameColumn = 'Nombre_Fld';
                $usernameColumn = 'HClin_Fld';
                $passColumn = 'WebPassWord_Fld';
                break;

            case 'ObraSoc':
                $role = 'obra_social';
                $idColumn = 'Numero_Fld';
                $nameColumn = 'Nombre_Fld';
                $usernameColumn = 'IDUUserID_Fld';
                $passColumn = 'IDUPassWord_Fld';
                break;

            case 'LOrigen':
                $role = 'lorigen';
                $idColumn = 'Abrev_Fld';
                $nameColumn = 'NomLab_Fld';
                $usernameColumn = 'IDUUserID_Fld';
                $passColumn = 'IDUPassWord_Fld';
                break;

            case 'UsuariosAdmin':
                $role = 'administrador';
                $idColumn = 'Id_Fld';
                $nameColumn = 'ApellidoyNombre_Fld';
                $usernameColumn = 'Usuario_Fld';
                $passColumn = 'Clave_Fld';
                $redirect = "";
                break;

            default:
                echo "No se conoce tipo de usuario";
                break;
        }

        $preparedQuery = $this->_db->stmt_init();

        if ($table != 'Pacientes') {


           $preparedQuery->prepare("SELECT " . $idColumn . ", " . $usernameColumn . ", " . $nameColumn . " FROM " . $table . " WHERE " . $usernameColumn . " = ? AND " . $passColumn . " = ?;");

            $preparedQuery->bind_param("ss", $username, $password);

            $preparedQuery->execute();
            $preparedQuery->store_result();

            if ($preparedQuery->error != "") {
                return false;
            } else {
                if ($preparedQuery->num_rows > 0) {
                    $preparedQuery->bind_result($userid, $currentUsername, $nombre);
                    $preparedQuery->fetch();

                    $user = array(
                            'userid' => $userid,
                            'username' => $currentUsername,
                            'nombre_apellido' => $nombre,
                            'role' => $role,
                            'redirect' => $redirect
                        );

                    return $user;
                } else {
                    return false;
                }
            }

        } else {
            $preparedQuery->prepare("SELECT " . $idColumn . ", " . $usernameColumn . ", " . $nameColumn . ", " . $passColumn . ", Fecha_Fld FROM " . $table . " WHERE " . $usernameColumn . " = ? ORDER BY Fecha_Fld DESC");

            $preparedQuery->bind_param("s", $username);

            $preparedQuery->execute();
            $preparedQuery->store_result();

            if ($preparedQuery->error != "") {
                return false;
            } else {
                if ($preparedQuery->num_rows > 0) {
                    $user = $this->_db->fetchArray($preparedQuery);

                    if ($username == $user[0][$usernameColumn] && $password == $user[0][$passColumn]) {
                        $loggedUser = array(
                            'userid' => $user[0]['HClin_Fld'],
                            'username' => $user[0]['HClin_Fld'],
                            'nombre_apellido' => $user[0]['Nombre_Fld'],
                            'role' => $role,
                            'redirect' => $redirect
                        );

                        return $loggedUser;
                    } else {
                        return false;
                    }

                } else {
                    return false;
                }
            }
        }

    }

    public function getIdRole($role_key){
        if (file_exists(APP_PATH . "permission_roles" . DS . "roles.json")) {
            $json = file_get_contents(APP_PATH . "permission_roles" . DS . "roles.json");
            $jsonObj = json_decode($json);
            $jsonArray = (array) $jsonObj->data;
            return array_keys($jsonArray, $role_key)[0];
        }
    }

    public function tableExist($table){
        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare("SELECT 1 FROM " . $table . " WHERE 1;");

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            return true;
        }
    }

    public function getLogo()
    {
        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare('SELECT * FROM Configs WHERE config_key = "logo_login"');

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $configs = $this->_db->fetchArray($preparedQuery);
                return !empty($configs[0]['config_value']) ? $configs[0]['config_value']: '';
            } else {
                return false;
            }
        }
    }

}

?>
