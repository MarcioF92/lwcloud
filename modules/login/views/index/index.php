<div id="login-form-container" class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-ms-6 col-ms-offset-3 col-xs-10 col-xs-offset-1 text-center">
	<div id="logo" class="col-lg-12">
        <?php if(!empty($this->_customVars['logoLogin'])) : ?>
            <img class="col-lg-12 col-md-12 col-ms-12 col-xs-12" src="<?php echo PRIMARY_URL; ?>laboratorios/assets/<?php echo LAB; ?>/<?php echo $this->_customVars['logoLogin']; ?>" />
        <?php else : ?>
		    <img class="col-lg-12 col-md-12 col-ms-12 col-xs-12" src="<?php echo PRIMARY_URL; ?>public/img/labwin-logo.png" />
        <?php endif; ?>
	</div>
	<div id="login-form" class="text-left col-lg-8 col-lg-offset-2">

		<?php if(isset($this->_customVars['_error']) != ''){ ?>
		<div id="alert-error" class="alert-danger">
				<?php echo $this->_customVars['_error']; ?>
		</div>
		<?php } ?>
		<form name="form1" method="post" action="">

			<input type="hidden" value="1" name="enviar" />

			<div class="col-lg-12 form-control-container">
				<label for="usuario" class="light-blue-color col-lg-12">USUARIO: </label><br>
				<input type="text" name="usuario" value="<?php if(isset($this->_customVars['datos']['usuario'])) echo $this->_customVars['datos']['usuario']; ?>" class="col-lg-12 custom-form-control" />
			</div>

			<div class="col-lg-12 form-control-container">
				<label for="pass" class="light-blue-color col-lg-12">CONTRASEÑA: </label><br>
				<input type="password" name="pass" class="col-lg-12 custom-form-control" />
			</div>



			<div class="col-lg-12 form-control-container text-center">
				<button class="btn btn-info" type="submit">Entrar</button>
			</div>
		</form>
	</div>
</div>
