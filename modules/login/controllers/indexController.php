<?php

/*
* Index Controller of Login Module
* /public_html/laboratorios/mobile/modules/login/controllers/indexController.php
*/

class indexController extends Controller
{
    private $_loginModel;

    public function __construct(){
        parent::__construct();

        $this->_loginModel = $this->loadModel('login');

    }

    public function index(){

        $this->_view->assign('body-classes', array('home-background'));
        $this->_view->assign('content-classes', array('display-flex'));

        $this->_view->assign('logoLogin', $this->_loginModel->getLogo());

        if ($this->getInt('enviar') == 1) {

            /*session_id(SID);
            session_start();
            Session::destroy();
            session_start();*/

            $this->_view->assign('datos', $_POST);

            if (!$this->getAlphaNum('usuario')) {
                $this->_view->assign('_error', "Debe introducir su nombre de usuario");
                $this->_view->render('index', 'login');
                exit;
            }

            if (!$this->getAlphaNum('pass')) {
                $this->_view->assign('_error', "Debe introducir Password");
                $this->_view->render('index', 'login');
                exit;
            }

            /*$user = $this->_loginModel->getUser(
                $this->getAlphaNum('usuario'),
                $this->getAlphaNum('pass'),
                $this->getPostParam('role')
            );*/

            $user = $this->_loginModel->newGetUser(
                $this->getAlphaNum('usuario'),
                $this->getAlphaNum('pass')
            );

            if (!$user) {
                $this->_view->assign('_error', "No se ha encontrado el usuario. Si sus datos son correctos, puede que todavía no tenga los resultados cargados");
                $this->_view->render('index', 'login');
                exit;
            }

            //Despues tratar de meter el $user en el Session
            $uniqId = uniqid();

            session_id(md5($user['userid']));
            session_start();
            Session::destroy();
            session_id(md5($user['userid']));
            Session::init();

            Session::set('authenticated', true);
            Session::set('role', $this->_loginModel->getIdRole($user['role']));
            Session::set('user', $user['username']);
            Session::set('nombre_apellido', $user['nombre_apellido']);
            Session::set('iduser', $user['userid']);
            Session::set('time', time());
            Session::set('uniqid', $uniqId);
            
            setcookie('biodatasa-'.SUBDOMAIN, $uniqId, time() + (86400 * 30), "/");

            $this->redirect($user['redirect']);

        } else {
            if (Session::get('authenticated')) {
                $this->redirect();
            }

            $this->_view->assign('titulo', 'Iniciar Sesión');
        }

        $this->_view->render('index','login');

    }

    public function cerrar(){
        Session::destroy();
        unset($_COOKIE['biodatasa-'.SUBDOMAIN]);
        setcookie('biodatasa-'.SUBDOMAIN, $user['userid'], time() - 3600, "/");
        $redirect_id = explode(".", str_replace('http://', '', $_SERVER['HTTP_HOST']));

        $redirects = unserialize(REDIRECTS_LOGOUT);

        if ($redirects[$redirect_id[0]] != "") {
            $this->redirect($redirects[$redirect_id[0]], true);
        } else {
          $this->redirect();
        }
    }

}

?>
