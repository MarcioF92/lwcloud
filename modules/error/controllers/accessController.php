<?php

class accessController extends Controller{

    public function __construct(){
        parent::__construct();
    }

    public function index(){}

    public function error($codigo){
        $this->_view->assign('titulo', 'Error');
        $this->_view->assign('body-classes', array('rol-mol-background'));
        $this->_view->assign('content-classes', array('vertical-align'));
        $this->_view->assign('error', $this->_getError($codigo));
        $this->_view->render('access', 'error');
    }

    private function _getError($codigo = false){
        if ($codigo) {
            $codigo = $this->filterInt($codigo);
            if (is_int($codigo)) {
                $codigo = $codigo;
            }
        } else {
            $codigo = 'default';
        }     

        $error['default'] = 'Ha ocurrido un error y la página no puede mostrarse';
        $error['5050'] = 'Acceso restringido: no dispones de los permisos necesarios para acceder a esta página';
        $error['8080'] = 'Tiempo de la sesión agotado: se ha agotado el tiempo de sesión, por favor vuelva a iniciarla';

        if (array_key_exists($codigo, $error)) {
            return $error[$codigo];
        } else {
            return $error['default'];
        }
    }

}

?>