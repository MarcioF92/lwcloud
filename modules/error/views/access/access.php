<div class="col-lg-12 col-xs-12 text-center">
	<h2>Ha ocurrido un error</h2>
	<div id="alert-error" class="alert-danger col-lg-4 col-lg-offset-4">
		<?php echo $this->_customVars['error'] ?>
	</div>
	<div class="col-lg-4 col-lg-offset-4">
		<a href="<?php echo $this->_layoutParams['base_url']; ?>">Volver al inicio</a>
	</div>
</div>