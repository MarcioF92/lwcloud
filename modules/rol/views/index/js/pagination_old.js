var app=angular.module('ui.bootstrap', ['ngAnimate', 'ui.bootstrap']);

// alternate - https://github.com/michaelbromley/angularUtils/tree/master/src/directives/pagination
// alternate - http://fdietz.github.io/recipes-with-angular-js/common-user-interface-patterns/paginating-through-client-side-data.html

app.controller('ROL', ['$scope', '$filter', '$http', '$q', function ($scope, $filter, $http, $q) {
    $scope.currentPage = 0;
    $scope.pageSize = 15;
    $scope.derivaciones = [];
    $scope.searchText = '';
    $scope.selectAno = String(new Date().getFullYear());

    $scope.dateFilter = function(derivacion){
        
      var dayFrom;
      var monthFrom;
      var yearFrom;
      var dateDerFrom;

      var dateDayFrom;
      var dateMonthFrom;
      var dateYearFrom;
      var dateDateFrom;

      var dayTo;
      var monthTo;
      var yearTo;
      var dateDerTo;

      var dateDayTo;
      var dateMonthTo;
      var dateYearTo;
      var dateDateTo;

      if ($scope.datePicker != undefined) {
        if ($scope.datePicker != '') {
          dayFrom = $filter('limitTo')(derivacion.Fecha_Fld, 2, 6);
          monthFrom = $filter('limitTo')(derivacion.Fecha_Fld, 2, 4);
          yearFrom = $filter('limitTo')(derivacion.Fecha_Fld, 4, 0);
          dateDerFrom = new Date(yearFrom, monthFrom, dayFrom, 0, 0, 0, 0);

          dateDayFrom = $filter('limitTo')($scope.datePicker, 2, 0);
          dateMonthFrom = $filter('limitTo')($scope.datePicker, 2, 3);
          dateYearFrom = $filter('limitTo')($scope.datePicker, 4, 6);
          dateDateFrom = new Date(dateYearFrom, dateMonthFrom, dateDayFrom, 0, 0, 0, 0);
        } else {
          dateDateFrom = false;
        }
      } else {
          dateDateFrom = false;
      }

      if ($scope.datePickerHasta != undefined) {
        if ($scope.datePickerHasta != '') {
          dayTo = $filter('limitTo')(derivacion.Fecha_Fld, 2, 6);
          monthTo = $filter('limitTo')(derivacion.Fecha_Fld, 2, 4);
          yearTo = $filter('limitTo')(derivacion.Fecha_Fld, 4, 0);
          dateDerTo =  new Date(yearTo, monthTo, dayTo, 0, 0, 0, 0);

          dateDayTo = $filter('limitTo')($scope.datePickerHasta, 2, 0);
          dateMonthTo = $filter('limitTo')($scope.datePickerHasta, 2, 3);
          dateYearTo = $filter('limitTo')($scope.datePickerHasta, 4, 6);

          dateDateTo = new Date(dateYearTo, dateMonthTo, dateDayTo, 0, 0, 0, 0);
        } else {
          dateDateTo = false;
        }
      } else {
          dateDateTo = false;
      }

      if(dateDateTo != false){
        if (dateDateFrom != false) {
          if(dateDateFrom.getTime() > dateDerFrom.getTime() || dateDateTo.getTime() < dateDerTo.getTime()){
            return false;
          }
        } else {
          if(dateDerTo.getTime() > dateDateTo.getTime()){
            return false;
          }
        }
      } else {
        if(dateDateFrom != false) {
          if(dateDerFrom.getTime() != dateDateFrom.getTime()){
            return false;
          }
        }
      }

      return true;

    }
    
    $scope.getDerivaciones = function () {
      // needed for the pagination calc
      // https://docs.angularjs.org/api/ng/filter/filter
      /*return $filter('filter')($scope.derivaciones, $scope.totalFilter);
      
        // manual filter
       // if u used this, remove the filter from html, remove above line and replace data with getData()
       
        var arr = [];
        if($scope.searchText == '') {
            arr = $scope.data;
        } else {
            for(var ea in $scope.data) {
                if($scope.data[ea].indexOf($scope.searchText) > -1) {
                    arr.push( $scope.data[ea] );
                }
            }
        }
        return arr;*/

        var searchArray = $filter('filter')($scope.derivaciones, $scope.dateFilter);
        return $filter('filter')(searchArray, $scope.searchText);
    }

    $scope.totalFilter=function(value, index, array){
      console.log("Total Filter");
      var returnValue = true;

      // Fecha
      /*var dateFrom;
        var dayFrom;
        var monthFrom;
        var yearFrom;
        var dateDayFrom;
        var dateMonthFrom;
        var dateYearFrom;

        var dateTo;
        var dayTo;
        var monthTo;
        var yearTo;
        var dateDayTo;
        var dateMonthTo;
        var dateYearTo;

        if ($scope.datePicker != undefined) {
          if ($scope.datePicker != '') {
            dayFrom = $filter('limitTo')(value.Fecha_Fld, 2, 6);
            monthFrom = $filter('limitTo')(value.Fecha_Fld, 2, 4);
            yearFrom = $filter('limitTo')(value.Fecha_Fld, 4, 0);
            dateFrom = dayFrom + '/' + monthFrom + '/' + yearFrom;

            dateDayFrom = $filter('limitTo')($scope.datePicker, 2, 0); // dd/mm/yyyy
            dateMonthFrom = $filter('limitTo')($scope.datePicker, 2, 3);
            dateYearFrom = $filter('limitTo')($scope.datePicker, 4, 6);
            dateDateFrom = new Date(yearFrom, monthFrom, dayFrom, 0, 0, 0, 0);
          } else {
            dateDateFrom = false;
          }
        } else {
            dateDateFrom = false;
        }

        if ($scope.datePickerHasta != undefined) {
          if ($scope.datePickerHasta != '') {
            dayTo = $filter('limitTo')(value.Fecha_Fld, 2, 6);
            monthTo = $filter('limitTo')(value.Fecha_Fld, 2, 4);
            yearTo = $filter('limitTo')(value.Fecha_Fld, 4, 0);
            dateTo = dayTo + '/' + monthTo + '/' + yearTo;

            dateDayTo = $filter('limitTo')($scope.datePickerHasta, 2, 0); // dd/mm/yyyy
            dateMonthTo = $filter('limitTo')($scope.datePickerHasta, 2, 3);
            dateYearTo = $filter('limitTo')($scope.datePickerHasta, 4, 6);

            var dateDateTo = new Date(yearTo, monthTo, dayTo, 0, 0, 0, 0);

          } else {
            dateDateTo = false;
          }
        } else {
            dateDateTo = false;
        }

        var dateDatePickerHasta;
        var dateDatePicker;

        if(dateDateTo != false){
          dateDatePickerHasta = new Date(dateYearTo, dateMonthTo, dateDayTo, 0, 0, 0, 0);
          if (dateDateFrom != false) {
            dateDatePicker = new Date(dateYearFrom, dateMonthFrom, dateDayFrom, 0, 0, 0, 0);
            if(dateDatePicker.getTime() <= dateDateFrom.getTime() && dateDatePickerHasta.getTime() >= dateDateTo.getTime()){
              returnValue =  true;
            } else {
              return false;
            }
          } else {
            if(dateDateTo.getTime() < dateDatePickerHasta.getTime()){
              return false;
            }
          }
        } else {
          if(dateDateFrom != false) {
            dateDatePicker = new Date(dateYearFrom, dateMonthFrom, dateDayFrom, 0, 0, 0, 0);
            if(dateDateFrom.getTime() != dateDatePicker.getTime()){
              return false;
            }
          }
        }*/

      var dayFrom;
      var monthFrom;
      var yearFrom;
      var dateDerFrom;

      var dateDayFrom;
      var dateMonthFrom;
      var dateYearFrom;
      var dateDateFrom;

      var dayTo;
      var monthTo;
      var yearTo;
      var dateDerTo;

      var dateDayTo;
      var dateMonthTo;
      var dateYearTo;
      var dateDateTo;

      if ($scope.datePicker != undefined) {
        if ($scope.datePicker != '') {
          dayFrom = $filter('limitTo')(value.Fecha_Fld, 2, 6);
          monthFrom = $filter('limitTo')(value.Fecha_Fld, 2, 4);
          yearFrom = $filter('limitTo')(value.Fecha_Fld, 4, 0);
          dateDerFrom = new Date(yearFrom, monthFrom, dayFrom, 0, 0, 0, 0);

          dateDayFrom = $filter('limitTo')($scope.datePicker, 2, 0);
          dateMonthFrom = $filter('limitTo')($scope.datePicker, 2, 3);
          dateYearFrom = $filter('limitTo')($scope.datePicker, 4, 6);
          dateDateFrom = new Date(dateYearFrom, dateMonthFrom, dateDayFrom, 0, 0, 0, 0);
        } else {
          dateDateFrom = false;
        }
      } else {
          dateDateFrom = false;
      }

      if ($scope.datePickerHasta != undefined) {
        if ($scope.datePickerHasta != '') {
          dayTo = $filter('limitTo')(value.Fecha_Fld, 2, 6);
          monthTo = $filter('limitTo')(value.Fecha_Fld, 2, 4);
          yearTo = $filter('limitTo')(value.Fecha_Fld, 4, 0);
          dateDerTo =  new Date(yearTo, monthTo, dayTo, 0, 0, 0, 0);

          dateDayTo = $filter('limitTo')($scope.datePickerHasta, 2, 0);
          dateMonthTo = $filter('limitTo')($scope.datePickerHasta, 2, 3);
          dateYearTo = $filter('limitTo')($scope.datePickerHasta, 4, 6);

          dateDateTo = new Date(dateYearTo, dateMonthTo, dateDayTo, 0, 0, 0, 0);
        } else {
          dateDateTo = false;
        }
      } else {
          dateDateTo = false;
      }

      if(dateDateTo != false){
        if (dateDateFrom != false) {
          if(dateDateFrom.getTime() > dateDerFrom.getTime() || dateDateTo.getTime() < dateDerTo.getTime()){
            return false;
          }
        } else {
          if(dateDerTo.getTime() > dateDateTo.getTime()){
            return false;
          }
        }
      } else {
        if(dateDateFrom != false) {
          if(dateDerFrom.getTime() != dateDateFrom.getTime()){
            return false;
          }
        }
      }

      //Protocolo
      if($scope.searchText.ImportNumOrig_Fld == undefined) {
        returnValue = true;
      } else {
        if($scope.searchText.ImportNumOrig_Fld != ''){
          if(value.ImportNumOrig_Fld.indexOf($scope.searchText.ImportNumOrig_Fld) <= -1) {
            return false;
          }
        }
      }

      //Nombre
      if($scope.searchText.Nombre_Fld == undefined) {
        returnValue = true;
      } else {
        if($scope.searchText.Nombre_Fld != ''){
          if(value.Nombre_Fld.indexOf($scope.searchText.Nombre_Fld) <= -1) {
            return false;
          }
        }
      }

      //Documento
      if($scope.searchText.HClin_Fld == undefined) {
        returnValue = true;
      } else {
        if($scope.searchText.HClin_Fld != ''){
          if(value.HClin_Fld.indexOf($scope.searchText.HClin_Fld) <= -1) {
            return false;
          }
        }
      }

      return returnValue;

    }
    $scope.numberOfPages=function(){
      return Math.ceil($scope.getDerivaciones().length/$scope.pageSize); 
    }
    
    /*for (var i=0; i<65; i++) {
        $scope.derivaciones.push(i);
    }*/

    $scope.ajaxUpdate=function(fromSearch){
      var deferred = $q.defer();
      var conAjax = $http.post(__root__ + 'rol/index/jsonResults', {from: fromSearch, to: 100});
      conAjax.success(function(respuesta){
        if (respuesta.length > 0) {
          for (var i = respuesta.length - 1; i >= 0; i--) {
            $scope.derivaciones.push(respuesta[i]);
          };
          $scope.ajaxUpdate(fromSearch + 100);
        } else {
          return false;
        }
      });
      //return the promise
      return deferred.promise;
    }

    $scope.ajaxUpdate(101);
    
}])
.directive("datepicker", function () {
      return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
          var updateModel = function (dateText) {
            scope.$apply(function () {
              ngModelCtrl.$setViewValue(dateText);
            });
          };
          var options = {
            dateFormat: "dd/mm/yy",
            onSelect: function (dateText) {
              updateModel(dateText);
            }
          };
          elem.datepicker(options);
        }
      }
});

//We already have a limitTo filter built-in to angular,
//let's make a startFrom filter
app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});