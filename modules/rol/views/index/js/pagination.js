var app=angular.module('ui.bootstrap', ['ngAnimate', 'ui.bootstrap']);

// alternate - https://github.com/michaelbromley/angularUtils/tree/master/src/directives/pagination
// alternate - http://fdietz.github.io/recipes-with-angular-js/common-user-interface-patterns/paginating-through-client-side-data.html

app.controller('ROL', ['$scope', '$filter', '$http', '$q', '$log', function ($scope, $filter, $http, $q, $log) {
    $scope.noVistos = false;
    $scope.currentPage = 0;
    $scope.pageSize = 15;
    $scope.derivaciones = [];
    $scope.searchText = '';
    $scope.selectAno = String(new Date().getFullYear());
    $scope.vecesMaxBusqueda = 3;
    $scope.vecesBusqueda = 0;
    $scope.fromSearch = 30;
    $scope.filtrados;

    $scope.dateFilter = function(derivacion){

        var dayFrom;
        var monthFrom;
        var yearFrom;
        var dateDerFrom;

        var dateDayFrom;
        var dateMonthFrom;
        var dateYearFrom;
        var dateDateFrom;

        var dayTo;
        var monthTo;
        var yearTo;
        var dateDerTo;

        var dateDayTo;
        var dateMonthTo;
        var dateYearTo;
        var dateDateTo;

        if ($scope.datePicker == undefined || $scope.datePicker == '') {
            dateDateFrom = false;
        } else {
            dayFrom = $filter('limitTo')(derivacion.Fecha_Fld, 2, 6);
            monthFrom = $filter('limitTo')(derivacion.Fecha_Fld, 2, 4);
            yearFrom = $filter('limitTo')(derivacion.Fecha_Fld, 4, 0);
            dateDerFrom = new Date(yearFrom, monthFrom, dayFrom, 0, 0, 0, 0);

            dateDayFrom = $filter('limitTo')($scope.datePicker, 2, 0);
            dateMonthFrom = $filter('limitTo')($scope.datePicker, 2, 3);
            dateYearFrom = $filter('limitTo')($scope.datePicker, 4, 6);
            dateDateFrom = new Date(dateYearFrom, dateMonthFrom, dateDayFrom, 0, 0, 0, 0);
        }

        if ($scope.datePickerHasta == undefined || $scope.datePickerHasta == '') {
            dateDateTo = false;
        } else {
            dayTo = $filter('limitTo')(derivacion.Fecha_Fld, 2, 6);
            monthTo = $filter('limitTo')(derivacion.Fecha_Fld, 2, 4);
            yearTo = $filter('limitTo')(derivacion.Fecha_Fld, 4, 0);
            dateDerTo =  new Date(yearTo, monthTo, dayTo, 0, 0, 0, 0);

            dateDayTo = $filter('limitTo')($scope.datePickerHasta, 2, 0);
            dateMonthTo = $filter('limitTo')($scope.datePickerHasta, 2, 3);
            dateYearTo = $filter('limitTo')($scope.datePickerHasta, 4, 6);

            dateDateTo = new Date(dateYearTo, dateMonthTo, dateDayTo, 0, 0, 0, 0);
        }

        if(dateDateTo != false){
            if (dateDateFrom != false) {
                if(dateDateFrom.getTime() > dateDerFrom.getTime() || dateDateTo.getTime() < dateDerTo.getTime()){
                    return false;
                }
            } else {
                if(dateDerTo.getTime() > dateDateTo.getTime()){
                    return false;
                }
            }
        } else {
            if(dateDateFrom != false) {
                if(dateDerFrom.getTime() != dateDateFrom.getTime()){
                    return false;
                }
            }
        }

        return true;

    }

    $scope.filterNoVistos = function(derivacion){

        if($scope.noVistos == true){
            if(derivacion.VecesImp_Fld != 0){
                return false;
            }
        }
        return true;
    }

    $scope.getDerivaciones = function () {

        var searchArray = $filter('filter')($scope.derivaciones, $scope.dateFilter);
        var finalArray = $filter('filter')(searchArray, $scope.searchText);
        finalArray = $filter('filter')(finalArray, $scope.filterNoVistos);
        $scope.numberOfPages = Math.ceil(finalArray.length/$scope.pageSize);
        return finalArray;
    }

    $scope.nextPage = function(filtrados) {
        if(!filtrados) {
            if($scope.currentPage >= $scope.getDerivaciones().length / $scope.pageSize - 1){
                $scope.ajaxUpdate();
            }
            $scope.currentPage = $scope.currentPage + 1;
        } else {
            if($scope.currentPage < $scope.getDerivaciones().length / $scope.pageSize - 1){
                $scope.currentPage = $scope.currentPage + 1;
            }
        }

    }

    $scope.ajaxUpdate = function(){
        var deferred = $q.defer();
        var conAjax = $http.post(__root__ + 'rol/index/jsonResults', {from: $scope.fromSearch, to: $scope.pageSize});
        conAjax.success(function(respuesta){
            if (respuesta.length > 0) {
                $scope.fromSearch += $scope.pageSize;
                for (var i = 0; i < respuesta.length; i++) {
                    $scope.derivaciones.push(respuesta[i]);
                };
                /*if($scope.vecesBusqueda < $scope.vecesMaxBusqueda) {
                    $scope.ajaxUpdate($scope.fromSearch);
                    $scope.vecesBusqueda += 1;
                }*/
            } else {
                return false;
            }
        });
        //return the promise
        return deferred.promise;
    }

    //$scope.ajaxUpdate();

}])
    .directive("datepicker", function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, elem, attrs, ngModelCtrl) {
                var updateModel = function (dateText) {
                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(dateText);
                    });
                };
                var options = {
                    dateFormat: "dd/mm/yy",
                    onSelect: function (dateText) {
                        updateModel(dateText);
                    }
                };
                elem.datepicker(options);
            }
        }
    });

//We already have a limitTo filter built-in to angular,
//let's make a startFrom filter
app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});