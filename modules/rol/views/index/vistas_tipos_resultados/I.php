<?php

if(isset($resultado['RESULTADOS'])) {
    $texto_resultado_anterior = '';
    $encabezado = '';
    if (isset($resultado['ENCABEZADO']))
        $encabezado = $resultado['ENCABEZADO'];
    ?>
    <div class="col-md-12 col-xs-12 no-padding resultado" style="background: <?php echo $tags['FondoResultado']; ?>">
        <div class="col-md-12 col-xs-12">
            <?php
            echo "<h4>" . $encabezado . "</h4>";
            ?>
        </div>
        <?php
        $nroResultadoIncluido = 1;
        foreach ($resultado['RESULTADOS'] as $key => $res) {
            ?>
            <!-- <?php echo "Número Resultado Incluído: $nroResultadoIncluido - Nueva línea: " . $res['NUEVA_LINEA'] . " - " . $res['ID']; ?> -->

            <?php
            if (!isset($res['NUEVA_LINEA'])) $res['NUEVA_LINEA'] = 1;
            if ($res['NUEVA_LINEA'] == 1) $nroResultadoIncluido = 0;
            ?>


            <?php if (($nroResultadoIncluido % 2 != 0 && $res['NUEVA_LINEA'] == 0) || $res['NUEVA_LINEA'] == 1) { ?>
                <div class="col-md-12 col-xs-12 no-padding">
            <?php } ?>

            <div class="col-md-6 col-xs-6 no-padding" style="padding-left:<?php echo $tags['TabTexto'] . 'px'; ?>">

                <?php

                $valor_resultado = "<" . $tags['Resultados'] . ">" . $res['RESULTADO'] . "</" . $tags['Resultados'] . ">" . "<" . $tags['Unidades'] . ">" . $res['UNIDADES'] . "</" . $tags['Unidades'] . ">";

                $hasResult = false;
                if(strpos($res['TEXTO_RESULTADO'], '&r') !== false) {
                    $hasResult = true;
                    $texto_resultado = str_replace('&r', $valor_resultado, $res['TEXTO_RESULTADO']);
                }

                if(strpos($res['TEXTO_RESULTADO'], '&R') !== false) {
                    $hasResult = true;
                    $texto_resultado = str_replace('&R', $valor_resultado, $res['TEXTO_RESULTADO']);
                }

                if(!$hasResult)
                    $texto_resultado = $res['TEXTO_RESULTADO'] . " " . $res['RESULTADO'];

                if (is_array($res['ANTERIORES']) && count($res['ANTERIORES']) > 0) {
                    $id = uniqid();
                    ?>

                    <button class="btn btn-info" data-toggle="collapse"
                            data-target="#collapse-<?php echo $id; ?>">+
                    </button>

                <?php } ?>

                <!-- Resultado: -->
                <?php
                echo "<" . $tags['Predeterminada'] . ">";
                echo $texto_resultado;
                echo "</" . $tags['Predeterminada'] . ">";
                ?>

            </div>

            <?php
            if (is_array($res['ANTERIORES']) && count($res['ANTERIORES']) > 0) {
                ?>

                <div class="col-md-12 col-xs-12">

                    <div id="collapse-<?php echo $id; ?>" class="collapse col-md-12 col-xs-12">
                        <?php foreach ($res['ANTERIORES'] as $anterior) { ?>
                            <div class="col-md-12 col-xs-12 no-padding">
                                <div class="col-md-4 col-xs-4 no-padding">
                                    <?php
                                    echo substr($anterior['fecha'], 6) . "/" . substr($anterior['fecha'], 4, 2) . "/" . substr($anterior['fecha'], 0, 4);
                                    ?>
                                </div>
                                <div class="col-md-4 col-xs-4 no-padding">
                                    <?php
                                    echo "<" . $tags['Anteriores'] . ">" . $anterior['valor1'] . "</" . $tags['Anteriores'] . ">";
                                    ?>
                                </div>
                                <div class="col-md-4 col-xs-4 no-padding">
                                    <?php
                                    if (isset($anterior['valor2']) && $anterior['valor2'] != '') {
                                        echo "<" . $tags['Anteriores'] . ">" . $anterior['valor2'] . "</" . $tags['Anteriores'] . ">";
                                    }
                                    echo "<" . $tags['Unidades'] . ">" . $anterior['unidades'] . "</" . $tags['Unidades'] . ">" . "<br>";
                                    ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>

            <?php } ?>

            <?php
            if ($res['METODO'] != '' ||
                $res['VALOR_REFERENCIA'] != '' ||
                $res['MATERIAL'] != ''
            ) {
                ?>
                <div class="col-md-12 col-xs-12 no-padding">
                    <div class="col-md-12 col-xs-12 no-padding"
                         style="padding-left:<?php echo $tags['TabTexto'] . 'px'; ?>">
                        <ul>
                            <?php
                            $br = "";
                            if ($res['METODO'] != '') {
                                echo "<" . $tags['Metodos'] . ">" . "Método: " . $res['METODO'] . "</" . $tags['Metodos'] . ">";
                                $br = "<br>";
                            }
                            if ($res['VALOR_REFERENCIA'] != '') {
                                echo $br . "<" . $tags['Referencias'] . ">" . "Valor de referencia: " . $res['VALOR_REFERENCIA'] . "</" . $tags['Referencias'] . ">";
                                $br = "<br>";
                            }
                            if ($res['MATERIAL'] != '') {
                                echo $br . "<" . $tags['Materiales'] . ">" . "Material: " . $res['MATERIAL'] . "</" . $tags['Materiales'] . ">";
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            <?php } ?>

            <?php if (($nroResultadoIncluido % 2 != 0 && $res['NUEVA_LINEA'] == 0) || $res['NUEVA_LINEA'] == 1) { ?>
                </div>  <!-- Determinación Incluída 0 -->
            <?php } ?>

            <?php $nroResultadoIncluido++;
        } ?>

        <?php
        ?>
    </div> <!-- Fin resultado -->
    <?php
}
?>