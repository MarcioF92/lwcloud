<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-ms-12 col-xs-12">
    <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12">
        <div class="col-lg-4 col-md-4 col-ms-4 col-xs-12 text-left">
            <img class="col-lg-12 col-md-12 col-ms-12 col-xs-12" src="<?php echo PRIMARY_URL; ?>/public/img/labwin-logo.png" />
        </div>
        <div style="padding: 10px;" class="col-lg-8 text-center-xs text-right">
            <h2 class="title light-blue-color"><?php echo $this->_customVars['titulo']; ?></h2>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $( ".datepicker" ).datepicker({
            inline: true
        });

        $("ul.sidebar-tabs li a").click(function(){
            $("li.active").removeClass('active');
            $("div.tab-pane.active").removeClass('active');

            $(this).parent().addClass('active');
            var content_id = $(this).attr("data-tab");
            $(content_id).addClass('active');

        });
    });
</script>

<div ng-controller="ROL">

    <div id="rol-filters" class="col-lg-10 col-lg-offset-1 col-xs-12">

        <input type="hidden" id="filtrados" name="filtrados" ng-model="filtrados" value="<?php echo $this->_customVars['filtrados']; ?>">

        <form class="col-lg-12 col-md-12" method="post" action="">

            <input type="hidden" name="buscar" value="1">

            <div id="categoria-filtros"  class="col-lg-6 col-md-6 col-xs-12 text-left hidden-xs hidden-sm">
                <ul class="nav nav-tabs sidebar-tabs text-left" id="sidebar" role="tablist">

                    <li class="active col-lg-2 col-xs-4 pull-left">
                        <a data-tab="#tab-1" role="tab" data-toggle="tab">Fechas</a>
                    </li>
                    <li class="col-lg-2 col-xs-4 pull-left">
                        <a data-tab="#tab-2" role="tab" data-toggle="tab">Protocolo</a>
                    </li>
                    <li class="col-lg-2 col-xs-4 pull-left">
                        <a data-tab="#tab-3" role="tab" data-toggle="tab">Paciente</a>
                    </li>

                </ul>
            </div>

            <div class="col-lg-2 col-md-2 col-xs-12">
                <button type="submit" class="btn btn-info">Buscar</button> -
                <a href="<?php echo BASE_URL; ?>rol">Limpiar búsqueda</a>
            </div>

            <div class="col-lg-2 col-md-2 col-xs-12">
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <p class="pull-right">Paginación:</p>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <select ng-model="pageSize" id="pageSize" class="form-control" placeholder="Paginación">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15" selected>15</option>
                        <option value="20">20</option>
                    </select>
                </div>
            </div>

        </div>

        <div id="categoria-filtros"  class="col-lg-6 col-md-6 col-xs-12 text-left hidden-md hidden-lg">
            <ul class="nav nav-tabs sidebar-tabs text-left" id="sidebar" role="tablist">

                <li class="active col-lg-2 col-xs-4 pull-left">
                    <a data-tab="#tab-1" role="tab" data-toggle="tab">Fechas</a>
                </li>
                <li class="col-lg-2 col-xs-4 pull-left">
                    <a data-tab="#tab-2" role="tab" data-toggle="tab">Protocolo</a>
                </li>
                <li class="col-lg-2 col-xs-4 pull-left">
                    <a data-tab="#tab-3" role="tab" data-toggle="tab">Paciente</a>
                </li>

            </ul>
        </div>


        <div class="tab-content col-lg-12 col-xs-12">

            <div class="tab-pane active col-lg-10 col-lg-offset-1" id="tab-1">
                <input type="text" name="fecha_desde" class="datepicker fullwith-mobile" ng-model="datePicker" ng-change="currentPage=0" placeholder="Desde/Fecha exacta"
                       value="<?php if(isset($this->_customVars['data']['fecha_desde'])) echo $this->_customVars['data']['fecha_desde']; ?>" datepicker>
                <input type="text" name="fecha_hasta" class="datepicker fullwith-mobile" ng-model="datePickerHasta" ng-change="currentPage=0" placeholder="Hasta"
                       value="<?php if(isset($this->_customVars['data']['fecha_desde'])) echo $this->_customVars['data']['fecha_hasta']; ?>" datepicker>
            </div>

            <div class="tab-pane col-lg-10 col-lg-offset-1" id="tab-2">
                <input type="text" name="protocolo" ng-model="searchText.ImportNumOrig_Fld" ng-change="currentPage=0" placeholder="Protocolo" class="fullwith-mobile">
            </div>

            <div class="tab-pane col-lg-10 col-lg-offset-1" id="tab-3">
                <input type="text" name="nombre" ng-model="searchText.Nombre_Fld" ng-change="currentPage=0" placeholder="Nombre" class="fullwith-mobile">
                <input type="text" name="documento" ng-model="searchText.HClin_Fld" ng-change="currentPage=0" placeholder="Documento" class="fullwith-mobile">
            </div>

        </div>

    </form>

    <div class="col-lg-10 col-lg-offset-1 col-xs-12 no-padding" ng-init="derivaciones = <?php echo htmlspecialchars(json_encode($this->_customVars['derivaciones'])); ?>">
        <div class="col-lg-12 table-header hidden-xs hidden-sm hidden-md no-padding">
            <div class="col-lg-2"><p>Fecha</p></div>
            <div class="col-lg-2"><p>Protocolo</p></div>
            <div class="col-lg-2"><p>Apellido y nombre</p></div>
            <div class="col-lg-2"><p>Documento</p></div>
            <div class="col-lg-2"><p>Fecha de entrega</p></div>
            <div class="col-lg-2">
                <p>Estado </p>
            </div>
        </div>
        <p class="pull-right"><input type="checkbox" ng-model="noVistos"> No leídos</p>

        <div class="col-lg-12 col-xs-12 custom-form-control no-padding" ng-repeat="derivacion in getDerivaciones() | startFrom:currentPage*pageSize | limitTo:pageSize">
            <div class="col-lg-2 col-xs-4 word-break hidden-xs hidden-sm">
                {{derivacion.Fecha_Fld | limitTo : 2 : 6}}/{{derivacion.Fecha_Fld | limitTo : 2 : 4}}/{{derivacion.Fecha_Fld | limitTo : 4 : 0}}
            </div>
            <div class="col-lg-2 col-xs-3 word-break">{{derivacion.ImportNumOrig_Fld}}</div>
            <div class="col-lg-2 col-xs-4 no-padding word-break">{{derivacion.Nombre_Fld}}</div>
            <div class="col-lg-2 col-xs-3 word-break hidden-xs hidden-sm">{{derivacion.HClin_Fld}}</div>
            <div class="col-lg-2 col-xs-3 word-break hidden-xs hidden-sm">
                <div ng-if="derivacion.Entrega_Fld != ''">
                    {{derivacion.Entrega_Fld | limitTo : 2 : 6}}/{{derivacion.Entrega_Fld | limitTo : 2 : 4}}/{{derivacion.Entrega_Fld | limitTo : 4 : 0}}
                </div>
            </div>
            <div class="col-lg-2 col-xs-5 word-break">
                {{derivacion.Estado_Fld}}
                <div class="pull-right">
                    <span ng-if="derivacion.VecesImp_Fld == 0">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    </span>
                    <a href="<?php echo BASE_URL . 'rol/index/ver_resultado/';?>{{derivacion.Numero_Fld}}/{{derivacion.Sucursal_Fld}}">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>


        <div class="col-lg-12 col-xs-12 text-right">
            <button class="pagination-button-left" ng-disabled="currentPage == 0" ng-click="currentPage=currentPage-1">
                <i class="fa fa-caret-left" aria-hidden="true"></i>
            </button>
            Página {{currentPage+1}} de {{numberOfPages}}
            <button class="pagination-button-right" ng-click="nextPage(<?php echo $this->_customVars['filtrados']; ?>)">
                <i class="fa fa-caret-right" aria-hidden="true"></i>
            </button>
        </div>

    </div>

</div>
