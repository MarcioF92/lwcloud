<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-ms-12 col-xs-12">
    <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12">
        <div class="col-lg-4 col-md-4 col-ms-4 col-xs-12 text-left">
            <img class="col-lg-12 col-md-12 col-ms-12 col-xs-12" src="<?php echo PRIMARY_URL; ?>/public/img/labwin-logo.png" />
        </div>
        <div style="padding: 10px;" class="col-lg-8 text-center-xs text-right">
            <h2 class="title light-blue-color"><?php echo $this->_customVars['titulo']; ?></h2>
        </div>
    </div>
</div>

<div>

    <div id="rol-filters" class="col-lg-10 col-lg-offset-1 col-xs-12">

        <form class="col-lg-12 col-md-12" method="GET" action="">

            <input type="hidden" name="buscar" value="1">

            <div class="tab-pane col-lg-12" id="tab-3">
                <p>Debe ingresar al menos cuatro números para realizar la búsqueda</p>
                <input type="text" name="documento" placeholder="Documento" class="custom-form-control">
                <button type="submit" class="btn btn-info">Buscar</button>
            </div>

        </form>

    </div>

    <div class="col-lg-10 col-lg-offset-1 col-xs-12 no-padding" ng-init="derivaciones = <?php echo htmlspecialchars(json_encode($this->_customVars['derivaciones'])); ?>">
        <div class="col-lg-12 table-header hidden-xs hidden-sm hidden-md no-padding">
            <div class="col-lg-3"><p>Apellido y nombre</p></div>
            <div class="col-lg-3"><p>Documento</p></div>
            <div class="col-lg-3">
                <p>Ver</p>
            </div>
        </div>

        <?php foreach ($this->_customVars['pacientes'] as $paciente) : ?>
        <div class="col-lg-12 col-xs-12 custom-form-control no-padding">
            <div class="col-lg-3 col-xs-4 word-break hidden-xs hidden-sm">
                <?php echo $paciente['Nombre_Fld']; ?>
            </div>
            <div class="col-lg-3 col-xs-3 word-break">
                <?php echo $paciente['HClin_Fld']; ?>
            </div>
            <div class="col-lg-3">
                <div class="pull-right">
                    <a href="<?php echo BASE_URL . 'rol/index/ver_determinaciones_por_fechas/'.$paciente['HClin_Fld'];?>">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
        <?php endforeach; ?>

    </div>

</div>
