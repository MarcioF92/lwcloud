<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-ms-12 col-xs-12">
    <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12">
        <div class="col-lg-4 col-md-4 col-ms-4 col-xs-12 text-left">
            <img class="col-lg-12 col-md-12 col-ms-12 col-xs-12" src="<?php echo PRIMARY_URL; ?>/public/img/labwin-logo.png" />
        </div>
        <div style="padding: 10px;" class="col-lg-8 text-center-xs text-right">
            <h2 class="title light-blue-color"><?php echo $this->_customVars['titulo']; ?></h2>
        </div>
    </div>
</div>

<div>

    <div id="rol-filters" class="col-lg-10 col-lg-offset-1 col-xs-12">

        <h2>Análisis realizados a <?php echo $this->_customVars['paciente']['Nombre_Fld']; ?></h2>

        <div class="pull-right">
            <a href="<?php echo $this->_layoutParams['base_url']; ?>rol/index/resultados_por_fechas">Volver</a>
        </div>

    </div>

    <div class="col-lg-10 col-lg-offset-1 col-xs-12 no-padding" ng-init="derivaciones = <?php echo htmlspecialchars(json_encode($this->_customVars['derivaciones'])); ?>">
        <div class="col-lg-12 table-header hidden-xs hidden-sm hidden-md no-padding">
            <div class="col-lg-6"><p>Práctica</p></div>
            <div class="col-lg-6">
                <p>Ver</p>
            </div>
        </div>

        <?php foreach ($this->_customVars['practicas'] as $abrev => $practicas) : ?>
            <div class="col-lg-12 col-xs-12 custom-form-control no-padding">
                <div class="col-lg-6 word-break">
                    <?php echo $abrev; ?>
                </div>
                <div class="col-lg-6">
                    <div class="pull-right">
                        <a href="#" data-toggle="collapse" data-target="#collapse-<?php echo $abrev; ?>">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                <div id="collapse-<?php echo $abrev; ?>" class="collapse col-lg-6" style="padding-left: 15px!important;">
                    <?php foreach ($practicas as $fecha => $resultados) : ?>
                        <p>
                            <?php
                                $date = new DateTime($fecha);
                                echo $date->format('d-m-Y');
                            ?>:
                        </p>
                        <ul>
                            <?php foreach ($resultados as $resultado) : ?>
                                <li><?php echo $resultado['determinaciones'][$abrev]['resultados'][0]['RESULTADO']; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endforeach; ?>

    </div>

</div>
