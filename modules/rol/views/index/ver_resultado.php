<style>
    #resultados-container{
        font-family: <?php echo $this->_customVars['fuente']; ?>!important;
    }
</style>
<div class="col-md-10 col-md-offset-1">
    <div class="col-md-12">
        <div class="col-md-4 text-left hidden-xs hidden-sm">
            <img class="col-md-12 col-xs-12" src="<?php echo PRIMARY_URL; ?>public/img/labwin-logo.png" />
        </div>
        <div style="padding: 10px;" class="col-md-8 text-right">
            <h2 class="title light-blue-color col-xs-12">
                <?php echo $this->_customVars['titulo']; ?>
            </h2>
        </div>
    </div>
</div>

<div id="resultado-blue-strip" class="col-md-12 col-xs-12">
    <div class="col-md-10 col-md-offset-1 col-xs-12">
        <div class="col-md-6 col-xs-6">
            <p class="big-font no-margin no-padding"><i class="fa fa-eye white-color" aria-hidden="true"></i></p>
        </div>
        <div class="col-md-6 text-right col-xs-6">
            <?php
                if($this->_customVars['configs']['imprimir_pdf_incompleto'] ||
                    $this->_customVars['resultado']['estado'] == 'Completo' ||
                    $this->_customVars['resultado']['estado'] == 'Actualizado') {

                    $pdfUrl = $this->_layoutParams['base_url'] . 'rol/index/ver_resultado_pdf/' . $this->_customVars['datos_derivacion']['idderivacion'] . '/'. $this->_customVars['datos_derivacion']['sucursal'];
                if($this->_customVars['role'] == 6) {
                    $pdfUrl .= '/' . $this->_customVars['role'] . '/' .
                        $this->_customVars['user'];
                }
            ?>
                <a class="custom-form-control" href="<?php echo $pdfUrl; ?>">Ver en PDF <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>

            <?php } ?>

            <?php
            $backURL = 'rol';
            if (Session::get('role') == 6) {
                $backURL = 'admin';
            }
            ?>
            <a class="custom-form-control" href="<?php echo $this->_layoutParams['base_url'] . $backURL?>">Volver <i class="fa fa-reply" aria-hidden="true"></i></a>
        </div>
    </div>
</div>

<div class="col-md-10 col-md-offset-1 col-xs-12 no-padding">

    <div class="col-md-12 hidden-xs hidden-sm">
        <div class="col-md-3 text-left">
            <?php if(!empty($this->_customVars['resultado']['textos'][0]['imagen'])) { ?>
                <img style="max-width: 250px;" src="<?php echo PRIMARY_URL . 'laboratorios/assets/' . $_GET['lab'] . '/labfiles/' . $this->_customVars['resultado']['textos'][0]['imagen']; ?>">
            <?php } ?>
        </div>
        <div class="col-md-6 text-center">
            <h2><?php echo $this->_customVars['resultado']['textos'][0]['titulo']; ?></h2>
            <p><?php echo $this->_customVars['resultado']['textos'][0]['texto']; ?></p>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 no-padding">
        <div class="col-md-4 col-xs-12">
            <p>Paciente: <?php echo $this->_customVars['resultado']['datos'][0]['Paciente']; ?></p>
            <p>Protocolo: <?php echo $this->_customVars['resultado']['datos'][0]['Protocolo']; ?></p>
        </div>
        <div class="col-md-4 col-xs-12">
            <p>Fecha: <?php echo substr($this->_customVars['resultado']['datos'][0]['Fecha'], 6) . "/" . substr($this->_customVars['resultado']['datos'][0]['Fecha'], 4, 2) . "/" . substr($this->_customVars['resultado']['datos'][0]['Fecha'], 0, 4); ?></p>

            <?php if($this->_customVars['resultado']['datos'][0]['MedicoFromNum'] != ''){ ?>
                <p>Solicita: <?php echo $this->_customVars['resultado']['datos'][0]['MedicoFromNum']; ?></p>
            <?php } else { ?>
                <?php if($this->_customVars['resultado']['datos'][0]['Medico'] != ''){ ?>
                    <p>Solicita: <?php echo $this->_customVars['resultado']['datos'][0]['Medico']; ?></p>
                <?php } ?>
            <?php } ?>
        </div>
    </div>

    <div id="resultados-container" class="col-md-12 col-xs-12">
        <?php if(!$this->_customVars['configs']['mostrar_resultados_incompletos'] &&
            in_array($this->_customVars['resultado']['estado'], ['Ingresado', 'Parcial'])) { ?>

            <div class="col-md-12">
                El resultado no se ha cargado en su totalidad, vuelva a intentarlo más tarde
            </div>

        <?php
        } else {

            $isFirst = true;
            $isFirstSinSaltos = true;

            $tags = [
                'Predeterminada' => $this->_customVars['formatos_determinaciones']['DEFAULT']['Predeterminada'],
                'Unidades' => $this->_customVars['formatos_determinaciones']['DEFAULT']['Unidades'],
                'Metodos' => $this->_customVars['formatos_determinaciones']['DEFAULT']['Metodos'],
                'Resultados' => $this->_customVars['formatos_determinaciones']['DEFAULT']['Resultados'],
                'Materiales' => $this->_customVars['formatos_determinaciones']['DEFAULT']['Materiales'],
                'Referencias' => $this->_customVars['formatos_determinaciones']['DEFAULT']['Referencias'],
                'Anteriores' => $this->_customVars['formatos_determinaciones']['DEFAULT']['Anteriores'],
                'TabTexto' => $this->_customVars['formatos_determinaciones']['DEFAULT']['TabTexto'],
                'TabResultado' => $this->_customVars['formatos_determinaciones']['DEFAULT']['TabResultado'],
                'Interlineado' => $this->_customVars['formatos_determinaciones']['DEFAULT']['Interlineado'],
                'FondoResultado' => $this->_customVars['formatos_determinaciones']['DEFAULT']['FondoResultado'],
            ];

            ?>

            <style>
                .determinacion{
                    margin-top: <?php echo $tags['Interlineado'] . 'px'; ?>!important;
                }
            </style>

            <?php
            foreach ($this->_customVars['resultado']['determinaciones'] as $determinacion) {

                if(array_key_exists($determinacion['id'], $this->_customVars['formatos_determinaciones'])){
                    $tags['Predeterminada'] = $this->_customVars['formatos_determinaciones'][$determinacion['id']]['Predeterminada'];
                    $tags['Unidades'] = $this->_customVars['formatos_determinaciones'][$determinacion['id']]['Unidades'];
                    $tags['Metodos'] = $this->_customVars['formatos_determinaciones'][$determinacion['id']]['Metodos'];
                    $tags['Resultados'] = $this->_customVars['formatos_determinaciones'][$determinacion['id']]['Resultados'];
                    $tags['Materiales'] = $this->_customVars['formatos_determinaciones'][$determinacion['id']]['Materiales'];
                    $tags['Referencias'] = $this->_customVars['formatos_determinaciones'][$determinacion['id']]['Referencias'];
                    $tags['Anteriores'] = $this->_customVars['formatos_determinaciones'][$determinacion['id']]['Anteriores'];
                }

            ?>
                <div id="determinacion" class="col-md-12 col-xs-12 determinacion">
                    <?php
                    if (($determinacion['encabezado'] != '') &&
                        ($determinacion['encabezado'] != '<br>') &&
                        ($determinacion['encabezado'] != '<BR>')) {
                            //echo "<pre>".htmlspecialchars(substr($determinacion['encabezado'], 0, 4))."</pre>";
                            while(substr($determinacion['encabezado'], 0, 4) == '<br>' ||  substr($determinacion['encabezado'], 0, 4) == '<BR>'){
                                $determinacion['encabezado'] = substr($determinacion['encabezado'], 4);
                            }
                            echo "<h2>" . preg_replace("/<BR\w?\/?>/", "<p style=\"font-size: 12px\">", $determinacion['encabezado']) . "</small></h2>";
                    }
                    ?>

                    <?php $nroResultado = 1; ?>

                    <?php foreach ($determinacion['resultados'] as $resultado) { ?>

                        <?php if(!isset($resultado['NUEVA_LINEA'])) $resultado['NUEVA_LINEA'] = 1; ?>
                        <?php if($resultado['NUEVA_LINEA'] == 1) $nroResultado = 0; ?>

                        <!-- <?php echo "Número Resultado: $nroResultado - Nueva línea: ".$resultado['NUEVA_LINEA']; ?> -->

                        <?php if (($nroResultado % 2 != 0 && $resultado['NUEVA_LINEA'] == 0) || $resultado['NUEVA_LINEA'] == 1) { ?>
                            <div class="col-md-12 col-xs-12 no-padding" style="background: <?php echo $tags['FondoResultado']; ?>">
                        <?php } ?>

                            <?php if($resultado['TIPO'] == "RV") {
                                require 'vistas_tipos_resultados/RV.php';
                            } ?>

                            <?php if($resultado['TIPO'] == "I") {
                                require 'vistas_tipos_resultados/I.php';
                            } ?>

                            <?php if($resultado['TIPO'] == "IM") {
                                require 'vistas_tipos_resultados/IM.php';
                            } ?>

                            <?php if($resultado['TIPO'] == "T") {
                                require 'vistas_tipos_resultados/T.php';
                            } ?>

                        <?php if ($nroResultado % 2 == 0 || $resultado['NUEVA_LINEA'] == 1) { ?>
                            </div>  <!-- Determinación 0 -->
                        <?php } ?>

                    <?php $nroResultado++; } ?>

                </div> <!-- Determinación 1 -->
        <?php } ?>

            <div id="pie-resultado" class="col-md-12 col-xs-12 text-right">
                <p><?php echo $this->_customVars['resultado']['textos'][0]['pie']; ?></p>
                <?php if(!empty($this->_customVars['resultado']['textos'][0]['firma'])) { ?>
                    <img style="max-width: 250px;" src = "<?php echo PRIMARY_URL . 'laboratorios/assets/' . $_GET['lab'] . '/labfiles/' . $this->_customVars['resultado']['textos'][0]['firma']; ?>" />
                    <?php
                }
                ?>
            </div>

        <?php } ?>


    </div> <!-- Determinación 2 -->

</div>
