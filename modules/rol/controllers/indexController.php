<?php

/*
* Index Controller of ROL
* /public_html/laboratorios/mobile/modules/rol/controllers/indexController.php
*/

class MYPDF extends TCPDF {

    private $_name;

    public function __construct($arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $datos, $textos){
      parent::__construct($arg1, $arg2, $arg3, $arg4, $arg5, $arg6);
      $this->_datos = $datos;
      $this->_textos = $textos;
      $this->logo_url = $this->_textos['imagen'];
      $this->firma_url = $this->_textos['firma'];
      $this->pie = $this->_textos['pie'];
    }

    //Page header
    public function Header() {
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Title
        $html = "<table>";
        $html .= "<tbody>";
        $html .= "<tr>";
        $html .= "<td width=\"400\">";
        $html .= date("d-m-Y h:i:s");
        $html .= " - ";
        $html .= $this->_datos['Paciente'];
        $html .= "</td>";
        $html .= "<td align=\"right\">";
        $html .= 'Página '.$this->getAliasNumPage().'/'.$this->getAliasNbPages();
        $html .= "</td>";
        $html .= "</tr>";
        $html .= "</tbody>";
        $html .= "</table>";

        $this->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        // Título
        $html = "<table><tbody>";
        $html .= "<tr>";

        /*if(!empty($this->logo_url)) {
            $html .= "<td align=\"left\">";

            $html .= "<img height=\"70\" style=\"float:left;\" src=\"" . 'http://'.SUBDOMAIN.'.biodatasa.com.ar/laboratorios/assets/' . SUBDOMAIN . '/' . $this->logo_url . "\" />";

            $html .= "</td>";
        }*/
        $html .= "<td align=\"center\">";
        $html .= '<h3>'. str_replace('\r\n', '<br>', $this->_textos['titulo']) . '</h3>';
        $html .= "</td>";
        $html .= "</tr>";
        $html .= "</tbody></table>";
        $html .= "<small>".str_replace('\r\n', '<br>', $this->_textos['texto']) . '</small><br>';

        $this->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        $this->SetFont('courier', '', 10, '', true);

        $html = "<table style=\"margin-bottom: 3px;margin-top:3px\"><tbody>";
        $html .= "<tr>";
        $html .= "<td width=\"400\">";
        $html .= "Paciente: " . $this->_datos['Paciente'];
        $html .= "<br><br>";
        $html .= "Protocolo: " . $this->_datos['Protocolo'];
        $html .= "</td>";
        $html .= "<td width=\"400\">";
        $html .= "Fecha: " . $this->parseFecha($this->_datos['Fecha']);
        $html .= "<br><br>";
        $html .= "Solicita: " . $this->_datos['Solicita'];
        $html .= "</td>";
        $html .= "</tr>";
        $html .= "</tbody></table>";

        $this->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        //$this->SetFont($fuente['pdf-family'], '', 11, '', true);

        // Línea cabecera
        $html = '<table>';
        $html .= '<tbody>';
        $html .= '<tr>';
        $html .= '<td>';

        $this->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Página '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

    public function parseFecha($fecha){
        return substr($fecha, 6) . "/" . substr($fecha, 4, 2) . "/" . substr($fecha, 0, 4);
    }
}

class indexController extends Controller
{
    private $_rolModel;

    public function __construct(){
        parent::__construct();

        $this->_rolModel = $this->loadModel('rol');
        $this->_view->setTemplate('rol_mol_template');
        $this->_view->assign('body-classes', array('rol-mol-background'));

    }

    public function index(){
        $this->_acl->access('rol');
        $this->_view->assign('titulo','Resultados Online');
        $this->_view->setJs(array('pagination'));
        $this->_view->assign('derivaciones', $this->_rolModel->getDerivaciones(Session::get('role'), Session::get('iduser')));

        $filtrados = 0;

        if($_POST['buscar']){
            $this->_view->assign('data',$_POST);
            $filtrados++;
        }

        $this->_view->assign('filtrados',$filtrados);

        $this->_view->render('index', 'rol');
    }

    public function ver_resultado($idderivacion = false, $sucursal = false, $userRole = false, $idUser = false){

        $configs = $this->_rolModel->getConfigs();

        /*
         * Visualización de resultados, posibles valores: solo_pdf, ambos, solo_web
         */

        if($configs['visualizacion_resultados'] == 'solo_pdf')
            $this->ver_resultado_pdf($idderivacion, $sucursal, $userRole, $idUser);

        if (!$idderivacion) {
            $this->redirect('rol');
        }
        $this->_acl->access('rol');

        $sucursal = (int) $sucursal;

        if(Session::get('role') != 6) {
            $role = Session::get('role');
            $user = Session::get('iduser');
            if (!$this->_rolModel->esMiDerivacion($sucursal, $idderivacion, Session::get('role'), Session::get('iduser')) && Session::get('role') != 6) {
                $this->redirect('error/access/error/5050');
            }
        } else {
            if (!$userRole || !$idUser) {
                $this->redirect('admin');
            }
            $role = $userRole;
            $user = $idUser;
        }
        
        $this->_view->assign('role', $role);
        $this->_view->assign('user', $user);
        
        $this->_view->assign('titulo','Ver resultado');
        $this->_view->assign('datos_derivacion', array('sucursal' => $sucursal, 'idderivacion' => $idderivacion));
        $formatos = $this->_rolModel->getFormatos();
        $formatosFinal = [];
        foreach ($formatos as $formato){
            $formatosFinal[$formato['Abrev_Fld']] = $formato;
        }
        $this->_view->assign('formatos_determinaciones', $formatosFinal);
        $fuente = $this->_rolModel->getFuente($formatosFinal['DEFAULT']['Fuente']);
        $this->_view->assign('fuente', $fuente['font-family']);
        $this->_view->assign('resultado', $this->_rolModel->getResultado($sucursal, $idderivacion, $role, $user, Session::get('role')));
        $this->_view->assign('configs', $this->_rolModel->getConfigs());

        $this->_view->render('ver_resultado', 'rol');
    }

    public function ver_resultado_pdf($idderivacion = false, $sucursal = false, $userRole = false, $idUser = false)
    {
        if (!$idderivacion) {
            $this->redirect('rol');
        }
        $this->_acl->access('rol');

        $sucursal = (int) $sucursal;

        if(Session::get('role') != 6) {
            $role = Session::get('role');
            $user = Session::get('iduser');
            if (!$this->_rolModel->esMiDerivacion($sucursal, $idderivacion, Session::get('role'), Session::get('iduser')) && Session::get('role') != 6) {
                $this->redirect('error/access/error/5050');
            }
        } else {
            if (!$userRole || !$idUser) {
                $this->redirect('admin');
            }
            $role = $userRole;
            $user = $idUser;
        }

        $sucursal = (int) $sucursal;

        // RESULTADO
        $resultado = $this->_rolModel->getResultado($sucursal, $idderivacion, $role, $user, Session::get('role'));

        if(file_exists(ROOT . DS . 'laboratorios' . DS . 'assets' . DS . $_GET['lab'] . DS . $resultado['datos'][0]['Fecha'] . DS . $sucursal . '-' . $idderivacion . '-01.pdf')){
            // Vamos a mostrar un PDF
            header('Content-type: application/pdf');

            // Se llamará downloaded.pdf
            header('Content-Disposition: attachment; filename="' . $sucursal . '-' . $idderivacion . '-01.pdf'.'"');

            // La fuente de PDF se encuentra en original.pdf
            readfile(ROOT . DS . 'laboratorios' . DS . 'assets' . DS . $_GET['lab'] . DS . $resultado['datos'][0]['Fecha'] . DS . $sucursal . '-' . $idderivacion . '-01.pdf');
            exit;
        }

        if(file_exists(ROOT . DS . 'laboratorios' . DS . 'assets' . DS . $_GET['lab'] . DS . $sucursal . '-' . $idderivacion . '-01.pdf')){
            // Vamos a mostrar un PDF
            header('Content-type: application/pdf');

// Se llamará downloaded.pdf
            header('Content-Disposition: attachment; filename="' . $sucursal . '-' . $idderivacion . '-01.pdf'.'"');

// La fuente de PDF se encuentra en original.pdf
            readfile(ROOT . DS . 'laboratorios' . DS . 'assets' . DS . $_GET['lab'] . DS . $sucursal . '-' . $idderivacion . '-01.pdf');
            exit;
        }

        $formatos = $this->_rolModel->getFormatos();
        $formatos_determinaciones = [];
        foreach ($formatos as $formato) {
            $formatos_determinaciones[$formato['Abrev_Fld']] = $formato;
        }

        $fuente = $this->_rolModel->getFuente($formatos_determinaciones['DEFAULT']['Fuente']);

        $pdf = new MYPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false, $resultado['datos'][0], $resultado['textos'][0]);

        // set document information
        $pdf->SetCreator('LWCLOUD');
        $pdf->SetAuthor('LWCLOUD');
        $pdf->SetTitle('Informe de resultado generado por LWCloud');

        // set default header data
        //$pdf->SetHeaderData(, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
        $pdf->setFooterData(array(0, 64, 0), array(0, 64, 128));

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(PDF_MARGIN_LEFT, "60px", PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(True, PDF_MARGIN_FOOTER);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->setCellHeightRatio(1);
        $pdf->SetCellPadding(0);

        // ---------------------------------------------------------

        $pdf->setFontSubsetting(true);

        $pdf->SetFont($fuente['pdf-family'], '', 12, '', true);

        $pdf->AddPage();

        /*echo "<pre>";
        print_r($resultado);
        echo "</pre>";*/

        $tags = [
            'Predeterminada' => $formatos_determinaciones['DEFAULT']['Predeterminada'],
            'Unidades' => $formatos_determinaciones['DEFAULT']['Unidades'],
            'Metodos' => $formatos_determinaciones['DEFAULT']['Metodos'],
            'Resultados' => $formatos_determinaciones['DEFAULT']['Resultados'],
            'Materiales' => $formatos_determinaciones['DEFAULT']['Materiales'],
            'Referencias' => $formatos_determinaciones['DEFAULT']['Referencias'],
            'Anteriores' => $formatos_determinaciones['DEFAULT']['Anteriores'],
            'TabTexto' => $formatos_determinaciones['DEFAULT']['TabTexto'],
            'TabResultado' => $formatos_determinaciones['DEFAULT']['TabResultado'],
            'Interlineado' => $formatos_determinaciones['DEFAULT']['Interlineado'],
        ];

        // RESULTADOS
        foreach ($resultado['determinaciones'] as $determinacion) {

            if (array_key_exists($determinacion['id'], $formatos_determinaciones)) {
                $tags['Predeterminada'] = $formatos_determinaciones[$determinacion['id']]['Predeterminada'];
                $tags['Unidades'] = $formatos_determinaciones[$determinacion['id']]['Unidades'];
                $tags['Metodos'] = $formatos_determinaciones[$determinacion['id']]['Metodos'];
                $tags['Resultados'] = $formatos_determinaciones[$determinacion['id']]['Resultados'];
                $tags['Materiales'] = $formatos_determinaciones[$determinacion['id']]['Materiales'];
                $tags['Referencias'] = $formatos_determinaciones[$determinacion['id']]['Referencias'];
                $tags['Anteriores'] = $formatos_determinaciones[$determinacion['id']]['Anteriores'];
            }

            if ($determinacion['encabezado'] != '' && $determinacion['encabezado'] != '<br>' && $determinacion['encabezado'] != '<BR>') {
                $html = "<tr><td class=\"encabezado\"><h3>" . $determinacion['encabezado'] . "</h3></td></tr>";
                $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                //print_r(htmlspecialchars($html));
            }

            foreach ($determinacion['resultados'] as $resultado) {

                if ($resultado['TIPO'] == "RV") {
                    if ($resultado['NUEVA_LINEA'] != "1") {
                        if (str_replace('&r', '', $resultado['TEXTO_RESULTADO']) != '') {
                            $html = "";
                            $html .= "<tr>";
                            $html .= "<td width=\"400\" class=\"texto-resultado\">";
                            $html .= "<" . $tags['Predeterminada'] . ">";
                            $texto_resultado = str_replace('&r', '', $resultado['TEXTO_RESULTADO']);
                            $html .= str_replace("Resultado :", "<br>&nbsp;&nbsp;&nbsp;&nbsp;Resultado :", $texto_resultado) . " ";
                            $html .= "</" . $tags['Predeterminada'] . ">";
                            $html .= "</td>";
                            $html .= "<td align=\"left\" >";
                            $html .= "<" . $tags['Resultados'] . ">";
                            $html .= str_replace('&lt;BR&gt;', '<BR>', htmlspecialchars($resultado['RESULTADO'])) . " ";
                            $html .= "</" . $tags['Resultados'] . ">";

                        } else {
                            $html .= " - <" . $tags['Resultados'] . ">";
                            $html .= str_replace('&lt;BR&gt;', '<BR>', htmlspecialchars($resultado['RESULTADO'])) . " ";
                            $html .= "</" . $tags['Resultados'] . ">";
                            $html .= "<" . $tags['Unidades'] . ">";
                            $html .= $resultado['UNIDADES'];
                            $html .= "</" . $tags['Unidades'] . ">";


                            if ($resultado['METODO'] != '' || $resultado['VALOR_REFERENCIA'] != '' || $resultado['MATERIAL'] != '') {
                                $pdf->SetFont($fuente['pdf-family'], '', 12, '', true);

                                $html .= "<tr>";
                                $html .= "<td width=\"20\">&nbsp;</td>";
                                $html .= "<td width=\"400\" style=\"font-weight: 200\">";

                                if ($resultado['METODO'] != '' || !empty($resultado['METODO'])) {
                                    $html .= "<" . $tags['Metodos'] . ">";
                                    $html .= "&emsp;Método: " . str_replace('&lt;BR&gt;', '<BR>', htmlspecialchars($resultado['METODO']));
                                    $html .= "</" . $tags['Metodos'] . ">";
                                    $br = "<br>";
                                }
                                if ($resultado['VALOR_REFERENCIA'] != '' || !empty($resultado['VALOR_REFERENCIA'])) {
                                    //$html .= $br;
                                    $html .= "<" . $tags['Referencias'] . ">";
                                    $html .= "&emsp;Valor de referencia: " . str_replace('&lt;BR&gt;', '<BR>', htmlspecialchars($resultado['VALOR_REFERENCIA']));
                                    $html .= "</" . $tags['Referencias'] . ">";
                                    $br = "<br>";
                                }
                                if ($resultado['MATERIAL'] != '' || !empty($resultado['MATERIAL'])) {
                                    //$html .= $br;
                                    $html .= "<" . $tags['Materiales'] . ">";
                                    $html .= "&emsp;Material: " . str_replace('&lt;BR&gt;', '<BR>', htmlspecialchars($resultado['MATERIAL']));
                                    $html .= "</" . $tags['Materiales'] . ">";
                                }

                                $html .= "</td>";
                                $html .= "</tr>";

                            }


                        }

                        $html .= "</td>";
                        $html .= "</tr>";

                        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                    } else {

                        $html = "";

                        if (str_replace('&r', '', $resultado['TEXTO_RESULTADO']) != '') {
                            $html .= "<tr>";
                            $html .= "<td class=\"texto-resultado\" width=\"400\" >";
                            $html .= "<" . $tags['Predeterminada'] . ">";
                            $texto_resultado = str_replace('&r', '', $resultado['TEXTO_RESULTADO']);
                            $html .= str_replace("Resultado :", "<br>&nbsp;&nbsp;&nbsp;&nbsp;Resultado :", $texto_resultado) . " ";
                            $html .= "</" . $tags['Predeterminada'] . ">";
                            $html .= "</td>";
                            $html .= "<td align=\"left\" >";
                            $html .= "<" . $tags['Resultados'] . ">";
                            $html .= str_replace('&lt;BR&gt;', '<BR>', htmlspecialchars($resultado['RESULTADO'])) . " ";
                            $html .= "</" . $tags['Resultados'] . ">";
                            $html .= "<" . $tags['Unidades'] . ">";
                            $html .= $resultado['UNIDADES'];
                            $html .= "</" . $tags['Unidades'] . ">";
                            $html .= "</td>";
                            $html .= "</tr>";

                            if ($resultado['METODO'] != '' || $resultado['VALOR_REFERENCIA'] != '' || $resultado['MATERIAL'] != '') {

                                $html .= "<tr>";
                                $html .= "<td width=\"10\">&nbsp;</td>";
                                $html .= "<td width=\"400\" style=\"font-weight: 200\">";

                                $html .= "<br>";
                                if ($resultado['METODO'] != '' || !empty($resultado['METODO'])) {
                                    $html .= "<" . $tags['Metodos'] . ">";
                                    $html .= "&emsp;Método: " . str_replace('&lt;BR&gt;', '<BR>', htmlspecialchars($resultado['METODO']));
                                    $html .= "</" . $tags['Metodos'] . ">";
                                    $br = "<br>";
                                }
                                if ($resultado['VALOR_REFERENCIA'] != '' || !empty($resultado['VALOR_REFERENCIA'])) {
                                    $html .= $br;
                                    $html .= "<" . $tags['Referencias'] . ">";
                                    $html .= "&emsp;Valor de referencia: " . str_replace('&lt;BR&gt;', '<BR>', htmlspecialchars($resultado['VALOR_REFERENCIA']));
                                    $html .= "</" . $tags['Referencias'] . ">";
                                    $br = "<br>";
                                }
                                if ($resultado['MATERIAL'] != '' || !empty($resultado['MATERIAL'])) {
                                    $html .= $br;
                                    $html .= "<" . $tags['Materiales'] . ">";
                                    $html .= "&emsp;Material: " . str_replace('&lt;BR&gt;', '<BR>', htmlspecialchars($resultado['MATERIAL']));
                                    $html .= "</" . $tags['Materiales'] . ">";
                                }

                                $html .= "</td>";
                                $html .= "</tr>";

                            }

                            $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                        }

                    }
                    /*print_r(htmlspecialchars($html));
                    echo "<hr>";*/
                }

                if ($resultado['TIPO'] == "I") {
                    foreach ($resultado['RESULTADOS'] as $res) {
                        $html = "";
                        if (str_replace('&r', '', $res['TEXTO_RESULTADO']) != '') {
                            $html .= "<tr>";
                            $html .= "<td class=\"texto-resultado\" width=\"400\">";
                            $html .= "<" . $tags['Predeterminada'] . ">";
                            $texto_resultado = str_replace('&r', '', $res['TEXTO_RESULTADO']);
                            $html .= str_replace("Resultado :", "<br>&nbsp;&nbsp;&nbsp;&nbsp;Resultado :", $texto_resultado) . " ";
                            $html .= "</" . $tags['Predeterminada'] . ">";
                            $html .= "</td>";
                            $html .= "<td align=\"left\">";
                            $html .= "<" . $tags['Resultados'] . ">";
                            $html .= $res['RESULTADO'] . " ";
                            $html .= "</" . $tags['Resultados'] . ">";
                            $html .= "<" . $tags['Unidades'] . ">";
                            $html .= $res['UNIDADES'];
                            $html .= "</" . $tags['Unidades'] . ">";
                            $html .= "</td>";
                            $html .= "</tr>";

                            if ($res['METODO'] != '' || $res['VALOR_REFERENCIA'] != '' || $res['MATERIAL'] != '') {

                                $html .= "<tr>";
                                $html .= "<td width=\"20\">&nbsp;</td>";
                                $html .= "<td width=\"400\" style=\"font-weight: 200\">";

                                $html .= "<br>";
                                if ($res['METODO'] != '' || !empty($resultado['METODO'])) {
                                    $html .= "<" . $tags['Metodos'] . ">";
                                    $html .= "&emsp;Método: " . str_replace('&lt;BR&gt;', '<BR>', htmlspecialchars($res['METODO']));
                                    $html .= "</" . $tags['Metodos'] . ">";
                                    $br = "<br>";
                                }
                                if ($res['VALOR_REFERENCIA'] != '' || !empty($resultado['VALOR_REFERENCIA'])) {
                                    $html .= $br;
                                    $html .= "<" . $tags['Referencias'] . ">";
                                    $html .= "&emsp;Valor de referencia: " . str_replace('&lt;BR&gt;', '<BR>', htmlspecialchars($res['VALOR_REFERENCIA']));
                                    $html .= "</" . $tags['Referencias'] . ">";
                                    $br = "<br>";
                                }
                                if ($res['MATERIAL'] != '' || !empty($resultado['MATERIAL'])) {
                                    $html .= $br;
                                    $html .= "<" . $tags['Meteriales'] . ">";
                                    $html .= "&emsp;Material: " . str_replace('&lt;BR&gt;', '<BR>', htmlspecialchars($res['MATERIAL']));
                                    $html .= "</" . $tags['Materiales'] . ">";
                                }

                                $html .= "</td>";
                                $html .= "</tr>";

                            }
                            $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                        }

                    }
                }

            }

        }

        $html = "</tbody></table>";

        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        $html = "<table><tbody>";
        $html .= "<tr>";
        $html .= "<td align=\"right\">";
        $html .= "<p>" . $pie . "<br>";
        $html .= "</td>";
        $html .= "</tr>";
        $html .= "</tbody></table>";

        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        $html = '</td>';
        $html = '</tr>';
        $html = '</tbody>';
        $html = '</table>';

        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        // ---------------------------------------------------------

        // Fuerza a descargar el PDF
        $pdf->Output($idderivacion . '-' . $pdf->_datos['Paciente'] . '-' . $pdf->_datos['Fecha'] . '.pdf', 'D');


    }

    /* Resultados por fechas */
    public function resultados_por_fechas()
    {
        $dni = isset($_GET['documento']) ? $_GET['documento'] : '';
        if(strlen($dni) > 4) {
            $this->_view->assign('pacientes', $this->_rolModel->getPacientesByDni($dni));
        } else {
            $this->_view->assign('pacientes', []);
        }

        $this->_view->render('resultados_por_fechas', 'rol');
    }

    public function ver_determinaciones_por_fechas($dni = false)
    {
        if(!$dni)
            $this->redirect('/rol/index/resultados_por_fechas');

        //$pacientes = $this->_rolModel->getPacientesByDni($dni);
        //$paciente = @array_pop($pacientes);
        $this->_view->assign('paciente', @array_pop($this->_rolModel->getPacientesByDni($dni)));
        $this->_view->assign('practicas', $this->_rolModel->getPracticasByDni($dni));

        $this->_view->render('resultados_de_paciente', 'rol');
    }

    public function jsonResults(){
        $this->_acl->access('rol');
        $data = json_decode(file_get_contents("php://input"));
        $arrayReturn = $this->_rolModel->getDerivaciones(Session::get('role'), Session::get('iduser'), $data->from, $data->to);
        if ($arrayReturn != false) {
            echo json_encode($arrayReturn);
        } else {
            echo false;
        }
    }

    public function parseFecha($fecha){
        return substr($fecha, 6) . "/" . substr($fecha, 4, 2) . "/" . substr($fecha, 0, 4);
    }

}

?>
