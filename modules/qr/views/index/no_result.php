<div id="login-form-container" class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-ms-6 col-ms-offset-3 col-xs-10 col-xs-offset-1 text-center">
    <div id="logo" class="col-lg-12">
        <img class="col-lg-12 col-md-12 col-ms-12 col-xs-12" src="<?php echo PRIMARY_URL; ?>public/img/labwin-logo.png" />
    </div>
    <div id="login-form" class="text-left col-lg-8 col-lg-offset-2">

        <div id="alert-error" class="alert-danger">
            <p>Aún no se ha cargado el resultado, inténtelo nuevamente más tarde.</p>
        </div>

    </div>
</div>
