var app=angular.module('ui.bootstrap', ['ngAnimate', 'ui.bootstrap']);

app.controller('MOL_INDEX', ['$scope', '$http', '$q', '$window', function ($scope, $http, $q, $window) {

	$scope.eliminarMuestra=function(id, nroTubo){
		var deferred = $q.defer();
		
		if($window.confirm("¿Está seguro que desea eliminar la muestra?")){
			var conAjax = $http.post(__root__ + 'mol/mis_derivaciones/eliminarMuestra', {idMuestra: id, tubo: nroTubo});
			conAjax.success(function(respuesta){
		    	
		       document.location.href = __root__ + 'mol';
		        
		   });
		}
		
		return deferred.promise;
	}

}]);