<div ng-controller="MOL_INDEX" class="col-md-12">

<div class="col-lg-10 col-lg-offset-1 col-xs-12">
  <div class="col-lg-12 col-xs-12">
    <div class="col-lg-4 col-xs-12 text-left">
      <img class="col-lg-12 col-xs-12" src="/public/img/labwin-logo.png" />
    </div>
    <div class="col-lg-8 col-xs-12 text-right">
      <h2 class="title light-blue-color"><?php echo $this->_customVars['titulo']; ?></h2>
    </div>
  </div>
</div>

<div id="resultado-blue-strip" class="col-lg-12 col-xs-12">
  <div class="col-lg-10 col-lg-offset-1 col-xs-12">
    <div class="col-lg-12 col-xs-12">
      <div class="col-lg-6 text-left col-xs-4 no-padding">
        Muestras sin confirmar:
        <?php
        	 	$cantidadMuestras = $this->_customVars['muestras'];
          	if ($this->_customVars['muestras']) {
            	$cantidadMuestras = count($this->_customVars['muestras']);
          	} else {
          		$cantidadMuestras = 0;
          	}
          	echo $cantidadMuestras;
        ?>
      </div>
      <div class="col-lg-6 text-right col-xs-8">
      	<?php
				if($cantidadMuestras > 0) {
      	?>
      		<a class="custom-form-control" href="<?php echo $this->_layoutParams['base_url'] ?>mol/mis_derivaciones/confirmarMuestras">Confirmar muestras</a>
			<?php
				}
      	?>
        	<a class="custom-form-control" href="<?php echo $this->_layoutParams['base_url'] ?>mol/mis_derivaciones">Mis derivaciones <i class="fa fa-caret-right" aria-hidden="true"></i></a>
        	<a class="custom-form-control" href="<?php echo $this->_layoutParams['base_url'] ?>mol/mis_derivaciones/nueva_muestra">Nueva Muestra <i class="fa fa-plus" aria-hidden="true"></i></a>
      </div>
    </div>
  </div>
</div>


<div class="col-lg-10 col-lg-offset-1 col-xs-12">
  <div class="col-lg-12 col-xs-12 row">
  <?php	if($this->_customVars['muestras'] && count($this->_customVars['muestras']) > 0){ ?>

    <div class="col-lg-12 col-xs-12 table-header word-break hidden-xs">
      <div class="col-lg-2 col-xs-2"># DE TUBO</div>
      <div class="col-lg-3 col-xs-3">APELLIDO Y NOMBRE</div>
      <div class="col-lg-3 col-xs-3">DETERMINACIONES</div>
      <div class="col-lg-4 col-xs-4">OBSERVACIONES</div>
    </div>
    <?php foreach ($this->_customVars['muestras'] as $muestra) { ?>

      <div class="col-lg-12 col-xs-12 no-padding word-break hidden-xs hidden-ls">
        	<div class="col-lg-2 col-xs-2 custom-form-control"><?php echo $muestra['Tubos_Fld'] ?></div>
        	<div class="col-lg-3 col-xs-3 custom-form-control"><?php echo $muestra['Nombre_Fld'] ?></div>
        	<div class="col-lg-3 col-xs-3 custom-form-control"><?php echo $muestra['determinaciones'] ?></div>
        	<div class="col-lg-4 col-xs-4 custom-form-control">
        		<?php echo $muestra['Observ_Fld'] ?>
				<div class="pull-right">
					<a	href="<?php echo BASE_URL . 'mol/mis_derivaciones/editar_muestra/' . $muestra['Numero_Fld']; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
					<a ng-click="eliminarMuestra(<?php echo $muestra['Numero_Fld'] . ',' . $muestra['Tubos_Fld']; ?>)"><i class="fa fa-times" aria-hidden="true"></i></a>
				</div>
			</div>
      </div>

      <div class="col-lg-12 col-xs-12 no-padding word-break hidden-md hidden-lg">
        	<div class="col-xs-12 custom-form-control no-padding">
        		<div class="col-xs-6">
        			<?php echo $muestra['Tubos_Fld'] ?> <br>
        			<?php echo $muestra['Nombre_Fld'] ?>
        		</div>
        		<div class="col-xs-5 no-padding">
        			<?php echo $muestra['determinaciones'] ?> <br>
        			<?php echo $muestra['Observ_Fld'] ?>
        		</div>
        		<div class="col-xs-1 no-padding">
					<div class="pull-right">
						<a	href="<?php echo BASE_URL . 'mol/mis_derivaciones/editar_muestra/' . $muestra['Numero_Fld']; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
						<a ng-click="eliminarMuestra(<?php echo $muestra['Numero_Fld'] . ',' . $muestra['Tubos_Fld']; ?>)"><i class="fa fa-times" aria-hidden="true"></i></a>
					</div>
        		</div>
        	</div>
      </div>

    <?php } ?>

  <?php	} else {
  		echo "No hay muestras sin confirmar";
  	}
  ?>
  </div>
</div>
</div>
