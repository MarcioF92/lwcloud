<div id="mol-options" ng-controller="NUEVA_MUESTRA" class="col-md-10 col-md-offset-1">

	<form>
		<input type="hidden"
			name="oldNroTubo"
			ng-model="oldNroTubo"
			<?php
				if(isset($this->_customVars['datos']['Tubos_Fld'])){
			?>
				ng-init="oldNroTubo=<?php echo $this->_customVars['datos']['Tubos_Fld']; ?>"
				value="<?php echo $this->_customVars['datos']['Tubos_Fld']; ?>"
			<?php
				} else {
			?>
				ng-init="oldNroTubo=null"
				value="null"
			<?php
				}
			?>
		>
		<input type="hidden"
			name="Numero_Fld"
			ng-model="Numero_Fld"
			<?php
				if(isset($this->_customVars['datos']['Numero_Fld'])){
			?>
				ng-init="Numero_Fld='<?php echo $this->_customVars['datos']['Numero_Fld']; ?>'"
				value="<?php echo $this->_customVars['datos']['Numero_Fld']; ?>"
			<?php
				} else {
			?>
				ng-init="Numero_Fld=null"
				value="null"
			<?php
				}
			?>
		>
        <input type="hidden"
               name="First_NumOrig_Fld"
               ng-model="First_NumOrig_Fld"
            <?php
            if(isset($this->_customVars['datos']['ImportNumOrig_Fld'])){
                $importNumOrig = explode('-', $this->_customVars['datos']['ImportNumOrig_Fld'])[1];
                ?>
                ng-init="First_NumOrig_Fld=<?php echo $importNumOrig; ?>"
                value="<?php echo $importNumOrig; ?>"
                <?php
            }
            ?>
        >

		<div class="col-md-6">
			<h3 class="title light-blue-color">Identificación de muestra</h3>

			<div class="alert alert-danger" ng-style="errorStyle">
				<p>{{errorLabel}}</p>
			</div>

			<div class="col-md-12 mol-option">
				<div class="col-md-6">
					<label class="mol-label" for="Nombre_Fld">Muestra </label>
				</div>
				<div class="col-md-6">
					<input
						class="custom-form-control col-md-12"
						type="text"
						name="Nombre_Fld"
						ng-model="Nombre_Fld"
						<?php
							if(isset($this->_customVars['datos']['Nombre_Fld'])){
						?>
							ng-init="Nombre_Fld='<?php echo $this->_customVars['datos']['Nombre_Fld']; ?>'"
							value="<?php echo $this->_customVars['datos']['Nombre_Fld']; ?>"
						<?php
							}
						?>
					>
				</div>
			</div>

			<div class="col-md-12 mol-option">
				<div class="col-md-6">
					<label class="mol-label" for="Sexo_Fld">Sexo </label>
				</div>
				<div class="col-md-6">
					<select class="custom-form-control col-md-12"
						name="Sexo_Fld"
						<?php
							if(isset($this->_customVars['datos']['Sexo_Fld'])){
								if($this->_customVars['datos']['Sexo_Fld'] == 8)
									$this->_customVars['datos']['Sexo_Fld'] = 0;
						?>
							ng-init="Sexo_Fld=<?php echo "dataSexo[" . $this->_customVars['datos']['Sexo_Fld'] . "].id"; ?>"
						<?php
							} else {
						?>
							 ng-init="Sexo_Fld = dataSexo[0].id"
						<?php
							}
						?>
						ng-model="Sexo_Fld"
						ng-options="option.id as option.name for option in dataSexo">
					</select>
				</div>
			</div>

			<div class="col-md-12 mol-option">
				<div class="col-md-6">
				<label class="mol-label" for="HClin_Fld">DNI </label></div>
				<div class="col-md-6">
					<input
						class="custom-form-control col-md-12"
						type="text"
						name="HClin_Fld"
						ng-model="HClin_Fld"
						<?php
							if(isset($this->_customVars['datos']['HClin_Fld'])){
						?>
							ng-init="HClin_Fld='<?php echo $this->_customVars['datos']['HClin_Fld']; ?>'"
							value="<?php echo $this->_customVars['datos']['HClin_Fld']; ?>"
						<?php
							}
						?>
					>
				</div>
			</div>

			<div class="col-md-12 mol-option">
				<div class="col-md-6">
					<label class="mol-label" for="NumOrig_Fld">ID Original </label>
				</div>
				<div class="col-md-6">
                    <input
                            class="custom-form-control col-md-12"
                            type="text"
                            name="NumOrig_Fld"
                            ng-model="NumOrig_Fld"
                        <?php
                        if(isset($this->_customVars['datos']['ImportNumOrig_Fld'])){
                            $importNumOrig = explode('-', $this->_customVars['datos']['ImportNumOrig_Fld'])[1];
                            ?>
                            ng-init="NumOrig_Fld=<?php echo $importNumOrig; ?>"
                            value="<?php echo $importNumOrig; ?>"
                            <?php
                        }
                        ?>
                    >
				</div>
			</div>

			<div class="col-md-12 mol-option">
				<div class="col-md-6">
					<label class="mol-label" for="FNacim_Fld">Fecha de nacimiento </label>
				</div>
				<div class="col-md-6">
					<small><i>Formato 99/99/9999</i></small>
					<input
						class="custom-form-control col-md-12"
						type="text"
						name="FNacim_Fld"
						ng-model="FNacim_Fld"
						mask="99/99/9999"
						clean="true"
						<?php
							if(isset($this->_customVars['datos']['FNacim_Fld'])){
						?>
							ng-init="FNacim_Fld='<?php echo substr($this->_customVars['datos']['FNacim_Fld'], 6, 2) . substr($this->_customVars['datos']['FNacim_Fld'], 4, 2) . substr($this->_customVars['datos']['FNacim_Fld'], 0, 4); ?>'"
							value="<?php echo substr($this->_customVars['datos']['FNacim_Fld'], 6, 2) . substr($this->_customVars['datos']['FNacim_Fld'], 4, 2) . substr($this->_customVars['datos']['FNacim_Fld'], 0, 4); ?>"
						<?php
							}
						?>
					>
				</div>
			</div>

			<div class="col-md-12 mol-option">
				<div class="col-md-6">
					<label class="mol-label" for="Tubos_Fld">Número de tubo </label>
				</div>
				<div class="col-md-6">
					<input
						class="custom-form-control col-md-12"
						type="text"
						name="Tubos_Fld"
						ng-model="Tubos_Fld"
						<?php
							if(isset($this->_customVars['datos']['Tubos_Fld'])){
						?>
							ng-init="Tubos_Fld='<?php echo $this->_customVars['datos']['Tubos_Fld']; ?>'"
							value="<?php echo $this->_customVars['datos']['Tubos_Fld']; ?>"
						<?php
							}
						?>
					>
				</div>
			</div>

			<div class="col-md-12 mol-option">
				<div class="col-md-6">
					<label class="mol-label" for="Observ_Fld">Observaciones </label>
				</div>
				<div class="col-md-6">
					<textarea
						class="custom-form-control col-md-12"
						name="Observ_Fld"
						ng-model="Observ_Fld"
						<?php
							if(isset($this->_customVars['datos']['Observ_Fld'])){
						?>
							ng-init="Observ_Fld='<?php echo $this->_customVars['datos']['Observ_Fld']; ?>'"
						<?php
							}
						?>
					>
						<?php if(isset($this->_customVars['datos']['Observ_Fld']))echo $this->_customVars['datos']['Observ_Fld']; ?>
					</textarea>
				</div>
			</div>

            <div class="col-md-12 mol-option">
                <div class="col-md-6">
                    <label class="mol-label" for="Observ_Fld">Determinaciones </label>
                    <p><small>Distingue entre mayúsculas y minúsculas</small></p>
                </div>
                <div class="col-md-6">
                    <div class="input-group mol-option">
                        <p><input type="text" ng-model="codigo" class="custom-form-control col-md-12" my-enter="ajaxUpdate()">
                            <br>
                            <a href="#" ng-click="ajaxUpdate()">Buscar</a></p>
                    </div>
                    <div id="determinaciones-result" ng-style="determinacionesResult" ng-init="determinaciones = []">
                        <ul ng-repeat="determinacion in determinaciones">
                            <li material="{{determinacion.Material_Fld}}">{{determinacion.Abrev_Fld}} - {{determinacion.Nombre_Fld}}. Material:  {{determinacion.nombre_material}} <span class="push-right"><a href="#" ng-click="addDeterminacion(determinacion)">Agregar</a></span></li>
                        </ul>
                    </div>
                </div>
            </div>

		</div>

		<div class="col-md-6">
			<div class="col-md-12 mol-option">
                <p>Abajo lista de sus prácticas más frecuentes</p>
                <small>Por cuestiones de mantenimiento no podemos listar sus prácticas más frecuentes, por favor, utilice el buscador</small>
                <div id="determinaciones-comunes" ng-style="determinacionesComunesStyle" ng-init="determinacionesComunes = []">
                    <ul ng-repeat="determinacion in determinacionesComunes">
                        <li material="{{determinacion.Material_Fld}}">{{determinacion.Abrev_Fld}} - {{determinacion.Nombre_Fld}} <span class="push-right"><a href="#" ng-click="ajaxUpdateComunes(determinacion.Abrev_Fld)">Agregar</a></span></li>
                    </ul>
                </div>

				<div
					<?php
						if(isset($this->_customVars['datos']['determinaciones'])){
					?>
						ng-init="determinacionesFinal = <?php echo htmlspecialchars(json_encode($this->_customVars['datos']['determinaciones'])); ?>"
					<?php
						} else {
					?>
						ng-init="determinacionesFinal = []"
					<?php
						}
					?>
					id="determinaciones-list" class="col-md-12">
					<ul ng-repeat="determinacionFinal in determinacionesFinal track by $index">
						<li material="{{determinacionFinal.Material_Fld}}">{{determinacionFinal.Nombre_Fld}}. Material:  {{determinacionFinal.Material_Fld}}<span class="push-right"><a href="#" ng-click="removeDeterminacion($index)">X</a></span></li>
					</ul>
				</div>
				<div class="col-md-12 mol-option">
					<input type="checkbox"
						name="datosClientes"
						ng-model="datosClientes"
						ng-true-value="true"
						ng-false-value="false"
						value="1"> Utilizar los datos del paciente actual
					<br>
					<button class="btn btn-info" ng-click="guardar(true)">Guardar y nuevo</button><br>
					<button class="btn btn-info" ng-click="guardar(false)">Guardar y salir</button><br>
					<a class="btn btn-info" href="<?php echo BASE_URL . 'mol'; ?>">Cancelar</a>
				</div>
			</div>
		</div>
	</form>
</div>

<style>
    #determinaciones-load{
        display: none;
    }
</style>