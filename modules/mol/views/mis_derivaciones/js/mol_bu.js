var app=angular.module('ui.bootstrap', ['ngAnimate', 'ui.bootstrap', 'ngMask']);

app.controller('NUEVA_MUESTRA', ['$scope', '$filter', '$http', '$q', '$log', '$window', function ($scope, $filter, $http, $q, $log, $window) {

	$scope.ajaxUpdate=function(searchType){

        $scope.determinacionesResult = {'display': 'none'};
        $scope.loadingDeters = {'display': 'block'};
        $scope.determinacionesComunesStyle = {'display': 'none'};

		var deferred = $q.defer();
		if($scope.codigo == '' || $scope.codigo == undefined){
			$scope.determinaciones.length = 0;
		} else {
			$scope.determinaciones.length = 0;
		    var conAjax = $http.post(__root__ + 'mol/mis_derivaciones/getDeterminacionesByAjax', {codigo: $scope.codigo});
		    conAjax.success(function(respuesta){
		        if (respuesta.length > 0) {
                    if (respuesta.length == 1) {
                        for (var i = respuesta.length - 1; i >= 0; i--) {
                            var agregar = true;
							angular.forEach($scope.determinacionesFinal, function(value, key){
                                $log.log(value.Material_Fld + " - " + respuesta[i].Material_Fld);
                                if (value.Material_Fld != respuesta[i].Material_Fld){
                                    agregar = false;
                                }
                            });
                            if(agregar){
                                $scope.determinacionesFinal.push(respuesta[i]);
                            } else {
                                $window.alert('No puede agregar la práctica '+respuesta[i].Abrev_Fld+' ya que tiene un material diferente a las de la lista');
                            }
                            $scope.determinacionesResult = {'display': 'none'};
                            $scope.loadingDeters = {'display': 'none'};
                            $scope.determinacionesComunesStyle = {'display': 'block'};
						}
					} else {
                        if(respuesta != 'false') {
                            for (var i = respuesta.length - 1; i >= 0; i--) {
                                $scope.determinaciones.push(respuesta[i]);
                                $scope.determinacionesResult = {'display': 'none'};
                                $scope.loadingDeters = {'display': 'none'};
                            }
                        } else {
                            $window.alert('No se han encontrado determinaciones. Pruebe con otra búsqueda');
						}
					}
		        } else {
		          	return false;
		        }
		    })
            .error(function(error, status){
                console.log(error);
                console.log(status);
            });
		}
		$scope.codigo = '';
		return deferred.promise;
    }

    $scope.determinacionesComunesList = function(){
        $log.log('Deters comunes');
        var conAjax = $http.post(__root__ + 'mol/mis_derivaciones/getDeterminacionesComunes');
        conAjax.success(function(respuesta){
            if (respuesta.length > 0) {
                for (var i = 0; i < respuesta.length; i++) {
                    $scope.determinacionesComunes.push(respuesta[i]);
                }
                $scope.loadingDeters = {'display' : 'none'};
                $scope.determinacionesComunesStyle = {'display' : 'block'};
            } else {
                return false;
            }
        })
        .error(function(error, status){
            console.log(error);
            console.log(status);
        });
	}

    $scope.removeDeterminacion=function(idx){
    	//$scope.determinacionesFinal.pop(determinacion);
        $scope.determinacionesFinal.splice(idx,1);
    }

    $scope.addDeterminacion=function(determinacion, comun){
        var agregar = true;
        angular.forEach($scope.determinacionesFinal, function(value, key){
            if (value.Material_Fld != determinacion.Material_Fld){
                agregar = false;
            }
        });

        if(agregar){
            $scope.determinacionesFinal.push(determinacion)
        } else {
            $window.alert('No puede agregar prácticas con diferentes materiales en la misma derivación');
        }

        $scope.determinacionesResult = {'display': 'none'};
        $scope.loadingDeters = {'display': 'none'};
        $scope.determinacionesComunesStyle = {'display': 'block'};
    }

    $scope.guardar=function(guardarYNuevo){
	    $scope.errorStyle = {'display' : 'none'};
    	var deferred = $q.defer();
	    var conAjax = $http.post(__root__ + 'mol/mis_derivaciones/guardar', {
	    	Nombre_Fld: $scope.Nombre_Fld,
	    	Sexo_Fld: $scope.Sexo_Fld,
	    	HClin_Fld: $scope.HClin_Fld,
	    	NumOrig_Fld: $scope.NumOrig_Fld,
            First_NumOrig_Fld: $scope.First_NumOrig_Fld,
	    	FNacim_Fld: $scope.FNacim_Fld,
	    	Tubos_Fld: $scope.Tubos_Fld,
	    	Observ_Fld: $scope.Observ_Fld,
	    	determinacionesFinal: $scope.determinacionesFinal,
	    	oldNroTubo: $scope.oldNroTubo,
	    	Numero_Fld: $scope.Numero_Fld,
	    	datosClientes: $scope.datosClientes,
	    	nuevo: guardarYNuevo
	    });
	    conAjax.success(function(respuesta){
				$log.log(respuesta.success);
				$log.log(respuesta['success']);

	    	if (respuesta.success) {
	    		$log.log(respuesta);
	    		document.location.href = __root__ + respuesta.redirect;
	    	} else {
	        	$log.log(respuesta);
	        	$scope.errorStyle = {'display' : 'block'};
	        	$scope.errorLabel = respuesta.error;
	        }

	    });
		return deferred.promise;
    }

    //Iniciar variables
    $scope.errorStyle = {'display' : 'none'};
    $scope.dataSexo = [{
        id: 0,
        name: 'OTROS'
    }, {
        id: 1,
        name: 'MASCULINO'
    }, {
        id: 2,
        name: 'FEMENINO'
    }];

    $scope.determinacionesComunesList();

}]);

app.directive('myEnter', function () {
		return function (scope, element, attrs) {
				element.bind("keydown keypress", function (event) {
						if(event.which === 13) {
								scope.$apply(function (){
										scope.$eval(attrs.myEnter);
								});
								event.preventDefault();
						}
				});
		};
});
