<div class="col-lg-10 col-lg-offset-1 col-xs-12">
  <div class="col-lg-12 col-xs-12">
    <div class="col-lg-4 col-xs-12 text-left">
      <img class="col-lg-12 col-xs-12" src="/public/img/labwin-logo.png" />
    </div>
    <div style="padding: 10px;" class="col-lg-8 text-right col-xs-12">
      <h2 class="title light-blue-color"><?php echo $this->_customVars['titulo']; ?></h2>
    </div>
  </div>
</div>

<div id="resultado-blue-strip" class="col-lg-12 text-right col-xs-12">
  <div class="col-lg-10 col-lg-offset-1 col-xs-12">
    <a class="custom-form-control" href="<?php echo $this->_layoutParams['base_url'] ?>mol">Inicio</a>
  </div>
</div>

<div class="col-lg-10 col-lg-offset-1 col-xs-12 no-padding">
  <div class="col-lg-12 row col-xs-12">
  <?php	if($this->_customVars['remitos'] && count($this->_customVars['remitos']) > 0){ ?>

    <div class="col-lg-12 col-xs-12 table-header text-center no-padding">
      <div class="col-lg-2 col-xs-4"># ENVÍO</div>
      <div class="col-lg-2 col-xs-4">FECHA</div>
      <div class="col-lg-8 col-xs-4">CANTIDAD DE TUBOS</div>
    </div>
    <?php foreach ($this->_customVars['remitos'] as $remito) { ?>

      <div class="col-lg-12 col-xs-12 table-row text-center no-padding">
        <div class="col-lg-2 col-xs-4 table-td custom-form-control"><?php echo $remito['tipo_envio'] . "-" . $remito['idu'] . "-" . $remito['id'] ?></div>
        <div class="col-lg-2 col-xs-4 table-td custom-form-control"><?php echo $remito['fecha'] ?></div>
        <div class="col-lg-8 col-xs-4 table-td custom-form-control">
        	<?php echo $remito['cantder'] ?>
        	<a class="pull-right" href="<?php echo BASE_URL . 'mol/mis_derivaciones/ver/' . $remito['id'];?>">
            <i class="fa fa-eye" aria-hidden="true"></i>
          </a>
        </div>
      </div>

    <?php } ?>

  <?php	} else {
  		echo "No hay remitos para mostrar";
  	}
  ?>
  </div>
</div>
