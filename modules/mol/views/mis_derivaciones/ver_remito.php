<div id="resultado-blue-strip" class="col-lg-12 text-right col-xs-12">
    <div class="col-lg-10 col-lg-offset-1 col-xs-12">
        <a class="custom-form-control" href="<?php echo $this->_layoutParams['base_url'] ?>mol/mis_derivaciones/ver_pdf/<?php echo $this->_customVars['remito']['remitos'][0]['id']; ?>">Ver en PDF</a>
        <a class="custom-form-control" href="<?php echo $this->_layoutParams['base_url'] ?>mol/mis_derivaciones">Volver</a>
    </div>
</div>

<?php if(is_array($this->_customVars['remito'])){ ?>

    <div class="col-lg-10 col-lg-offset-1 col-xs-12">
        <h3>Se ha confirmado el envío de <?php echo count($this->_customVars['remito']['remitos']); ?> muestras</h3>
        <h3>El número de envío es: <strong><?php echo $this->_customVars['remito']['remitos'][0]['tipo_envio'] . "-" . $this->_customVars['remito']['remitos'][0]['idu'] . "-" . $this->_customVars['remito']['remitos'][0]['id']; ?></strong></h3>
        <p><strong>Colega: <?php echo $this->_customVars['remito']['remitos'][0]['idu'] . " - " . $this->_customVars['remito']['remitos'][0]['nombre_colega']; ?></strong></p>
        <p><strong>Fecha: <?php echo $this->_customVars['remito']['remitos'][0]['fecha']; ?></strong></p>
    </div>

    <div class="col-lg-10 col-lg-offset-1 col-xs-12 no-padding">
        <?php foreach ($this->_customVars['remito']['remitos'] as $r) {?>
            <div class="col-lg-12 col-xs-12 custom-form-control">
                <div class="col-lg-4 col-xs-4">
                    <p>Cod. de Barra</p>
                </div>
                <div class="col-lg-2 col-xs-2">
                    <p>Nro. Tubo</p>
                </div>
                <div class="col-lg-3 col-xs-3">
                    <p>Apellido y Nombre</p>
                </div>
                <div style="word-break:break-all" class="col-lg-3 col-xs-3">
                    <p>Determinaciones</p>
                </div>

                <div class="col-lg-4 col-xs-4 text-center no-padding">
                    <?php
                    $nroTubo = str_pad($r['nro_tubo'], $digitos, '0', STR_PAD_LEFT);
                    $codigo = '98-' . $r['idu'] . '-' . $nroTubo;
                    if($this->_customVars['configs']['eliminar_guiones_codigo_barra'])
                        $codigo = str_replace('-', '', $codigo);
                    ?>
                    <img class="col-lg-12 col-xs-12 no-padding" alt="<?php echo $codigo; ?>" src="<?php echo PRIMARY_URL . 'libs/barcode.php?text=' . $codigo; ?>" /><br>
                    <?php echo $codigo; ?>
                </div>
                <div class="col-lg-2 col-xs-2">
                    <p>
                        <?php
                        $nroTubo = (int) str_pad($r['nro_tubo'], $digitos, '0', STR_PAD_LEFT);
                        if($nroTubo < 0)
                            $nroTubo = $nroTubo * -1;
                        echo $nroTubo;
                        ?>
                    </p>
                </div>
                <div class="col-lg-3 col-xs-3">
                    <p><?php echo $r['nombre_paciente']; ?></p>
                </div>
                <div class="col-lg-3 col-xs-3">
                    <p><?php echo $r['determinaciones']; ?></p>
                </div>
                <div class="col-lg-12">
                    <p>Observaciones: <?php echo $r['Observ_Fld']; ?></p>
                </div>
            </div>

        <?php } ?>

    </div>

<?php } else { ?>
    <div class="col-lg-12 col-xs-12">
        <div class="col-lg-4 col-lg-offset-4 col-xs-12 alert alert-danger">
            No existe remito con ese código para el usuario en sesión
        </div>
    </div>
<?php } ?>
