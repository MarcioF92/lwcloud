<?php

/*
* Index Controller of MOL
* /public_html/laboratorios/mobile/modules/mol/controllers/indexController.php
*/

class indexController extends Controller
{
    private $_molModel;

    public function __construct(){
        parent::__construct();
        
        $this->_molModel = $this->loadModel('mol');
        $this->_view->setTemplate('rol_mol_template');
        $this->_view->assign('body-classes', array('rol-mol-background'));
        
    }

    public function index(){
        $this->_acl->access('mol');
        $this->_view->setJs(array('molIndex'));
        $this->_view->assign('titulo','Muestras Online');
        $this->_view->assign('muestras', $this->_molModel->molList(Session::get('iduser')));
        $this->_view->render('index', 'mol');
    }

}

?>