<?php

/*
* Mis Derivaciones Controller of MOL
* /public_html/laboratorios/mobile/modules/mol/controllers/mis_derivacionesController.php
*/

class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        /*$image_file = 'http://sacmilani.lwcloud.com.ar/mobile/public/img/labwin-logo.png';
        $this->Image($image_file, 10, 10, 50, '', 'png', '', 'T', false, 300, '', false, false, 0, false, false, false);*/
        // Set font
        $this->SetFont('helvetica', 'B', 20);
        // Title
        //$this->Cell(0, 15, 'Resultados', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Página '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

class mis_derivacionesController extends Controller
{
    private $_molModel;

    public function __construct(){
        parent::__construct();

        $this->_molModel = $this->loadModel('mol');
        $this->_view->setTemplate('rol_mol_template');
        $this->_view->assign('body-classes', array('rol-mol-background'));

    }

    public function index(){
        $this->_acl->access('mol');
        $this->_view->assign('titulo','Mis Derivaciones');
        $this->_view->assign('remitos', $this->_molModel->getRemitos(Session::get('iduser')));
        $this->_view->render('index', 'mol');
    }

    public function ver($idremito = false){
        if (!$idremito) {
            $this->redirect('mol/mis_derivaciones');
        }
        $this->_acl->access('mol');
        $this->_view->assign('titulo','Ver Remito');
        $this->_view->assign('remito', $this->_molModel->getRemito(Session::get('iduser'), $idremito));
        $this->_view->assign('configs', $this->_molModel->getConfigs());
        $this->_view->render('ver_remito', 'mol');
    }

    public function nueva_muestra(){
        $this->_acl->access('mol');
        $this->_view->setJs(array('mol'));
        $this->_view->assign('titulo','Nueva Muestra');
        if(Session::get('idem')) {
            Session::set('idem', false);
            $datos['Nombre_Fld'] = Session::get('Nombre_Fld');
            $datos['Sexo_Fld'] = Session::get('Sexo_Fld');
            $datos['HClin_Fld'] = Session::get('HClin_Fld');
            $datos['FNacim_Fld'] = Session::get('FNacim_Fld');
            $this->_view->assign('datos',$datos);
        }
        $this->_view->render('nueva_muestra', 'mol');
    }

    public function editar_muestra($id = false){
    		if (!$id) {
                $this->redirect('mol');
        	}
        	$this->_acl->access('mol');
        	$this->_view->setJs(array('mol'));
        	$this->_view->assign('titulo','Editar Muestra');
        	$datos = $this->_molModel->getForEdit($id);
        	$this->_view->assign('datos',$datos);
        	$this->_view->render('nueva_muestra', 'mol');
    }

    public function eliminarMuestra() {
			$this->_acl->access('mol');
			$data = json_decode(file_get_contents("php://input"));

			if(isset($data->idMuestra) && isset($data->tubo)) {
				$respuesta = $this->_molModel->eliminarMuestra($data->idMuestra, $data->tubo);
			} else {
				$respuesta['success'] = false;
				$respuesta['error'] = "Ocurrió un error al intentar eliminar la muestra";
			}

			echo json_encode($respuesta);
    }

    public function getDeterminacionesByAjax(){
        $this->_acl->access('mol');
        $data = json_decode(file_get_contents("php://input"));

        $determinaciones = $this->_molModel->getDeterminacionesByNomen($data->codigo);
        echo json_encode($determinaciones);
    }


    public function guardar(){
        $this->_acl->access('mol');
        $data = json_decode(file_get_contents("php://input"));

        $existeNroTubo = false;

        $numDeriv = Session::get('iduser');

        $nroTubo = ltrim($data->Tubos_Fld, "0");

        $configs = $this->_molModel->getConfigs();

        if(isset($configs['digitos_nro_tubo'])) {
            $digitos = $configs['digitos_nro_tubo'];
        } else {
            $digitos = 4;
        }

        //$nroTubo = str_pad($nroTubo, $digitos, '0', STR_PAD_LEFT);

        $respuesta['success'] = false;
        $respuesta['redirect'] = 'mol';

        if(count($data->determinacionesFinal) <= 0) {
            $respuesta['error'] = 'Ingrese al menos una determinación';
            echo json_encode($respuesta);
            exit;
        }

        if (!isset($nroTubo) || $nroTubo == '') {
            $respuesta['error'] = 'Ingrese número de tubo, por favor';
            echo json_encode($respuesta);
            exit;
        }

        //Get ID

        if(isset($data->NumOrig_Fld)) {
            if ($data->NumOrig_Fld == '') {
                if ($data->Numero_Fld != null) {
                    $data->NumOrig_Fld = $data->Numero_Fld;
                } else {
                    $data->NumOrig_Fld = $this->_molModel->getIdUnico();
                }
            } else {
                if (!strpos($data->NumOrig_Fld, '/')) {
                    if ($data->Numero_Fld != null) {
                        if ($data->NumOrig_Fld != $data->First_NumOrig_Fld) {
                            if ($this->_molModel->existeNumero($data->NumOrig_Fld)) {
                                $respuesta['error'] = 'Ya existe paciente con ese ID ';
                                echo json_encode($respuesta);
                                exit;
                            }
                        }
                    } else {
                        if ($data->NumOrig_Fld != $data->First_NumOrig_Fld) {
                            if ($this->_molModel->existeNumero($data->NumOrig_Fld)) {
                                $respuesta['error'] = 'Ya existe paciente con ese ID';
                                echo json_encode($respuesta);
                                exit;
                            }
                        }
                    }
                } else {
                    $respuesta['error'] = 'No se puede utilizar barra en el ID';
                    echo json_encode($respuesta);
                    exit;
                }
            }
        }

        if(isset($data->HClin_Fld)) {
            if (!is_numeric($data->HClin_Fld) && !empty($data->HClin_Fld)) {
                $respuesta['error'] = 'El documento debe ser numérico';
                echo json_encode($respuesta);
                exit;
            }
        }

        if(!is_numeric($data->Tubos_Fld)){
            $respuesta['error'] = 'El número de tubo debe ser numérico';
            echo json_encode($respuesta);
            exit;
        }

        $oldNroTubo = $data->oldNroTubo;

        if ($oldNroTubo != null) {
            if ($nroTubo != $oldNroTubo) {
                if($this->_molModel->existeNroTubo($nroTubo, $numDeriv, $data->Numero_Fld, $oldNroTubo)){
                    $respuesta['error'] = 'Ya existe número de tubo';
                    echo json_encode($respuesta);
                    exit;
                }
                $this->_molModel->reemplazarTubo($numDeriv, $oldNroTubo);
                $respuesta['error'] = 'Reemplazar Tubo';
            }
        } else {
            if($this->_molModel->existeNroTubo($nroTubo, $numDeriv, $data->Numero_Fld)){
                $respuesta['error'] = 'Ya existe número de tubo';
                echo json_encode($respuesta);
                exit;
            }
        }

        if (!isset($data->Nombre_Fld) || $data->Nombre_Fld == '') {
            $respuesta['error'] = 'Ingrese nombre, por favor';
            echo json_encode($respuesta);
            exit;
        }
        if (!$this->validarNombre($data->Nombre_Fld)) {
            $respuesta['error'] = 'Ingrese nombre con formato "APELLIDO,NOMBRE"';
            echo json_encode($respuesta);
            exit;
        }
        if (!isset($data->HClin_Fld) || $data->HClin_Fld == '') {
            $data->HClin_Fld = 0;
        }

        //Corregir Fecha de Nacimiento
        $fecha_de_nacimiento = substr($data->FNacim_Fld, 4, 4) . substr($data->FNacim_Fld, 2, 2) . substr($data->FNacim_Fld, 0, 2);

        if ($data->Numero_Fld != null) {
            $paciente = $this->_molModel->editPaciente($data->Numero_Fld, $data->Nombre_Fld, $data->Sexo_Fld, $data->HClin_Fld, $fecha_de_nacimiento, $nroTubo, isset($data->Observ_Fld) ? $data->Observ_Fld : '', isset($data->NumOrig_Fld) ? $data->NumOrig_Fld : '', $numDeriv);
        } else {
            $paciente = $this->_molModel->addPaciente($this->_molModel->getIdUnico(), $data->Nombre_Fld, $data->Sexo_Fld, $data->HClin_Fld, $fecha_de_nacimiento, $nroTubo, isset($data->Observ_Fld) ? $data->Observ_Fld : '',  isset($data->NumOrig_Fld) ? $data->NumOrig_Fld : '', $numDeriv);
        }

        if ($paciente) {
            if ($this->_molModel->addDeterminaciones($paciente, $nroTubo, $data->determinacionesFinal)) {
                $respuesta['success'] = true;
                if ($data->nuevo == 'true') {
                    $respuesta['guardarynuevo'] = true;
                    $respuesta['redirect'] .= '/mis_derivaciones/nueva_muestra';
                    if ($data->datosClientes == 'true') {
                        Session::set('idem', true);
                        Session::set('Nombre_Fld', $data->Nombre_Fld);
                        Session::set('Sexo_Fld', $data->Sexo_Fld);
                        Session::set('HClin_Fld', $data->HClin_Fld);
                        Session::set('FNacim_Fld', $fecha_de_nacimiento);
                    }
                }
            }
        }

        echo json_encode($respuesta);
    }

    public function validarNombre($nombre){
        return (strrpos($nombre, ',') && strrpos($nombre, ',') < strlen($nombre));
    }

	public function confirmarMuestras() {
		$this->_acl->access('mol');
		$id = Session::get('iduser');
        if($this->_molModel->confirmarMuestras($id)) {
			$this->redirect('mol');
		}
	}

	public function ver_pdf($idremito){
    $this->_acl->access('mol');

    $muestra = $this->_molModel->getRemito(Session::get('iduser'), $idremito);
    $config = $this->_molModel->getConfigs();

    $pdf = new MYPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator('LWCLOUD');
        $pdf->SetAuthor('LWCLOUD');
        $pdf->SetTitle('Informe de resultado generado por LWCloud');

        // set default header data
        //$pdf->SetHeaderData(, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
        $pdf->setFooterData(array(0,64,0), array(0,64,128));

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(True, PDF_MARGIN_FOOTER);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->setCellHeightRatio(2);

        // ---------------------------------------------------------


        $pdf->setFontSubsetting(true);

        $pdf->SetFont('dejavusans', '', 9, '', true);

        $pdf->AddPage();

        // Línea cabecera
        if($muestra['textos'][0]['imagen'] != '') {
            $logo_url = $this->_layoutParams['base_url'] . 'laboratorios/assets/' . array_shift((explode(".", $_SERVER['HTTP_HOST']))) . '/' . $muestra['textos'][0]['imagen'];
        } else {
            $logo_url = false;
        }

        // Título
        $html = "<table><tbody>";
        $html .= "<tr>";
        /*if($logo_url) {
            $html .= "<td width=\"200\">";
            $html .= '<img src="' . $logo_url . '">';
            $html .= "</td>";
        }*/
        $html .= "<td width=\"50\">";
        $html .= "</td>";
        $html .= "<td width=\"400\">";
        $html .= "<h1>".str_replace('\r\n','<br>',$muestra['textos'][0]['titulo'])."</h1>";
        $html .= "</td>";
        $html .= "</tr>";
        $html .= "<tr>";
        $html .= "<td width=\"200\">";
        $html .= "<p>Título".str_replace('\r\n','<br>',$muestra['textos'][0]['texto'])."</p>";
        $html .= "</td>";
        $html .= "<tr>";
        $html .= "</tbody></table>";
        $html .= '<hr>';

        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        // Líneas de cabecera
        $html = "<table><tbody>";
        $html .= "<tr>";
        $html .= "<td width=\"600\" align=\"right\">";
        $html .= "Envío N° " . $muestra['remitos'][0]['tipo_envio'] . "-" . $muestra['remitos'][0]['idu'] . "-" . $muestra['remitos'][0]['id'];

        $html .= " | " . count($muestra['remitos'], $mode = null) . " tubos";

        $html .= "</td>";
      $html .= "</tr>";

      $html .= "<tr>";
        $html .= "<td width=\"600\" align=\"right\">";
        $html .= "Colega: " . $muestra['remitos'][0]['numero_colega'];
        $html .= " - " . $muestra['remitos'][0]['nombre_colega'];
        $html .= " Fecha: " . $muestra['remitos'][0]['fecha'];
        $html .= "</td>";
      $html .= "</tr>";

      $html .= "</tbody>";
      $html .= "</table>";

      $html .= '<hr>';

      $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

      //Espacios entre cabecera y datos
      $html = '<br>';

      $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

      /*echo "Muestra<br>";
      echo "<pre>";
      print_r($muestra);*/



      foreach($muestra['remitos'] as $m){

        //Determinaciones
        $html = "<table><tbody>";

          $html .= "<tr>";

          $html .= "<td width=\"100\">";
          $html .= "Cod. barra";
          $html .= "</td>";
          $html .= "<td width=\"100\">";
          $html .= "Nro. Tubo";
          $html .= "</td>";
          $html .= "<td width=\"150\">";
          $html .= "Apellido y nombre";
          $html .= "</td>";
          $html .= "<td width=\"200\">";
          $html .= "Determinaciones";
          $html .= "</td>";

        $html .= "</tr>";

        $html .= "<tr>";

          $html .= "<td width=\"350\" colspan=\"3\">";
          $codigo = '98-' . $m['idu'] . '-' . $m['nro_tubo'];
          if($config['eliminar_guiones_codigo_barra'])
              $codigo = str_replace('-', '', $codigo);
          $html .= "<img width=\"335\" alt=\"" . $codigo . "\" src=\"" . PRIMARY_URL . "libs/barcode.php?text=" . $codigo . "\" />";
          $html .= "</td>";

          $html .= "<td width=\"200\">";
          $html .= "&nbsp;";
          $html .= "</td>";

          $html .= "</tr>";

          $html .= "<tr>";

          $html .= "<td width=\"100\">";
          $html .= "98".$m['idu'].$m['nro_tubo'];
          $html .= "</td>";
          $html .= "<td width=\"100\">";
          $html .= $m['nro_tubo'];
          $html .= "</td>";
          $html .= "<td width=\"150\">";
          $html .= $m['nombre_paciente'];
          $html .= "</td>";
          $html .= "<td width=\"200\">";
          $html .= $m['determinaciones'];
          $html .= "</td>";

        $html .= "</tr>";

        $html .= "<tr>";

          $html .= "<td width=\"200\">";
          $html .= "<p>Observaciones: ".$m['Observ_Fld']."</p>";
          $html .= "</td>";

        $html .= "</tr>";

        $html .= "<br>";

        $html .= "</tbody>";
        $html .= "</table>";

        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

    }

      // Fuerza a descargar el PDF
      $pdf->Output('resultados.pdf', 'D');
  }

    public function getDeterminacionesComunes(){
        $this->_acl->access('mol');

        //$determinaciones = $this->_molModel->getDeterminacionesComunes();
        $determinaciones = [];
        echo json_encode($determinaciones);
    }

}

?>
