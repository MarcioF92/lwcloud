<?php
class molModel extends Model{

    public function __construct(){
        parent::__construct();
    }

    public function molList($iduser){

        $consulta = "SELECT * FROM Pacientes WHERE Importado_Fld = 0 AND NumDeriv_Fld = ? ORDER BY Numero_Fld DESC";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("i", $iduser);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $muestras = $this->_db->fetchArray($preparedQuery);
                foreach ($muestras as &$muestra) {
                    $muestra['determinaciones'] = $this->getDeterminaciones($muestra['Numero_Fld'], $muestra['Tubos_Fld']);
                }
                return $muestras;
            } else {
                return false;
            }
        }
    }

    public function getDeterminaciones($numero_fld, $numTubo){

        $consulta = "SELECT * FROM Deters WHERE  Numero_Fld = ? AND NumTubo_Fld = ?";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("ii", $numero_fld, $numTubo);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $abrevs = $this->_db->fetchArray($preparedQuery);
                $abrevsFinal = '';
                $isFirst = true;
                foreach ($abrevs as $abr) {
                    if ($isFirst) {
                        $abrevsFinal = $abr['Abrev_Fld'];
                        $isFirst = false;
                    } else {
                        $abrevsFinal .= ', ' . $abr['Abrev_Fld'];
                    }

                }
                return $abrevsFinal;
            } else {
                return false;
            }
        }
    }

    public function getDeterminacionesByNomen($codigo){

        $consulta = 'SELECT DISTINCT n.*, m.Material_Fld, m.Nombre_Fld AS nombre_material FROM Nomen n
          LEFT OUTER JOIN Material m ON n.Material_Fld = m.Numero_Fld
          WHERE Abrev_Fld COLLATE latin1_general_cs LIKE "' . $codigo . '"';

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $determinaciones = $this->_db->fetchArray($preparedQuery);
                return $determinaciones;
            } else {
                $consulta = 'SELECT DISTINCT n.*, m.Material_Fld, m.Nombre_Fld AS nombre_material FROM Nomen n
                  LEFT OUTER JOIN Material m ON n.Material_Fld = m.Numero_Fld
                  WHERE UPPER(CONCAT_WS("|",n.Nombre_Fld, Abrev_Fld)) COLLATE latin1_general_cs LIKE "%' . $codigo . '%" GROUP BY Abrev_Fld';

                $preparedQuery = $this->_db->stmt_init();

                $preparedQuery->prepare($consulta);

                $preparedQuery->execute();
                $preparedQuery->store_result();
                if ($preparedQuery->error != "") {
                    printf("Error: %s.\n", $preparedQuery->error);
                    return false;
                } else {
                    if ($preparedQuery->num_rows > 0) {
                        $determinaciones = $this->_db->fetchArray($preparedQuery);
                        return $determinaciones;
                    } else {
                        return false;
                    }
                }
            }
        }

    }

    public function getRemitos($iduser){
        $idu = $this->getIDU($iduser);

        $consulta = 'SELECT E.*, COUNT(D.Numero_Fld) as cantder
                FROM Envios E INNER JOIN Pacientes D ON E.id = D.NumEnvio_Fld
                WHERE E.idu = ?
                AND D.NumDeriv_Fld = ?
                GROUP BY E.id
                ORDER BY fecha DESC';

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("ii", $idu, $iduser);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $remitos = $this->_db->fetchArray($preparedQuery);
                return $remitos;
            } else {
                return false;
            }
        }
    }

    public function getIDU($iduser){
        $consulta = 'SELECT IDU_Fld FROM DerBioq WHERE Numero_Fld = ?';

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("i", $iduser);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $idu = $this->_db->fetchArray($preparedQuery);
                return $idu[0]['IDU_Fld'];
            } else {
                return false;
            }
        }
    }

    public function getRemito($iduser, $idremito){
        $consulta = 'SELECT DISTINCT E.*, D.Nombre_Fld AS nombre_colega,  P.Observ_Fld, D.Numero_Fld AS numero_colega, P.Nombre_Fld AS nombre_paciente, P.Numero_Fld AS id_paciente, P.Tubos_Fld AS nro_tubo
                    FROM Envios E
                    INNER JOIN DerBioq D
                    ON E.idu = D.IDU_Fld
                    INNER JOIN Pacientes P
                    ON E.id = P.NumEnvio_Fld
                    WHERE E.idu = ? AND E.id = ? AND P.NumDeriv_Fld = ?';

        $idu = $this->getIDU($iduser);

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("iii", $idu, $idremito, $iduser);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $remitos = [];
                $remitos['remitos'] = $this->_db->fetchArray($preparedQuery);
                foreach ($remitos['remitos'] as &$remito) {
                    $remito['determinaciones'] = $this->getDeterminaciones($remito['id_paciente'], $remito['nro_tubo']);
                }
                $remitos['textos'] = $this->getTextos();
                return $remitos;
            } else {
                return false;
            }
        }
    }

    public function save($data){
        $data->respuesta = 'Respuesta satisfactoria';

        return $data;

    }

    public function addPaciente($id, $nombre, $sexo, $dni, $fecha_nacimiento, $nro_tubo, $observaciones, $numOrig, $nro_derivante)
    {
        $consulta = "INSERT INTO Pacientes (Numero_Fld, Nombre_Fld, Sexo_Fld, HClin_Fld, FNacim_Fld, Tubos_Fld, Observ_Fld, Sucursal_Fld, Importado_Fld, ImportNumOrig_Fld, NumDeriv_Fld) VALUES ('".$id."','".$nombre."',".$sexo.",".$dni.",'".$fecha_nacimiento."','".$nro_tubo."','".$observaciones."',999,0,'999-".$numOrig."',".$nro_derivante.")";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        $this->_db->log($consulta);

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
            return false;
        } else {
            //if ($preparedQuery->insert_id) {
            $consulta = "SELECT * FROM Pacientes WHERE Numero_Fld = '" . $id."'";

            $preparedQuery = $this->_db->stmt_init();

            $preparedQuery->prepare($consulta);

            $preparedQuery->execute();
            $preparedQuery->store_result();

            if ($preparedQuery->error != "") {
                printf("Error: %s.\n", $preparedQuery->error);
                return false;
            } else {
                if ($preparedQuery->num_rows > 0) {
                    $lastRow = $this->_db->fetchArray($preparedQuery);
                    return $lastRow[0]['Numero_Fld'];
                }
            }
            //} else {
            return false;
            //}
        }
    }

    public function editPaciente($id, $nombre, $sexo, $dni, $fecha_nacimiento, $nro_tubo, $observaciones, $importNumOrigin, $nro_derivante){

        $consulta = "UPDATE Pacientes SET Nombre_Fld = '".$nombre."', Sexo_Fld = ".$sexo.", HClin_Fld = ".$dni.", FNacim_Fld = '".$fecha_nacimiento."', Tubos_Fld = '".$nro_tubo."', Observ_Fld = '".$observaciones."', Sucursal_Fld = 999, Importado_Fld = 0, ImportNumOrig_Fld = '999-".$importNumOrigin."', NumDeriv_Fld = ".$nro_derivante." WHERE Numero_Fld = ".$id.";";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        $this->_db->log($consulta);

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
            return false;
        } else {
            return $id;
        }
    }

    public function existeNroTubo($nroTubo, $numDeriv, $numFld = false, $oldNroTubo = false){
        //$consulta = 'SELECT * FROM Pacientes WHERE NumDeriv_Fld = ? AND Tubos_Fld = ?';

        $consulta = 'SELECT * FROM Pacientes
          WHERE NumDeriv_Fld = ? AND Tubos_Fld = ?';
        if ($numFld) {
          $consulta .= ' AND Numero_Fld <> '.$numFld;
        }

        $consulta .= ' HAVING Fecha_Fld = (SELECT MAX(Fecha_Fld) FROM Pacientes WHERE NumDeriv_Fld = ? AND Tubos_Fld = ?)
          OR Importado_Fld = 0 ORDER BY Fecha_Fld DESC';

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("iiii", $numDeriv, $nroTubo, $numDeriv, $nroTubo);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n" . " - " . $consulta, $preparedQuery->error);
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $return = false;
                $existePeroEsViejo = false;
                $rows = $this->_db->fetchArray($preparedQuery);
                $newDate = new DateTime();

                foreach ($rows as $row) {
                    $importado = (int) $row['Importado_Fld'];
                    if ($importado != 0) {
                        $startDate = new DateTime(substr($row['Fecha_Fld'],0,4) . "-" . substr($row['Fecha_Fld'],4,2) . "-" . substr($row['Fecha_Fld'],6,2) . " 00:00:00");
                        $intervalo = $newDate->diff($startDate);
                        $dias = (int) $intervalo->format('%R%a');
                        if ($dias > -60) {
                            $return = true;
                        } else {
                            $existePeroEsViejo = true;
                        }
                    } else {
                        $return = true;
                    }
                }
                if($existePeroEsViejo && !$return){
                    $this->reemplazarTubo($numDeriv, $nroTubo);
                }
                return $return;
            } else {
                return false;
            }
        }
    }

    public function existeNumero($numero){
			$consulta = "SELECT DISTINCT(Numero_Fld) FROM Pacientes WHERE ImportNumOrig_Fld LIKE '%-".$numero."'";

        	$preparedQuery = $this->_db->stmt_init();

        	$preparedQuery->prepare($consulta);

        	$preparedQuery->execute();
        	$preparedQuery->store_result();

        	if ($preparedQuery->error != "") {
                printf("Error: %s.\n" . " - " . $consulta, $preparedQuery->error);
                return false;
        	} else {
                if ($preparedQuery->num_rows > 0) {
                    return true;
                } else {
                    return false;
                }
        	}
    	}

    public function getIdUnico(){
        $consulta = 'INSERT INTO idsunicos (void) VALUES (1)';

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
            return false;
        } else {
            if ($preparedQuery->insert_id) {

                $consulta = 'SELECT * FROM Pacientes
                WHERE Numero_Fld = '.$preparedQuery->insert_id.' AND Sucursal_Fld = 999';

                $preparedSecondQuery = $this->_db->stmt_init();

                $preparedSecondQuery->prepare($consulta);

                $preparedSecondQuery->execute();
                $preparedSecondQuery->store_result();

                if ($preparedSecondQuery->num_rows > 0){
                    $this->_db->log("ID ".$preparedQuery->insert_id." repetido");
                    return $this->getIdUnico();
                }
                return $preparedQuery->insert_id;
            } else {
                return false;
            }
        }
    }

    /*public function getIdUnico(){
        $consulta = 'SELECT MAX(Numero_Fld) as maxId FROM Pacientes LIMIT 1';

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $pacienteAux = $this->_db->fetchArray($preparedQuery);
                return $pacienteAux[0]['maxId'] + 1;
            } else {
                return false;
            }
        }
    }*/

    public function addDeterminaciones($id, $nroTubo, $determinaciones){
        if ($this->existsDeterminaciones($id, $nroTubo)) {
            if($this->removeDeterminaciones($id, $nroTubo)){
                $orden = 1;
                foreach ($determinaciones as $determinacion) {
                    $consulta = "INSERT INTO Deters (Numero_Fld, NumTubo_Fld, Abrev_Fld, Sucursal_Fld, Orden_Fld) VALUES (".$id.",'".$nroTubo."','".$determinacion->Abrev_Fld."',999,".$orden.")";

                    $preparedQuery = $this->_db->stmt_init();

                    $preparedQuery->prepare($consulta);

                    $preparedQuery->execute();
                    $preparedQuery->store_result();

                    $this->_db->log($consulta);

                    if ($preparedQuery->error != "") {
                        printf("Error: %s.\n", $preparedQuery->error);
                        return false;
                    }
                    $orden += 1;
                }

                return true;
            } else {
                return false;
            }
        } else {
            $orden = 1;
            foreach ($determinaciones as $determinacion) {
                $consulta = "INSERT INTO Deters (Numero_Fld, NumTubo_Fld, Abrev_Fld, Sucursal_Fld, Orden_Fld) VALUES (".$id.",'".$nroTubo."','".$determinacion->Abrev_Fld."',999,".$orden.")";

                $preparedQuery = $this->_db->stmt_init();

                $preparedQuery->prepare($consulta);

                $preparedQuery->execute();
                $preparedQuery->store_result();

                $this->_db->log($consulta);

                if ($preparedQuery->error != "") {
                    printf("Error: %s.\n", $preparedQuery->error);
                    return false;
                }
                $orden += 1;
            }

            return true;
        }


    }

    public function existsDeterminaciones($id, $nroTubo){
        $consulta = "SELECT * FROM Deters WHERE Numero_Fld = ".$id." AND  NumTubo_Fld = '".$nroTubo."'";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function removeDeterminaciones($id, $nroTubo){
        $consulta = "DELETE FROM Deters WHERE Numero_Fld = ".$id." AND  NumTubo_Fld = '".$nroTubo."'";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        $this->_db->log($consulta);

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
            return false;
        } else {
            return true;
        }
    }

    public function getForEdit($id){
        $consulta = 'SELECT * FROM Pacientes WHERE Numero_Fld LIKE ?';

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("s", $id);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $pacienteAux = $this->_db->fetchArray($preparedQuery);
                $paciente = $pacienteAux[0];
                $paciente['determinaciones'] = $this->getDeterminacionesForEdit($id);
                return $paciente;
            } else {
                return false;
            }
        }
    }

    public function getDeterminacionesForEdit($id){
        $consulta = "SELECT D.Abrev_Fld, N.Nombre_Fld, M.Material_Fld as Material_Fld
                FROM Deters D LEFT JOIN Nomen N ON D.Abrev_Fld = N.Abrev_Fld
                LEFT JOIN Material M ON M.Numero_Fld = N.Material_Fld
                  WHERE D.Numero_Fld like ?
                  ORDER BY Orden_Fld";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->bind_param("s", $id);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $determinaciones = $this->_db->fetchArray($preparedQuery);
                return $determinaciones;
            } else {
                return false;
            }
        }
    }

    public function reemplazarTubo($numDeriv, $oldNroTubo){
        $consulta = "UPDATE Pacientes SET Tubos_Fld = (Tubos_Fld * -1)
                     WHERE NumDeriv_Fld = ".$numDeriv."
                     AND Tubos_Fld = ".$oldNroTubo;

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        $this->_db->log($consulta);

        if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
            return false;
        } else {
            return true;
        }
    }

    public function eliminarMuestra($id, $nroTubo){

 		if($this->removeDeterminaciones($id, $nroTubo)){
			$consulta = "DELETE FROM Pacientes WHERE Numero_Fld = ".$id;

        	$preparedQuery = $this->_db->stmt_init();

        	$preparedQuery->prepare($consulta);

        	$preparedQuery->execute();
        	$preparedQuery->store_result();

            $this->_db->log($consulta);

        	if ($preparedQuery->error != "") {
            printf("Error: %s.\n", $preparedQuery->error);
         	return false;
        	} else {
            return true;
        	}
 		}

    }

    public function confirmarMuestras($iduser){
    		$idu = $this->getIDU($iduser);

    		$fecha = time();

    		$consulta = "INSERT INTO Envios (idu, tipo_envio, fecha) VALUES (".$idu.",'EM','".date('Y-m-d H:i:s', $fecha)."')";

        	$preparedQuery = $this->_db->stmt_init();

        	$preparedQuery->prepare($consulta);

        	$preparedQuery->execute();
        	$preparedQuery->store_result();

            $this->_db->log($consulta);

        	if ($preparedQuery->insert_id) {
        		$id = $preparedQuery->insert_id;

                $consulta = "SELECT Numero_Fld FROM Pacientes WHERE Importado_Fld = 0 AND NumDeriv_Fld = ".$iduser.";";

                $preparedQuery = $this->_db->stmt_init();

                $preparedQuery->prepare($consulta);

                $preparedQuery->execute();
                $preparedQuery->store_result();

                if ($preparedQuery->error != "") {
                    return false;
                } else {
                    if ($preparedQuery->num_rows > 0) {
                        $pacientes = $this->_db->fetchArray($preparedQuery);

                        foreach ($pacientes as $paciente) {

                            $consulta = "UPDATE Deters
                                        SET UltModif_Fld = ''
                                        WHERE Numero_Fld = " . $paciente['Numero_Fld'] . ";";

                            $preparedQuery = $this->_db->stmt_init();

                            $preparedQuery->prepare($consulta);

                            $preparedQuery->execute();
                            $preparedQuery->store_result();
                            $this->_db->log($consulta);

                        }

                        $consulta = "UPDATE Pacientes
                                    SET Importado_Fld = 1, Fecha_Fld = '" . date('Ymd', $fecha) . "', NumEnvio_Fld = " . $id . "
                                    WHERE Importado_Fld = 0 AND NumDeriv_Fld = " . $iduser;

                        $preparedQuery = $this->_db->stmt_init();

                        $preparedQuery->prepare($consulta);

                        $preparedQuery->execute();
                        $preparedQuery->store_result();

                        $this->_db->log($consulta);

                        if ($preparedQuery->error != "") {
                            printf("Error: %s.\n", $preparedQuery->error);
                            return false;
                        } else {
                            return true;
                        }

                    }
                }
        		

        	} else {
        		printf("Error: %s.\n", $preparedQuery->error);
         	    return false;
        	}
    }

    public function getTextos(){
        $consulta = "SELECT * FROM textos";

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $textos = $this->_db->fetchArray($preparedQuery);
            } else {
                $textos = false;
            }
        }

        return $textos;
    }

    public function getDeterminacionesComunes(){
        $consulta = "SELECT d.* FROM Deters d
                    INNER JOIN Pacientes p ON d.Numero_Fld = p.Numero_Fld
                    WHERE p.NumDeriv_Fld = ?
                    GROUP BY d.Abrev_Fld
                    ORDER BY count(d.Abrev_Fld) DESC
                    LIMIT 10;";

        /*$consulta = "SELECT d.*, n.Nombre_Fld FROM Deters d
                    INNER JOIN Nomen n ON d.Abrev_Fld = n.Abrev_Fld
                    INNER JOIN Pacientes p ON d.Numero_Fld = p.Numero_Fld
                    WHERE p.NumDeriv_Fld = ?
                    GROUP BY d.Abrev_Fld
                    ORDER BY count(d.Abrev_Fld) DESC
                    LIMIT 10;";*/

        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare($consulta);

        $idUser = Session::get('iduser');

        $preparedQuery->bind_param("i", $idUser);

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $determinaciones = $this->_db->fetchArray($preparedQuery);

            } else {
                $determinaciones = false;
            }
        }

        return $determinaciones;
    }

    public function getConfigs(){
        $preparedQuery = $this->_db->stmt_init();

        $preparedQuery->prepare('SELECT * FROM Configs');

        $preparedQuery->execute();
        $preparedQuery->store_result();

        if ($preparedQuery->error != "") {
            return false;
        } else {
            if ($preparedQuery->num_rows > 0) {
                $configs = $this->_db->fetchArray($preparedQuery);
                $configsAux = [];
                foreach ($configs as $config){
                    $configsAux[$config['config_key']] = $config['config_value'];
                }
                return $configsAux;
            } else {
                return false;
            }
        }
    }

}

?>
