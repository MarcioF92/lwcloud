<?php
/* Identificador en rama master */

ini_set('upload_tmp_dir', 'tmp/files');

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', realpath(dirname(__FILE__)) . DS); // Ruta raíz de la Appi
define('APP_PATH', ROOT . 'app' . DS );

require_once APP_PATH . 'Autoload.php';
require_once APP_PATH . 'Config.php';

App::init();

try{


    Session::init();

    $registry = Registry::getInstance();

    $registry->_request = new Request();

    $registry->_db = new Database();

    $registry->_acl = new Acl();

    Bootstrap::run($registry->_request);


}
catch(Exception $e){
    $e->getMessage();
}
?>
