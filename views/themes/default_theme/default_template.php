<?php $this->getPartOfTemplate('header'); ?>

<div class="col-lg-12 row  <?php 
	foreach ($this->_customVars['content-classes'] as $class) {
		echo $class.' ';
	} 
?>" id="content">
	<?php include_once $content;?>
</div>

<?php $this->getPartOfTemplate('footer'); ?>