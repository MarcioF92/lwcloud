<!DOCTYPE html>
<html lang="es_AR" ng-app="ui.bootstrap">
<head>
	<title>
		<?php
			if (isset($this->_customVars['titulo']) && $this->_customVars['titulo'] != '') {
				echo $this->_customVars['titulo'] . ' | ';
			}

			echo APP_NAME;

		?>
	</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->_layoutParams['path_css'] . 'bootstrap.min.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->_layoutParams['path_css'] . 'font-awesome.min.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->_layoutParams['path_css'] . 'jquery-ui.min.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->_layoutParams['path_css'] . 'jquery-ui.structure.min.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->_layoutParams['path_css'] . 'jquery-ui.theme.min.css'; ?>">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->_layoutParams['path_css'] . 'style.css'; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->_layoutParams['path_css'] . 'colorpicker.min.css'; ?>">
	<script type="text/javascript" src="<?php echo $this->_layoutParams['path_js'] . 'jquery.js'; ?>"></script>
	<script type="text/javascript" src="<?php echo $this->_layoutParams['path_js'] . 'jquery-ui.min.js'; ?>"></script>
	<script type="text/javascript" src="<?php echo $this->_layoutParams['path_js'] . 'bootstrap.min.js'; ?>"></script>
	<script type="text/javascript" src="<?php echo $this->_layoutParams['path_js'] . 'angular.min.js'; ?>"></script>
	<script type="text/javascript" src="<?php echo $this->_layoutParams['path_js'] . 'angular-animate.min.js'; ?>"></script>
	<script type="text/javascript" src="<?php echo $this->_layoutParams['path_js'] . 'angular-touch.min.js'; ?>"></script>
	<script type="text/javascript" src="<?php echo $this->_layoutParams['path_js'] . 'angular-sanitize.js'; ?>"></script>
	<script type="text/javascript" src="<?php echo $this->_layoutParams['path_js'] . 'ui-bootstrap-tpls-1.3.3.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $this->_layoutParams['path_js'] . 'bootstrap-colorpicker-module.min.js'; ?>"></script>

	<script>
		var __root__ = "<?php echo BASE_URL; ?>";
		var __primary__ = "<?php echo PRIMARY_URL; ?>";
	</script>

	<?php foreach ($this->_js as $js) { ?>
		<script type="text/javascript" src="<?php echo $js; ?>"></script>
	<?php } ?>
</head>
<body class="background <?php 
	foreach ($this->_customVars['body-classes'] as $class) {
		echo $class.' ';
	} 
?>">
	<div id="main">
		<div id="error-message">
            <?php
            if (isset($this->_customVars['_error']))
                echo $this->_customVars['_error'];
            ?>
		</div>

		<?php
				echo $this->getPartOfTemplate('admin-menu');
		?>