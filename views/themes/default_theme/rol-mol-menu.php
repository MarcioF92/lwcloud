<!-- ROL - MOL Menu -->
<nav class="navbar navbar-default col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-ms-12 col-xs-12">
  <div class="container-fluid col-lg-12 col-md-12 col-ms-12 col-xs-12">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header row hidden-lg hidden-md">
      <button id="menu-button" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Menu</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse row" id="bs-example-navbar-collapse-1" data-collapse="collapsed">
      <ul id="menu-ul" class="nav navbar-nav col-lg-12">
          <li class="col-lg-3 <?php if($this->getViewId() == 'rol'){echo 'item-active';}else{echo 'item-no-active';} ?>">
        	  <a href="<?php echo $this->_layoutParams['base_url']; ?>rol">RESULTADOS ONLINE</a>
          </li>

          <?php if (Session::get('role') == 1) { ?>

              <li class="col-lg-4 <?php if($this->getViewId() == 'mol'){echo 'item-active';}else{echo 'item-no-active';} ?>">
                <a href="<?php echo $this->_layoutParams['base_url']; ?>mol">MUESTRAS ONLINE</a>
              </li>

            <?php } ?>

          <?php if (Session::get('role') == 3) { ?>

              <li class="col-lg-4 <?php if($this->getViewId() == 'mol'){echo 'item-active';}else{echo 'item-no-active';} ?>">
                  <a href="<?php echo $this->_layoutParams['base_url']; ?>config/index/cambiar_password">CAMBIAR CONTRASEÑA</a>
              </li>

          <?php } ?>

          <li class="dropdown item-no-active col-lg-2">
              <a href="#" class="dropdown-toggle text-right" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Perfil <span class="caret"></span></a>
              <ul class="dropdown-menu">
                  <li>
                      <a>
                          <i class="fa fa-flask" aria-hidden="true"></i> <?php echo Session::get('nombre_apellido') ?>
                      </a>
                  </li>
                  <li>
                      <a href="<?php echo BASE_URL . 'config/index/cambiar_password'; ?>">
                          <i class="fa fa-key" aria-hidden="true"></i> Cambiar contraseña
                      </a>
                  </li>
                  <li>
                      <a href="<?php echo BASE_URL . 'login/index/cerrar'; ?>">
                          <i class="fa fa-lock" aria-hidden="true"></i> Cerrar sesión
                      </a>
                  </li>
              </ul>
          </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<script>
$(document).on('ready', function(){
	$("#menu-button").click(function(){
		if($("#bs-example-navbar-collapse-1").attr('data-collapse') == 'collapsed'){
			$("#bs-example-navbar-collapse-1").slideDown();
			$("#bs-example-navbar-collapse-1").attr('data-collapse', 'expanded');
		} else {
			$("#bs-example-navbar-collapse-1").slideUp();
			$("#bs-example-navbar-collapse-1").attr('data-collapse', 'collapsed');
		}
	});
});

</script>