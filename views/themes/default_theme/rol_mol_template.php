<?php $this->getPartOfTemplate('header', 'rol-mol'); ?>

<div class="<?php 
	foreach ($this->_customVars['content-classes'] as $class) {
		echo $class.' ';
	} 
?>" id="content">
	<?php include_once $content;?>
</div>

<?php $this->getPartOfTemplate('footer'); ?>